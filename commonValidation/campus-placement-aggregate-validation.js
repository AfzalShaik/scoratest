'use strict';
var Joi = require('joi');
var validVal;
  /**
   *validateJson-function deals with validating the correct json format
   *@constructor
   * @param {object} massuploadData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function validateJson(massuploadData, cb) {
  //validating input json fields
  var currentYear = new Date().getFullYear();
 // console.log(date);
  var schema = Joi.object().keys({
    departmentName: Joi.string().max(100).required(),
    programName: Joi.string().max(50).required(),
    acedemicYear: [Joi.number().integer().min(2000).max(currentYear), Joi.allow(null)],
    minimumOffer: [Joi.string().max(18), Joi.allow(null)],
    maximumOffer: [Joi.string().max(18), Joi.allow(null)],
    rowNumber: Joi.number().integer().max(999999999999999),
    totalOffers: [Joi.string().max(18), Joi.allow(null)],
    noOfStudents: [Joi.number().integer().max(99999), Joi.allow(null)],
    noOfOffers: [Joi.number().integer().max(99999), Joi.allow(null)],
    top5: [Joi.string().max(18), Joi.allow(null)],
    sixToTen: [Joi.string().max(18), Joi.allow(null)],
    levenTo20: [Joi.string().max(18), Joi.allow(null)],
    twentyOneTo50: [Joi.string().max(18), Joi.allow(null)],
    above50: [Joi.string().max(18), Joi.allow(null)],
    error: [Joi.string().max(500), Joi.allow(null)],
  });

  Joi.validate(massuploadData, schema, function(err, value) {
    if (err) {
      var error = new Error('');
      error.statusCode = 422;
      error.requestStatus = false;
      error.err = err.details[0].message;
      validVal = false;

      cb(error, null);
    } else {
      validVal = true;
      cb(null, validVal);
    }
  });
}
exports.validateJson = validateJson;
