'use strict';
var server = require('../server/server');
//validating addressTypeValueId in addressTypeCodeFunction
/**
   *typeCodeFunction-function deals finding lookup type value id and look up type id
   *@constructor
   * @param {object} lookupValueObj - contains the data need to get find
   * @param {object} prefixCode - for comparing the lookupcode with prefix code
   * @param {function} cb - deals with the response
   */
function typeCodeFunction(lookupValueObj, prefixCode, cb) {
  var lookupVal = false;
  var lookupValue = server.models.LookupValue;
  var lookupType = server.models.LookupType;
  var objCheck = lookupValueObj[Object.keys(lookupValueObj)[0]];

  if (objCheck != undefined) {
    lookupValue.findOne({
      'where': {
        'and': [lookupValueObj],
      },
    }, function(err, lookupValueId) {
      if (err) {
        lookupVal = false;

        cb(lookupVal);
      } else if (lookupValueId) {
        var typeId = lookupValueId.lookupTypeId;

        lookupType.find({
          'where': {
            'lookupTypeId': typeId,
          },
        }, function(err, lookupTypeId) {
          if (err) {
            lookupVal = false;
            cb(lookupVal);
          } else if (lookupTypeId) {
            var lookupCode = lookupTypeId[0].lookupCode;

            if (lookupCode == prefixCode) {
              lookupVal = true;
              cb(lookupVal);
            } else {
              lookupVal = false;
              cb(lookupVal);
            }
          } else {
            lookupVal = false;

            cb(lookupVal);
          }
        });
      } else {
        lookupVal = false;

        cb(lookupVal);
      }
    });
  } else {
    lookupVal = true;
    cb(lookupVal);
  }
}
var count = 0;

function validateLookups(inputObj, cb) {
  inputObj.map(function(lookupValueObj) {
    var lookupVal = false;
    var objCheck = lookupValueObj[Object.keys(lookupValueObj)[0]];
    var prefixCode = lookupValueObj[Object.keys(lookupValueObj)[1]];
    var lookupValue = server.models.LookupValue;
    var lookupType = server.models.LookupType;
    lookupValue.findOne({
      'where': {
        'and': [{
          'lookupValueId': objCheck,
        }],
      },
    }, function(err, lookupValueId) {
      if (err) {
        lookupVal = false;
        cb(lookupVal);
      } else if (lookupValueId) {
        var typeId = lookupValueId.lookupTypeId;
        lookupType.find({
          'where': {
            'lookupTypeId': typeId,
          },
        }, function(err, lookupTypeId) {
          if (err) {
            lookupVal = false;
            cb(lookupVal);
          } else if (lookupTypeId) {
            var lookupCode = lookupTypeId[0].lookupCode;
            if (lookupCode == prefixCode) {
              lookupVal = true;
              count++;
              if (inputObj.length == count) {
                count = 0;
                cb(lookupVal);
              }
            } else {
              lookupVal = false;
              cb(lookupVal);
            }
          } else {
            lookupVal = false;

            cb(lookupVal);
          }
        });
      } else {
        lookupVal = false;

        cb(lookupVal);
      }
    });
  });
}


function lookupMethod(code, cb) {
  var lookupValueModel = server.models.LookupValue;
  var lookupTypeModel = server.models.LookupType;
  lookupTypeModel.findOne({'where': {'lookupCode': code}}, function(err, typeResp) {
    if (err) {
      cb(err, null);
    } else {
      lookupValueModel.find({'where': {'lookupTypeId': typeResp.lookupTypeId}}, function(error, valueResp) {
        if (error) {
          cb(error, null);
        } else {
          cb(null, valueResp);
        }
      });
    }
  });
}
function getLookupId(code, value, cb) {
  lookupMethod(code, function(err, response) {
    var valueId = response.find(findStatus);

    function findStatus(statusVal) {
      return statusVal.lookupValue === value;
    }
    cb(valueId);
  });
}
exports.typeCodeFunction = typeCodeFunction;
exports.validateLookups = validateLookups;
exports.lookupMethod = lookupMethod;
exports.getLookupId = getLookupId;

