'use strict';
var Joi = require('joi');
var validVal;
 /**
   *validateCampusJson-function deals with validating the campus json
   *@constructor
   * @param {object} campusData - contains all the data need to get updated
   * @param {function} cb - deals with the response
   */
function validateCampusJson(campusData, cb) {
  //validating input json fields
  var schema = Joi.object().keys({
    campusId: Joi.number().integer().max(999999999999999).required(),
    name: Joi.string().max(100),
    description: [Joi.string().max(200), Joi.allow(null)],
    numberOfStudents: [Joi.number().max(999999999999999), Joi.allow(null)],
    universityId: Joi.number().max(999999999999999),
    logo: [Joi.string().max(200), Joi.allow(null)],
    brandingImage: [Joi.string().max(200), Joi.allow(null)],
    missionStatement: [Joi.string().max(2000), Joi.allow(null)],
    shortName: [Joi.string().max(50), Joi.allow(null)],
    establishedDate: [Joi.date().iso(), Joi.allow(null)],
    website: [Joi.string().max(200), Joi.allow(null)],
    facebook: [Joi.string().max(200), Joi.allow(null)],
    twitter: [Joi.string().max(200), Joi.allow(null)],
    youtube: [Joi.string().max(200), Joi.allow(null)],
    linkedin: [Joi.string().max(200), Joi.allow(null)],
    createDatetime: Joi.date().iso(),
    updateDatetime: Joi.date().iso(),
    createUserId: Joi.number().max(999999999999999),
    updateUserId: Joi.number().max(999999999999999),
    rating: [Joi.number().max(99), Joi.allow(null)],
    rank: [Joi.number().max(99999), Joi.allow(null)],
    tierValueId: [Joi.number().max(99999), Joi.allow(null)],
    campusStatusValueId: Joi.number().integer().max(999999999999999),
    searchName: [Joi.string().max(100), Joi.allow(null)],
    searchShortName: [Joi.string().max(50), Joi.allow(null)],
  });

  Joi.validate(campusData, schema, function(err, value) {
    if (err) {
      var error = new Error('Validation Error');
      error.statusCode = 422;
      error.requestStatus = false;
      error.err = err;
      validVal = false;

      cb(error, validVal);
    } else {
      validVal = true;
      cb(null, validVal);
    }
  });
}
exports.validateCampusJson = validateCampusJson;
