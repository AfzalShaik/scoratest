'use strict';
var server = require('../server/server');
var async = require('async');
/**
   *createCampusAggregate-function deals with creating the campus aggregate
   *@constructor
   * @param {object} aggregateData - contains department id
   * @param {function} callBc - deals with the response
   */
function createCampusAggregate(aggregateData, callBc) {
  async.map(aggregateData, aggregateMethod, function(err, resp) {
    if (err) {
      callBc(err.errors, null);
    } else {
      callBc(null, resp);
    }
  });

  function aggregateMethod(obj, callback) {
    var campusPlacementAggregates = server.models.CampusPlacementAggregates;
    aggInput(obj, aggregateData, function(aggregateObj) {
      campusPlacementAggregates.create(aggregateObj, function(err, aggResp) {
        if (err) {
          callback(err, null);
        } else {
          callback(null, aggResp);
        }
      });
    });
  }
}

function aggInput(obj, aggregateData, callBc) {
  var department = server.models.Department;
  var campus = server.models.Campus;
  var program = server.models.Program;
  department.findOne({
    'where': {
      'departmentId': obj.departmentId,
    },
  }, function(err, deptResp) {
    var campusId = deptResp.campusId;
    
    campus.findOne({
      'where': {
        'campusId': campusId,
      },
    }, function(err, campusResp) {
      program.findOne({'where': {
        'and': [{
          'campusId': campusId,
        }, {
          'departmentId': obj.departmentId,
        }],
      }}, function(err, programResp) {
        var programId = 101010;
        var universityId = campusResp.universityId;
        var aggregateObj = {
          'campusId': campusId,
          'universityId': universityId,
          'departmentId': obj.departmentId,
          'programId': programId,
          'noOfStudents': obj.noOfStudents,
          'noOfOffers': obj.noOfOffers,
          'totalOffers': obj.totalOffers,
          'minimumOffer': obj.minimumOffer,
          'maximumOffer': obj.maximumOffer,
          'academicYear': obj.acedemicYear,
          'top5': obj.top5,
          'sixToTen': obj.sixToTen,
          'levenTo20': obj.levenTo20,
          'twentyOneTo50': obj.twentyOneTo50,
          'above50': obj.above50,
        };
        // console.log(aggregateObj);
        callBc(aggregateObj);
      });
    });
  });
}
exports.createCampusAggregate = createCampusAggregate;
