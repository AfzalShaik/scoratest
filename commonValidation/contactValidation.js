'use strict';
var Joi = require('joi');
var validVal;

// TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Use standard hyphen separated naming conventino for files. 
/**
   *validateContactJson-function deals with validating json data
   *@constructor
   * @param {object} contactData - contains all the data need to get valiadated
   * @param {function} cb - deals with the response
   */
function validateContactJson(contactData, cb) {
  var schema = Joi.object().keys({
    campusId: Joi.number().integer().max(999999999999999),
    companyId: Joi.number().max(999999999999999),
    studentId: Joi.number().max(999999999999999),
    departmentId: Joi.number().max(999999999999999),
    organizationId: Joi.number().max(999999999999999),
    educationPersonId: Joi.number().max(999999999999999),
    programId: Joi.number().max(999999999999999),
    employerPersonId: Joi.number().max(999999999999999),
    contactId: Joi.number().max(999999999999999).required(),
    contactTypeValueId: Joi.number().max(999999999999999),
    contactInfo: [Joi.string().max(100), Joi.allow(null)],
    primaryInd: [Joi.string().max(1), Joi.allow(null)],
    createDatetime: Joi.date().iso(),
    updateDatetime: Joi.date().iso(),
    createUserId: Joi.number().max(999999999999999),
    updateUserId: Joi.number().max(999999999999999),
  });
  Joi.validate(contactData, schema, function(err, value) {
    if (err) {
      var error = new Error('Validation Error');
      error.statusCode = 422;
      error.requestStatus = false;
      error.err = err;
      validVal = false;

      cb(error, validVal);
    } else {
      validVal = true;
      cb(null, validVal);
    }
  });
}
exports.validateContactJson = validateContactJson;
