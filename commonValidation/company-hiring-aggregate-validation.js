'use strict';
var Joi = require('joi');
var validVal;
  /**
   *validateJson-function deals with validating the correct json format
   *@constructor
   * @param {object} massuploadData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function validateJson(massuploadData, cb) {
  //validating input json fields
  var date = new Date().getFullYear();
  var schema = Joi.object().keys({
    organizationName: Joi.string().max(100).required(),
    jobRoleName: Joi.string().max(50).required(),
    campusName: Joi.string().max(100).required(),
    calendarYear: [Joi.allow(['', null]), Joi.number().integer().max(date)],
    maximumOffer: Joi.number().precision(6),
    rowNumber: Joi.number().integer().max(999999999999999),
    minimumOffer: Joi.number().precision(6),
    sumOfOffers: Joi.number().precision(6),
    noOfOffers: Joi.number().max(99999),
  });
  Joi.validate(massuploadData, schema, function(err, value) {
    if (err) {
      var error = new Error('');
      error.statusCode = 422;
      error.requestStatus = false;
      error.err = err.details[0].message;
      validVal = false;

      cb(error, null);
    } else {
      var acedemic = isNumeric(massuploadData.calendarYear);
      if (acedemic == true) {
        var date = new Date();
        var year = date.getFullYear();
        if (parseInt(massuploadData.calendarYear) <= year && parseInt(massuploadData.calendarYear) >= (year - 4)) {
          value.calendarYear = parseInt(massuploadData.calendarYear);
          cb(null, value);
        } else {
          var error = new Error('');
          error.statusCode = 422;
          error.requestStatus = false;
          error.err = 'Calender year must be less than or equal to present year and Should be past 5 year from the current year';
          validVal = false;
          cb(error, null);
        }
      } else {
        var error = new Error('');
        error.statusCode = 422;
        error.requestStatus = false;
        error.err = 'Calender year must be a Number';
        validVal = false;
        cb(error, null);
      }
    }
  });
  function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }
}
exports.validateJson = validateJson;
