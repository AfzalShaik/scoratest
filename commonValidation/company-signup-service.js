'use strict';
var server = require('../server/server');
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var lookup = require('./lookupMethods').lookupMethod;

function companySignupService(userInstance, callBack) {
  // To Get Campus status_Type_Value_Id
  lookup('COMPANY_STATUS_CODE', function(err, response) {
    var companyIndicator = response.find(findIndicatior);

    function findIndicatior(companyVal) {
      return companyVal.lookupValue === 'Registered';
    }
    var company = server.models.Company;
    var companyObj = {
      'name': userInstance.entityName,
      'searchName': userInstance.entityName.toUpperCase(),
      'companyStatusValueId': companyIndicator.lookupValueId,
      'createDatetime': new Date(),
      'updateDatetime': new Date(),
      'createUserId': 1,
      'updateUserId': 1,
    };
    company.create(companyObj, function(companyErr, companyResp) {
      if (companyErr) {
        throwError(companyErr, callBack);
      }
      var userRole = server.models.ScoraUserRole;
      var employerPerson = server.models.EmployerPerson;
      if (companyResp) {
        var userRoleObj = {
          'id': userInstance.id,
          'roleCode': 'RECDIR',
          'startDate': new Date(),
          'createDatetime': new Date(),
          'updateDatetime': new Date(),
          'createUserId': 1,
          'updateUserId': 1,
        };
        userRole.create(userRoleObj, function(roleError, roleResponse) {
          if (roleError) {
            throwError(roleError, callBack);
          } else {
            var employerPersonObj = {
              'companyId': companyResp.companyId,
              'id': userInstance.id,
              'firstName': userInstance.firstName,
              'lastName': userInstance.lastName,
              'createDatetime': new Date(),
              'updateDatetime': new Date(),
              'createUserId': 1,
              'updateUserId': 1,
            };
            employerPerson.create(employerPersonObj, function(empError, empResponse) {
              if (empError) {
                throwError(empError, callBack);
              } else {
                callBack(null, empResponse);
              }
            });
          }
        });
      }
    });
  });
}
exports.companySignupService = companySignupService;
