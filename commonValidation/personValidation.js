'use strict';
var Joi = require('joi');
var validVal;
/**
   *validateContactJson-function deals with validating the json file of contact
   *@constructor
   * @param {object} personData - data that need to get validated
   * @param {function} cb - deals with the response
   */
// TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Can these files be generated from the Database schema ?
function validateContactJson(personData, cb) {
  var schema = Joi.object().keys({
    campusId: Joi.number().integer().max(999999999999999),
    companyId: Joi.number().integer().max(999999999999999),
    middleName: [Joi.string().max(50), Joi.allow(null)],
    firstName: Joi.string().max(50),
    userId: Joi.number().max(999999999999999),
    educationPersonId: Joi.number().max(999999999999999),
    employerPersonId: Joi.number().max(999999999999999),
    lastName: [Joi.string().max(50), Joi.allow(null)],
    prefixValueId: [Joi.number().max(999999999999999), Joi.allow(null)],
    genderValueId: [Joi.number().max(999999999999999), Joi.allow(null)],
    personTypeValueId: [Joi.number().max(999999999999999), Joi.allow(null)],
    designation: [Joi.string().max(100), Joi.allow(null)],
    createDatetime: Joi.date().iso(),
    updateDatetime: Joi.date().iso(),
    createUserId: Joi.number().max(999999999999999),
    updateUserId: Joi.number().max(999999999999999),
    pictureUrl: [Joi.string().max(200), Joi.allow(null)],
  });
  Joi.validate(personData, schema, function(err, value) {
    if (err) {
      var error = new Error('Validation Error');
      error.statusCode = 422;
      error.requestStatus = false;
      error.err = err;
      validVal = false;

      cb(error, validVal);
    } else {
      validVal = true;
      cb(null, validVal);
    }
  });
}
exports.validateContactJson = validateContactJson;
