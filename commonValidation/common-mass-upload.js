'use strict';
//function to read the csv file
function readFile(inputFile, cb) {
  var fs = require('fs');
  var parse = require('csv-parse');
  // var async = require('async');
  var parser = parse({
    delimiter: ',',
    columns: true,
  }, function(err, data) {
    //   async.eachSeries(data, function(line, callBc) {  });
    if (err) {
      cb(err, null);
    } else {
      cb(null, data);
    }
  });

  fs.createReadStream(inputFile).pipe(parser);
}
// this function will remove null, undefined and '' from the array.
function cleanArray(actual) {
  var newArray = new Array();
  for (var i = 0; i < actual.length; i++) {
    if (actual[i]) {
      newArray.push(actual[i]);
    }
  }
  return newArray;
}
exports.readFile = readFile;
exports.cleanArray = cleanArray;
