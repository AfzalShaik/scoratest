'use strict';
var server = require('../server/server');
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var lookup = require('./lookupMethods').lookupMethod;
function studentSignupService(userInstance, callBack) {
  // console.log('userinstanceeeeeeeeeeeeeeeeeeeeeeeeeee ', userInstance.id);
  // To Get Campus status_Type_Value_Id
  lookup('STUDENT_STATUS_CODE', function(err, response) {
    var studentIndicator = response.find(findIndicatior);

    function findIndicatior(studentVal) {
      return studentVal.lookupValue === 'Pending Confirmation';
    }
    var userRole = server.models.ScoraUserRole;
    var enrollment = server.models.Enrollment;
    var userRoleObj = {
      'id': userInstance.id,
      'roleCode': 'STUDENT',
      'startDate': new Date(),
      'createDatetime': new Date(),
      'updateDatetime': new Date(),
      'createUserId': 1,
      'updateUserId': 1,
    };
    userRole.create(userRoleObj, function(roleError, roleResponse) {
      // console.log('roleerrrrrrrrrrrrrrrrrrrrrrrrrr ', roleError, roleResponse.roleCode);
      if (roleError) {
        throwError(roleError, callBack);
      } else {
        var student = server.models.Student;
        var studentObj = {
          'id': userInstance.id,
          'studentStatusValueId': studentIndicator.lookupValueId,
          'firstName': userInstance.firstName,
          'lastName': userInstance.lastName,
          'createDatetime': new Date(),
          'updateDatetime': new Date(),
          'createUserId': 1,
          'updateUserId': 1,
        };
        student.create(studentObj, function(studentErr, studentResp) {
          // console.log('studenterrrrrrrrrrrrrrrrrrrrrrrrrrrrrr ', studentErr);
          if (studentErr) {
            throwError(studentErr, callBack);
          } else {
            var campusId = (userInstance.campusId) ? userInstance.campusId : undefined;
            var enrollmentObj = {
              'studentId': studentResp.studentId,
              'programId': userInstance.programId,
              'admissionNo': userInstance.admissionNo,
              'startDate': userInstance.startDate,
              'planedCompletionDate': userInstance.planedCompletionDate,
              'dataVerifiedInd': 'N',
              'createDatetime': new Date(),
              'updateDatetime': new Date(),
              'createUserId': 1,
              'updateUserId': 1,
              'campusId': campusId,
            };
          
            enrollment.create(enrollmentObj, function(enrollmentError, enrollmentResponse) {
              // console.log('enrollmenterrrrrrrrrrrrrrrrrrrrrrr ', enrollmentError);
              if (enrollmentError) {
                throwError(enrollmentError, callBack);
              } else {
                callBack(null, enrollmentResponse);
              }
            });
          }
        });
      }
    });
  });
}
exports.studentSignupService = studentSignupService;
