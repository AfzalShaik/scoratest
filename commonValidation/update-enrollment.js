'use strict';
var findEntity = require('../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var server = require('../server/server');
var enrollment = server.models.Enrollment;
var validateModel = require('../ServicesImpl/CommonImpl/models-validation.js').validateInput;
var enrollmentJson = require('../common/models/ENROLLMENT.json').properties;
/**
   *updateEnrollmentService-function deals with updating the enrollment service
   *@constructor
   * @param {object} enroll - contains the id need to get updated
   * @param {function} cb - deals with the response
   */
function updateEnrollmentService(enroll, cb) {
  validateModel(enroll, enrollmentJson, function(err, resp) {
    if (err) {
      throwError(JSON.stringify(error), cb);
    } else {
      if (enroll.dataVerifiedInd === 'N') {
        updateEnrollMethod(enroll, function(err, offCampusResp) {
          if (err) {
            throwError(err, cb);
            logger.error('error while updating offcampus enrollment record');
          } else {
            cb(null, offCampusResp);
          }
        });
      } else if (enroll.dataVerifiedInd === 'Y') {
        if (enroll.admissionNo || enroll.startDate || enroll.cgpaScore || enroll.internshipAvailInd || enroll.summProgAvailInd || enroll.placementAvailInd) {
          throwError('Oncampus students are not allowed to update some fields', cb);
          logger.error('Oncampus students are not allowed to update some fields');
        } else {
          updateEnrollMethod(enroll, function(err, onCampusResp) {
            if (err) {
              throwError(err, cb);
              logger.error('error while updating oncampus enrollment record');
            } else {
              cb(null, onCampusResp);
            }
          });
        }
      }
    }
  });
}
exports.updateEnrollmentService = updateEnrollmentService;

function updateEnrollMethod(enroll, cb) {
  var findObj = {};
  findObj['enrollmentId'] = enroll.enrollmentId;
  findObj['studentId'] = enroll.studentId;
  findObj['programId'] = enroll.programId;
  findEntity(findObj, enrollment, function(error, response) {
    if (error) {
      throwError(error, cb);
      logger.error('Error while fetching enrollment record');
    } else if (response) {
      response.data.updateAttributes(enroll, function(err, info) {
        if (err) {
          throwError('error while updating enrollment', cb);
        } else {
          logger.info('enrollment record Updated Successfully');
          info['requestStatus'] = true;
          info['updateDatetime'] = new Date();
          cb(null, info);
        }
      });
    } else {
      throwError('invalid enrollmentId or studentId or programId', cb);
      logger.error('invalid enrollmentId or studentId or programId');
    }
  });
};
