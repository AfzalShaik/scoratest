'use strict';
var Joi = require('joi');
  /**
   *json validation for every model
   */

// TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Use constants instead of 999999999999999 (something like INT_MAX)
var employerCampusListJson = {
  listId: Joi.number().max(999999999999999).required(),
  companyId: Joi.number().max(999999999999999).required(),
  jobRoleId: Joi.number().max(999999999999999).required(),
  compApprovalStatusValueId: Joi.number().max(999999999999999),
  listName: Joi.string().max(50),
  description: Joi.string().max(100),
  createDatetime: Joi.date().iso(),
  updateDatetime: Joi.date().iso(),
  createUserId: Joi.number().max(999999999999999),
  updateUserId: Joi.number().max(999999999999999),
};
var employerCampusListCompPakJson = {
  listId: Joi.number().max(999999999999999).required(),
  companyId: Joi.number().max(999999999999999).required(),
  listCompPkgId: Joi.number().max(999999999999999).required(),
  compPackageId: Joi.number().max(999999999999999).required(),
  endReason: Joi.string().max(100),
  startDate: Joi.date().iso(),
  endDate: Joi.date().iso(),
  createDatetime: Joi.date().iso(),
  updateDatetime: Joi.date().iso(),
  createUserId: Joi.number().max(999999999999999),
  updateUserId: Joi.number().max(999999999999999),
};
var employerCampusListProgramJson = {
  listCampusId: Joi.number().max(999999999999999).required(),
  companyId: Joi.number().max(999999999999999).required(),
  listId: Joi.number().max(999999999999999).required(),
  campusId: Joi.number().max(999999999999999).required(),
  endReason: Joi.string().max(100),
  startDate: Joi.date().iso(),
  endDate: Joi.date().iso(),
  createDatetime: Joi.date().iso(),
  updateDatetime: Joi.date().iso(),
  createUserId: Joi.number().max(999999999999999),
  updateUserId: Joi.number().max(999999999999999),
};
var departmentJson = {
  campusId: Joi.number().integer().max(999999999999999).required(),
  departmentId: Joi.number().integer().max(999999999999999).required(),
  shortName: Joi.string().max(50),
  name: Joi.string().max(100),
  description: Joi.string().max(200),
  createDatetime: Joi.date().iso(),
  updateDatetime: Joi.date().iso(),
  createUserId: Joi.number().max(999999999999999),
  updateUserId: Joi.number().max(999999999999999),
};
var organizationJson = {
  organizationId: Joi.number().integer().max(999999999999999).required(),
  companyId: Joi.number().integer().max(999999999999999).required(),
  shortName: Joi.string().max(50),
  name: [Joi.string().max(100), Joi.allow(null)],
  description: [Joi.string().max(200), Joi.allow(null)],
  createDatetime: Joi.date().iso(),
  updateDatetime: Joi.date().iso(),
  createUserId: Joi.number().max(999999999999999),
  updateUserId: Joi.number().max(999999999999999),
  facebook: [Joi.string().max(200), Joi.allow(null)],
  twitter: [Joi.string().max(200), Joi.allow(null)],
  youtube: [Joi.string().max(200), Joi.allow(null)],
  linkedin: [Joi.string().max(200), Joi.allow(null)],
  logo: [Joi.string().max(200), Joi.allow(null)],
  brandingImage: [Joi.string().max(200), Joi.allow(null)],
};
var employerDrive = {
  empDriveId: Joi.number().integer().max(999999999999999).required(),
  companyId: Joi.number().integer().max(999999999999999).required(),
  organizationId: Joi.number().integer().max(999999999999999),
  driveName: Joi.string().max(50).required(),
  description: Joi.string().max(100).required(),
  driveTypeValueId: Joi.number().integer().max(999999999999999).required(),
  jobRoleId: Joi.number().integer().max(999999999999999),
  noOfPositions: Joi.number().integer().max(999999999999999),
  startDate: Joi.date().iso(),
  endDate: Joi.date().iso(),
  driveStatusValueId: Joi.number().integer().max(999999999999999).required(),
  createDatetime: Joi.date().iso(),
  updateDatetime: Joi.date().iso(),
  createUserId: Joi.number().max(999999999999999).required(),
  updateUserId: Joi.number().max(999999999999999).required(),
};
var employerEvent = {
  empDriveId: Joi.number().integer().max(999999999999999).required(),
  empEventId: Joi.number().integer().max(999999999999999).required(),
  companyId: Joi.number().integer().max(999999999999999).required(),
  cgpa: Joi.number().integer().max(999),
  eventName: [Joi.string().max(100), Joi.allow(null)],
  eventTypeValueId: Joi.number().integer().max(999999999999999),
  eventStatusValueId: Joi.number().integer().max(999999999999999),
  scheduledDate: Joi.date().iso(),
  scheduledStartTime: Joi.date().iso(),
  duration: Joi.number().integer().max(999999999999999),
  hostName: [Joi.string().max(100), Joi.allow(null)],
  hostContact: [Joi.string().max(100), Joi.allow(null)],
  addressLine1: [Joi.string().max(100), Joi.allow(null)],
  addressLine2: [Joi.string().max(100), Joi.allow(null)],
  addressLine3: [Joi.string().max(100), Joi.allow(null)],
  cityId: Joi.number().integer().max(999999999999999),
  stateCode: Joi.string().max(16),
  countryCode: Joi.string().max(16),
  postalCode: Joi.string().max(16),
  createDatetime: Joi.date().iso(),
  updateDatetime: Joi.date().iso(),
  createUserId: Joi.number().max(999999999999999),
  updateUserId: Joi.number().max(999999999999999),
  eventRequirement: [Joi.string().max(2000), Joi.allow(null)],
};
var employerEventPanel = {
  empEventPanelId: Joi.number().integer().max(999999999999999).required(),
  empEventId: Joi.number().integer().max(999999999999999).required(),
  empDriveId: Joi.number().integer().max(999999999999999).required(),
  companyId: Joi.number().integer().max(999999999999999).required(),
  employerPersonId: Joi.number().integer().max(999999999999999).required(),
  createDatetime: Joi.date().iso(),
  updateDatetime: Joi.date().iso(),
  createUserId: Joi.number().max(999999999999999).required(),
  updateUserId: Joi.number().max(999999999999999).required(),
};

var campusDrive = {
  campusDriveId: Joi.number().integer().max(999999999999999).required(),
  campusId: Joi.number().integer().max(999999999999999).required(),
  driveName: Joi.string().max(50).required(),
  description: Joi.string().max(100).required(),
  driveTypeValueId: Joi.number().integer().max(999999999999999).required(),
  createDatetime: Joi.date().iso(),
  updateDatetime: Joi.date().iso(),
  startDate: Joi.date().iso(),
  endDate: Joi.date().iso(),
  driveStatusValueId: Joi.number().integer().max(999999999999999).required(),
  createUserId: Joi.number().max(999999999999999).required(),
  updateUserId: Joi.number().max(999999999999999).required(),
};

exports.employerCampusListJson = employerCampusListJson;
exports.employerCampusListCompPakJson = employerCampusListCompPakJson;
exports.employerCampusListProgramJson = employerCampusListProgramJson;
exports.departmentJson = departmentJson;
exports.organizationJson = organizationJson;
exports.employerDrive = employerDrive;
exports.employerEvent = employerEvent;
exports.employerEventPanel = employerEventPanel;
exports.campusDrive = campusDrive;
