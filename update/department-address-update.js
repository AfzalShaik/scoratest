var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var allmethods = require("./allmethods");

  var Joi = require('joi');
   /**
   *DepartmentUpdate-function deals with updating the department data
   *@constructor
   * @param {object} departmentData - contains all the data need to get updated
   * @param {function} cb - deals with the response
   */
function DepartmentUpdate (departmentData,cb){

  if(departmentData){
    if(departmentData.createDatetime || departmentData.updateDatetime || departmentData.createUserId || departmentData.updateUserId){
      var error = new Error("You cannot perform update operation: ");
      error.statusCode = 422;
      error.requestStatus=false;
       cb(error, departmentData);
    }else{
      if(departmentData.campusId && departmentData.departmentId && departmentData.addressId){


        console.log("validating fields: ",departmentData);
        //validating input json fields
        var schema = Joi.object().keys({
          campusId: Joi.number().integer().max(999999999999999).required(),
          departmentId:Joi.number().integer().max(999999999999999).required(),
          addressId: Joi.number().max(999999999999999).required(),
          addressTypeValueId:Joi.number().max(999999999999999),
          addressLine1:Joi.string().max(100),
          addressLine2:Joi.string().max(100),
          addressLine3:Joi.string().max(100),
          postalCode:Joi.string().max(16),
          stateCode:Joi.string().max(16),
          cityId: Joi.number().max(999999999999999),
          countryCode:Joi.string().max(16),
      });

      Joi.validate(departmentData, schema, function (err, value) {

        if(err){
           cb(err, departmentData);
        }else{

          var cityValue;
          var stateValue;
          var countryValue;
            var typeCodeVal;
           var DepartmentAddress = server.models.DepartmentAddress;
           var Address = server.models.Address;
           //addressTypeCodeFunction to verify addressTypeValueId
             allmethods.addressTypeCodeFunction(departmentData,function(typeCodeCheck){
               typeCodeVal = typeCodeCheck;

               //addressFunction function verifies city id , countryCode and stateCode
                 allmethods.addressFunction(departmentData,function(cityCheck){
                  cityValue = cityCheck;
                   if(cityValue==false){
                     var error = new Error(" cityId and stateCode and countryCode required and should be valid");
                     error.statusCode = 422;
                     error.requestStatus=false;
                     cb(error, departmentData);
                   }else{
              if(cityValue==true && typeCodeVal==true){

                DepartmentAddress.findOne({"where":{"and":[{"campusId": departmentData.campusId},{"addressId": departmentData.addressId},{"departmentId":departmentData.departmentId}]}}, function (err, DepartmentList) {

                  if(err){
                    //throws error
                     cb(err, departmentData);
                  }else if (DepartmentList){
                    //checking record in address table with addressId
                    Address.findOne({'where':{"addressId": departmentData.addressId}}, function (err, AddressResp) {
                      //if record found it will the  updateAttributes
                      AddressResp.updateAttributes(departmentData,function(err, info){

                        if(err){
                          cb(err,info)
                        }else{
                          //making requestStatus is true;
                          logger.info("Department Address Updated Successfully");
                          info["requestStatus"] = true;
                          info["updateDatetime"] = new Date();

                          cb(null, info);
                        }

                      })
                    })

                  }
                  else{
                    //throws error incase of invalid address_id or campusId
                    var error = new Error("Invalid address_id or campusId or departmentId");
                    logger.error("Invalid address_id or campusId or departmentId");
                    error.statusCode = 422;
                    error.requestStatus=false;
                     cb(error, departmentData);
                  }
                })
              }else{
                //throws error in case of Invalid input
                logger.error("Invalid addressTypeValueId");
                var error = new Error("Invalid addressTypeValueId");
                error.statusCode = 422;
                error.requestStatus=false;
                 cb(error, departmentData);
              }
            }

          })
            })
        }
      })
    }else{

      var error = new Error("campusId and departmentId and addressId is required ");
      error.statusCode = 422;
      error.requestStatus=false;
       cb(error, departmentData);
    }

    }
  }else{
    logger.error("Input can't be Null");
    var error = new Error("Input is Null: ");
      error.statusCode = 422;
      error.requestStatus=false;
       cb(error, departmentData);
  }
 }

exports.DepartmentUpdate = DepartmentUpdate;
