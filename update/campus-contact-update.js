var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var allmethods = require("./allmethods");
var Joi = require('joi');
 /**
   *updateCampusContact-function deals with updating the campus contact
   *@constructor
   * @param {object} contactData - contains all the data need to get updated
   * @param {function} cb - deals with the response
   */
function updateCampusContact (contactData,cb){
  //validating the input
  if(contactData.createDatetime || contactData.updateDatetime || contactData.createUserId || contactData.updateUserId){
    var error = new Error("You cannot perform update operation: ");
      error.statusCode = 422;
      error.requestStatus=false;
       cb(error, contactData);
  }else{
    //validating the input json
    var schema = Joi.object().keys({
        campusId: Joi.number().integer().max(999999999999999).required(),
        contactId: Joi.number().max(999999999999999).required(),
        contactTypeValueId:Joi.number().max(999999999999999),
        contactInfo:Joi.string().max(100),
    });
    Joi.validate(contactData, schema, function (err, value) {
      if(err){
        cb(err,contactData)
      }else{
        var typeCodeValue;
         var CampusContact = server.models.CampusContact;
          var Contact = server.models.Contact;
         //update technique for campus contact
         allmethods.contactTypeCodeFunction(contactData,function(typeCodeCheck){
           typeCodeValue = typeCodeCheck;
           if(typeCodeValue==false){
             var error = new Error("contactTypeValueId is not valid");
             error.statusCode = 422;
             error.requestStatus=false;
             cb(error, contactData);
           } else{

                //checking the record with campusId and contactId
                CampusContact.findOne({'where':{"and":[{"campusId": contactData.campusId},{"contactId": contactData.contactId}]}}, function (err, contactList) {
                  if(err){
                     cb(err, contactData);
                  }else if (contactList){
                    //checking record in campus contact table with contact id
                      Contact.findOne({'where':{"contactId": contactData.contactId}}, function (err, CampusResp) {
                        //if record found it will update the attributes
                        CampusResp.updateAttributes(contactData,function(err, info){
                          if(err){
                            cb(err,info)
                          }else{
                            //if update is successful requestStatus will become true
                            info["requestStatus"] = true;
                            info["updateDatetime"] = new Date();
                            cb(null, info);
                          }

                        })
                      })

                  }
                  else{
                    //throws error in case of Invalid contatcId or campusId
                    var error = new Error("Invalid contatcId or campusId ");
                    error.statusCode = 422;
                    error.requestStatus=false;
                     cb(error, contactData);
                  }
                })
              }
            })
      }
    })
  }

}
exports.updateCampusContact = updateCampusContact;
