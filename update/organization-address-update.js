var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var allmethods = require("./allmethods");

  var Joi = require('joi');
   /**
   *OrganizationAddressUpdate-function deals with updating organization address
   *@constructor
   * @param {object} organizationData - contains all the data need to get updated
   * @param {function} cb - deals with the response
   */
function OrganizationAddressUpdate (organizationData,cb){

  if(organizationData){
    if(organizationData.createDatetime || organizationData.updateDatetime || organizationData.createUserId || organizationData.updateUserId){
      var error = new Error("You cannot perform update operation: ");
      error.statusCode = 422;
      error.requestStatus=false;
       cb(error, organizationData);
    }else{
        //  console.log("validating fields: ",organizationData);
      if(organizationData.companyId && organizationData.organizationId && organizationData.addressId){


        // console.log("validating fields: ",organizationData);
        //validating input json fields
        var schema = Joi.object().keys({
          companyId: Joi.number().integer().max(999999999999999).required(),
          organizationId:Joi.number().integer().max(999999999999999).required(),
          addressId: Joi.number().max(999999999999999).required(),
          addressTypeValueId:Joi.number().max(999999999999999),
          addressLine1:Joi.string().max(100),
          addressLine2:Joi.string().max(100),
          addressLine3:Joi.string().max(100),
          postalCode:Joi.string().max(16),
          stateCode:Joi.string().max(16),
          cityId: Joi.number().max(999999999999999),
          countryCode:Joi.string().max(16),
      });

      Joi.validate(organizationData, schema, function (err, value) {

        if(err){
           cb(err, organizationData);
        }else{

          var cityValue;
          var stateValue;
          var countryValue;
            var typeCodeVal;
           var OrganizationAddress = server.models.OrganizationAddress;
           var Address = server.models.Address;
           //addressTypeCodeFunction to verify addressTypeValueId
             allmethods.addressTypeCodeFunction(organizationData,function(typeCodeCheck){
               typeCodeVal = typeCodeCheck;

               //addressFunction function verifies city id , countryCode and stateCode
                 allmethods.addressFunction(organizationData,function(cityCheck){
                  cityValue = cityCheck;
                   if(cityValue==false){
                     var error = new Error(" cityId and stateCode and countryCode required and should be valid");
                     error.statusCode = 422;
                     error.requestStatus=false;
                     cb(error, organizationData);
                   }else{
              if(cityValue==true && typeCodeVal==true){

                OrganizationAddress.findOne({"where":{"and":[{"companyId": organizationData.companyId},{"addressId": organizationData.addressId},{"organizationId":organizationData.organizationId}]}}, function (err, OrgList) {

                  if(err){
                    //throws error
                     cb(err, organizationData);
                  }else if (OrgList){
                    //checking record in address table with addressId
                    Address.findOne({'where':{"addressId": organizationData.addressId}}, function (err, AddressResp) {
                      //if record found it will the  updateAttributes
                      AddressResp.updateAttributes(organizationData,function(err, info){

                        if(err){
                          cb(err,info)
                        }else{
                          //making requestStatus is true;
                          logger.info("Organization Address Updated Successfully");
                          info["requestStatus"] = true;
                          info["updateDatetime"] = new Date();

                          cb(null, info);
                        }

                      })
                    })

                  }
                  else{
                    //throws error incase of invalid address_id or companyId
                    var error = new Error("Invalid address_id or companyId or organizationId");
                    logger.error("Invalid address_id or companyId or organizationId");
                    error.statusCode = 422;
                    error.requestStatus=false;
                     cb(error, organizationData);
                  }
                })
              }else{
                //throws error in case of Invalid input
                logger.error("Invalid addressTypeValueId");
                var error = new Error("Invalid addressTypeValueId");
                error.statusCode = 422;
                error.requestStatus=false;
                 cb(error, organizationData);
              }
            }

          })
            })
        }
      })
    }else{

      var error = new Error("companyId and organizationId and addressId is required ");
      error.statusCode = 422;
      error.requestStatus=false;
       cb(error, organizationData);
    }

    }
  }else{
    logger.error("Input can't be Null");
    var error = new Error("Input is Null: ");
      error.statusCode = 422;
      error.requestStatus=false;
       cb(error, organizationData);
  }
 }

exports.OrganizationAddressUpdate = OrganizationAddressUpdate;
