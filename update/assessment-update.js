'use strict';
var server = require('../server/server');
var validateModel = require('../ServicesImpl/CommonImpl/models-validation.js').validateInput;
var lookupValidation = require('../commonValidation/lookupMethods.js').validateLookups;
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var findEntity = require('../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
var assessment = server.models.Assessment;
 /**
   *updateAssessment-function deals with updating the assessment details
   *@constructor
   * @param {object} input - contains all the data need to get updated
   * @param {function} cb - deals with the response
   */
function updateAssessment(input, cb) {
  if (input) {
    if (input.assessmentId) {
      var inputData = input;
      findAndUpdate(inputData, function(error, output) {
        if (error) {
          throwError(JSON.stringify(error), cb);
        } else {
          cb(null, output);
        }
      });
    } else {
      throwError('Invalid Input', cb);
    }
  } else {
    throwError('Invalid Input', cb);
  }
};
 /**
   *findAndUpdate-function deals with finding and updating the details
   *@constructor
   * @param {object} input - contains all the data need to get updated
   * @param {function} cb - deals with the response
   */
function findAndUpdate(input, cb) {
  var inputFilterObject = {};
  inputFilterObject['assessmentId'] = input.assessmentId;
  findEntity(inputFilterObject, assessment, function(error, response) {
    if (error) {
      throwError(error, cb);
    } else if (response) {
      if (response.data.dataVerifiedInd == 'Y' || response.data.dataVerifiedInd == 'y') {
        delete input.score;
        response.data.updateAttributes(input, function(err, info) {
          if (err) {
            throwError(err, cb);
          } else {
            logger.info('assessment record Updated Successfully');
            info['requestStatus'] = true;
            info['updateDatetime'] = new Date();
            cb(null, info);
          }
        });
      } else {
        response.data.updateAttributes(input, function(err, info) {
          if (err) {
            throwError(err, cb);
          } else {
            logger.info('assessment record Updated Successfully');
            info['requestStatus'] = true;
            info['updateDatetime'] = new Date();
            cb(null, info);
          }
        });
      }
    } else {
      throwError('Invalid assessmentId', cb);
    }
  });
};

/*
//   if (assessmentData) {
// //checks that user is attempting the fields that are not accpected
//     if (assessmentData.createDatetime || assessmentData.updateDatetime || assessmentData.createUserId || assessmentData.updateUserId) {
//       var error = new Error('You cannot perform update operation: ');
//       error.statusCode = 422;
//       error.requestStatus = false;
//       cb(error, assessmentData);
//     } else {
//       if (assessmentData.assessmentId) {
//         var schema = Joi.object().keys({
//           assessmentId: Joi.number().integer().max(999999999999999).required(),
//           score: Joi.number().max(100),
//           subjectDetails: Joi.string().max(100),
//           highlights: Joi.string().max(100),
//         });
//         Joi.validate(assessmentData, schema, function(err, value) {
//           if (err) {
//             cb(err, assessmentData);
//           }
//           else {
//             var score, subjectDetails, highlights;
//             assessmentCode(assessmentData, function(err, res) {
//               var i = res.statusCode;
//               if (err) {
//                 var error = new Error('error while updatig: ');
//                 error.statusCode = 422;
//                 error.requestStatus = false;
//                 cb(error, assessmentData);
//               }
//               else if (res.statusCode == 422) {
//                 var error = new Error('Your response was null make sure with the provided details');
//                 error.statusCode = 422;
//                 error.requestStatus = false;
//                 cb(null, res);
//                // callbackAssessmentFunction(null, res, null);
//               }
//               else {
//                 assessmentData.requestStatus = true;
//                 cb(null, assessmentData);
//               }
//             });
//           }
//         });
//       } else {
//         var error = new Error('You need to provide assessmentId to update any operation: ');
//         error.statusCode = 422;
//         error.requestStatus = false;
//         cb(error, assessmentData);
//       }
//     }
//   }
// }

// /**
//  * function which takes care of finding-
//  *-the correct assessment id and updating that assessment id releated fields
//  * @param {object} assessmentData - total data user entered
//  * @param {function} callbackAssessmentFunction -acts as an assessment function
//  */
// function assessmentCode(assessmentData, callbackAssessmentFunction) {
//   var assessmentValue = false;
//   if (assessmentData.assessmentId) {
//     var assessmentModel = server.models.Assessment;
//  // for finding data using assessment id
//     assessmentModel.find({
//       'where': {
//         'assessmentId': assessmentData.assessmentId,
//       },
//     }, function(err, res) {
//       if (res == 0) {
//         assessmentValue = false;
//         res.statusCode = 422;
//         callbackAssessmentFunction(null, res);
//       } else if (err) {
//         assessmentValue = false;
//         callbackAssessmentFunction(err, null);
//       }
//       else {
//         var ver = res[0].dataVerifiedInd;
//         //checks for data verified field
//         if (ver == 'Y' || ver == 'y') {
//           delete assessmentData.score;
//           //updates all the fields user entered except score since dataverified was yes
//           assessmentModel.updateAll({assessmentId: assessmentData.assessmentId}, assessmentData, function(err, r) {
//             if (err) {
//               callbackAssessmentFunction(err, null);
//             }
//             else {
//               assessmentValue = true;
//               assessmentData['updateDatetime'] = new Date();
//               callbackAssessmentFunction(null, assessmentData);
//             }
//           }
//                     );
//         } else {
//           //will be in tjis if the data verified was other than Y or y
//           //updates every field user entered even score since data verified was N or n
//           assessmentModel.updateAll({assessmentId: assessmentData.assessmentId}, assessmentData, function(err, r) {
//             if (err) {
//               callbackAssessmentFunction(err, null);
//             }
//             else {
//               assessmentData['updateDatetime'] = new Date();
//               assessmentValue = true;
//               callbackAssessmentFunction(null,  assessmentData);
//             }
//           }
//           );
//         }
//       }
//     }
//     );
//   }
// }
exports.updateAssessment = updateAssessment;
