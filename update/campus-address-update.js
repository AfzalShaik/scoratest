var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var allmethods = require('./allmethods');
var Joi = require('joi');
 /**
   *updateCampus-function deals with updating the campus data
   *@constructor
   * @param {object} campusData - contains all the data need to get updated
   * @param {function} cb - deals with the response
   */
// TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Isn't this logic already duplicated via Joi validations elsewhere ?
function updateCampus(campusData, cb) {
  if (campusData) {
    if (campusData.createDatetime ||
      campusData.updateDatetime ||
      campusData.createUserId ||
      campusData.updateUserId) {
      var error = new Error('You cannot perform update operation: ');
      error.statusCode = 422;
      error.requestStatus = false;
      cb(error, campusData);
    } else {
      if (campusData.campusId &&  campusData.addressId) {
          //validating input json fields
        var schema = Joi.object().keys({
          campusId: Joi.number().integer().max(999999999999999).required(),
          addressId: Joi.number().max(999999999999999).required(),
          addressTypeValueId: Joi.number().max(999999999999999),
          addressLine1: Joi.string().max(100),
          addressLine2: Joi.string().max(100),
          addressLine3: Joi.string().max(100),
          postalCode: Joi.string().max(16),
          stateCode: Joi.string().max(16),
          cityId: Joi.number().max(999999999999999),
          countryCode: Joi.string().max(16),
        });

        Joi.validate(campusData, schema, function(err, value) {
          if (err) {
            cb(err, campusData);
          } else {
            var cityValue, stateValue, countryValue, typeCodeVal;
            var CampusAddress = server.models.CampusAddress;
            var Address = server.models.Address;
             //addressTypeCodeFunction to verify addressTypeValueId
            allmethods.addressTypeCodeFunction(campusData, function(typeCodeCheck) {
              typeCodeVal = typeCodeCheck;

            //addressFunction function verifies city id , countryCode and stateCode
              allmethods.addressFunction(campusData, function(cityCheck) {
                cityValue = cityCheck;
                if (cityValue == false) {
                  var error = new Error(' cityId and stateCode and countryCode required and should be valid');
                  error.statusCode = 422;
                  error.requestStatus = false;
                  cb(error, campusData);
                } else {
                  if (typeCodeVal == true && cityValue == true) {
                    CampusAddress.findOne({'where': {'and': [{'campusId': campusData.campusId}, {'addressId': campusData.addressId}]}}, function(err, campusList) {
                      if (err) {
                      //throws error
                        cb(err, campusData);
                      } else if (campusList) {
                      //checking record in address table with addressId
                        Address.findOne({'where': {'addressId': campusData.addressId}}, function(err, AddressResp) {
                        //if record found it will the  updateAttributes
                          AddressResp.updateAttributes(campusData, function(err, info) {
                            if (err) {
                              cb(err, info);
                            } else {
                            //making requestStatus is true;
                              logger.info('Campus Address Updated Successfully');
                              info['requestStatus'] = true;
                              info['updateDatetime'] = new Date();
                              cb(null, info);
                            }
                          });
                        });
                      }
                    else {
                      //throws error incase of invalid address_id or campusId
                        var error = new Error('Invalid address_id or campusId ');
                        logger.error('Invalid address_id or campusId');
                        error.statusCode = 422;
                        error.requestStatus = false;
                        cb(error, campusData);
                      }
                    });
                  } else {
                  //throws error in case of Invalid input
                    logger.error('Invalid addressTypeValueId');
                    var error = new Error('Invalid addressTypeValueId ');
                    error.statusCode = 422;
                    error.requestStatus = false;
                    cb(error, campusData);
                  }
                }
              });
            });
          }
        });
      } else {
        var error = new Error('campusId and  addressId are required');
        error.statusCode = 422;
        error.requestStatus = false;
        cb(error, campusData);
      }
    }
  } else {
    logger.error("Input can't be Null");
    var error = new Error('Input is Null: ');
    error.statusCode = 422;
    error.requestStatus = false;
    cb(error, campusData);
  }
}
exports.updateCampus = updateCampus;
