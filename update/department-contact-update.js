var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var allmethods = require("./allmethods");
var Joi = require('joi');
 /**
   *updateDepartmentContact-function deals with updating the department contact
   *@constructor
   * @param {object} contactData - contains all the data need to get updated
   * @param {function} cb - deals with the response
   */
function updateDepartmentContact (contactData,cb){
   console.log("contactData::::::::::::------------------------------- ",contactData);
  //validating the input
  if(contactData.createDatetime || contactData.updateDatetime || contactData.createUserId || contactData.updateUserId){
    var error = new Error("You cannot perform update operation: ");
      error.statusCode = 422;
      error.requestStatus=false;
       cb(error, contactData);
  }else{
    //validating the input json
    var schema = Joi.object().keys({
        departmentId: Joi.number().integer().max(999999999999999).required(),
        campusId: Joi.number().integer().max(999999999999999).required(),
        contactId: Joi.number().max(999999999999999).required(),
        contactTypeValueId:Joi.number().max(999999999999999),
        contactInfo:Joi.string().max(100),
    });
    Joi.validate(contactData, schema, function (err, value) {
      if(err){
        cb(err,contactData)
      }else{
        var typeCodeValue;
         var DepartmentContact = server.models.DepartmentContact;
          var Contact = server.models.Contact;
          //checking record with campusId,departmentId and contactId
          allmethods.contactTypeCodeFunction(contactData,function(typeCodeCheck){
            typeCodeValue = typeCodeCheck;
            if(typeCodeValue==false){
              var error = new Error("contactTypeValueId is not valid");
              error.statusCode = 422;
              error.requestStatus=false;
              cb(error, contactData);
            } else{
               DepartmentContact.findOne({'where':{"and":[{"campusId": contactData.campusId},{"departmentId":contactData.departmentId},{"contactId": contactData.contactId}]}}, function (err, departmentList) {
                 if(err){
                    cb(err, contactData);
                 }else if (departmentList){
                   console.log("departmentList--------------------- ",departmentList);

                   //checking record with contactId in contact table
                     Contact.findOne({'where':{"contactId": contactData.contactId}}, function (err, DepartmentResp) {
                       //update the contact table
                       if(err){
                         cb(err,DepartmentResp)
                       }else{
                         DepartmentResp.updateAttributes(contactData,function(err, infoDepartment){
                           if(err){
                             cb(err,infoDepartment)
                           }else{
                             //if success requestStatus will becomes true
                             infoDepartment["requestStatus"] = true;
                             infoDepartment["updateDatetime"] = new Date();
                             cb(null, infoDepartment);
                           }

                         })
                       }

                     })

                 }
                 else{
                   //throws error when invalid contatcId or departmentId or campusId
                   var error = new Error("Invalid contactId or campusId or departmentId ");
                   error.statusCode = 422;
                   error.requestStatus=false;
                    cb(error, contactData);
                 }
               })
             }
           })
      }
    })
  }

}
exports.updateDepartmentContact = updateDepartmentContact;
