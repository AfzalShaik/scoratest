
'use strict';
var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var errorResponse = require('../ErrorHandling/customizeErrorTypes.js').errorFunction;
var dataToExport = [];
var unregStudentData = [];
var finaldata = [];
var companyName, eventName;
var async = require('async');
/**
 *getStudentData- for getting the student data
 *@constructor
 * @param {object} data - contains data of company,campus,employer details
 * @param {function} studentDetailsCallback - deals with response
 */
function getSkills(input, skillsCB) {
  var studentSkills = server.models.StudentSkills;
  studentSkills.find({
    'where': {
      'studentId': input.studentId,
    },
  }, function(skillsErr, skillsResp) {
    if (skillsResp) {
      async.map(skillsResp, getSkillNames, function(nameErr, names) {
        skillsCB(null, names);
      });
    } else {
      skillsCB(null, names);
    }
  });

  function getSkillNames(obj, nameCB) {
    var lookupValue = server.models.LookupValue;
    lookupValue.findOne({
      'where': {
        'lookupValueId': obj.skillTypeValueId,
      },
    }, function(lookErr, lookValue) {
      // console.log('000000000000000 ', obj);
      nameCB(null, lookValue.lookupValue);
    });
  }
}

function getInterests(input, interestCB) {
  var studentInterests = server.models.StudentInterests;
  studentInterests.find({
    'where': {
      'studentId': input.studentId,
    },
  }, function(interestsErr, interestsResp) {
    if (interestsResp) {
      async.map(interestsResp, getInterestNames, function(nameErr, names) {
        interestCB(null, names);
      });
    } else {
      interestCB(null, null);
    }
  });

  function getInterestNames(obj, nameCB) {
    // console.log('dataaaaaaaaaaaaaa ', obj);
    var lookupValue = server.models.LookupValue;
    lookupValue.findOne({
      'where': {
        'lookupValueId': obj.interestTypeValueId,
      },
    }, function(lookErr, lookValue) {
      nameCB(null, lookValue.lookupValue);
    });
  }
}
var getStudentData = function(data, studentDetailsCallback) {
  // console.log('event', data);
  var eventList = server.models.EventStudentList;
  var eventObj = {};
  if (data.flag == 'campusEvent') {
    eventObj['campusId'] = data.campusId;
    eventObj['campusEventId'] = data.eventId;
    eventObj['campusPublishInd'] = 'Y';
  } else {
    eventObj['companyId'] = data.companyId;
    eventObj['employerEventId'] = data.eventId;
    eventObj['campusPublishInd'] = 'Y';
  }
  // console.log('eventobjjjjjjjjjjjjjjjj ', eventObj);
  eventList.find({
    'where': {
      'and': [eventObj],
    },
  },
    function(err, response) {
      if (err) {
        errorResponse('there was an error', studentDetailsCallback);
      } else {
        var student = server.models.Student;
        var user = server.models.ScoraUser;
        var unregStudent = server.models.UnregisterCampusStudent;
        var studentInterests = server.models.StudentInterests;
        // console.log('responseeeeeeeeeeeeeeeeeeee ', data);
        async.map(response, getStudentInfo, function(studentErr, studentInformation) {
          // console.log('studentInformationnnnnnnnnnnn', studentInformation);
          if (data.flag == 'empEvent') {
            // convertToCsv(studentInformation);
            var json2csv = require('json2csv');
            var fs = require('fs');
            var fileName;
            // var randomstring = require('randomstring');
            // fileName = randomstring.generate({
            //   length: 12,
            //   charset: 'alphabetic',
            // });
            // var company = server.models.Company;
            // company.findOne({'where' : {'companyId'}})
            // console.log('--------------------- ', data);
            fileName = data.CompanyName + '_' + data.EventName;
            fileName = './../../attachments/company-upload' + '/' + fileName + '.csv';
            // console.log(fileName);
            var fields = ['CompanyName', 'EventName', 'FirstName', 'MiddleName', 'LastName', 'Email', 'ScreeningStatus'];
            var csv = json2csv({
              data: studentInformation,
              fields: fields,
            });
            fs.writeFile(fileName, csv, function(err) {
              if (err) throw err;
            });
          } else {
            var json2csv = require('json2csv');
            var fs = require('fs');
            var fileName;
            // var randomstring = require('randomstring');
            // fileName = randomstring.generate({
            //   length: 12,
            //   charset: 'alphabetic',
            // });
            // console.log('insideeeeeeeeeeeeee ', data);
            fileName = data.campusName + '_' + data.eventName;
            fileName = './../../attachments/company-upload' + '/' + fileName + '.csv';
            // console.log('..', fileName);
            var fields = ['CampusName', 'EventName', 'FirstName', 'MiddleName', 'LastName', 'Email', 'Skills', 'Interests', 'ScreeningStatus'];
            var csv = json2csv({
              data: studentInformation,
              fields: fields,
            });
            fs.writeFile(fileName, csv, function(err) {
              if (err) throw err;
            });
          }
          var filePath = {};
          filePath['exportPath'] = fileName;
          studentDetailsCallback(null, filePath);
        });
        // getStudentInfo Functionality
        function getStudentInfo(studentDetails, studentCb) {
          var student = server.models.Student;
          var user = server.models.ScoraUser;
          // console.log('===================== ', studentDetails.registrationInd);
          if (studentDetails.registrationInd == 'Y' || studentDetails.registrationInd == 'y') {
            student.findById(studentDetails.studentId, function(err, studentDetailsResponse) {
              if (err) {
                studentCb(err, null);
              } else {
                user.findById(studentDetailsResponse.id, function(err, userResponse) {
                  getSkills(studentDetailsResponse, function(studentErr, studentSkills) {
                    getInterests(studentDetailsResponse, function(interestErr, studentInterests) {
                      var employerEvent = server.models.EmployerEvent;
                      var needtoInsert = {};
                      needtoInsert.CompanyName = data.CompanyName;
                      // needtoInsert.companyId = data.companyId;
                      // needtoInsert.CampusName = data.CampusName;
                      needtoInsert.CampusName = data.campusName;
                      needtoInsert.EventName = (data.eventName) ? data.eventName : data.EventName;
                      // needtoInsert.empEventId = data.eventId;
                      // needtoInsert.studentId = studentDetailsResponse.studentId;
                      needtoInsert.FirstName = studentDetailsResponse.firstName;
                      needtoInsert.MiddleName = studentDetailsResponse.middleName;
                      needtoInsert.LastName = studentDetailsResponse.lastName;
                      needtoInsert.Email = userResponse.email;
                      needtoInsert.Skills = studentSkills.toString();
                      needtoInsert.Interests = studentInterests.toString();
                      if (studentDetails.candidateStatusValueId == 376) {
                        needtoInsert.ScreeningStatus = 'Shortlisted/Rejected/On Hold';
                      } else if (studentDetails.candidateStatusValueId == 377) {
                        needtoInsert.ScreeningStatus = 'Shortlisted';
                      } else if (studentDetails.candidateStatusValueId == 378) {
                        needtoInsert.ScreeningStatus = 'Rejected';
                      } else if (studentDetails.candidateStatusValueId == 379) {
                        needtoInsert.ScreeningStatus = 'On Hold';
                      } else if (studentDetails.candidateStatusValueId == 380) {
                        needtoInsert.ScreeningStatus = 'Interview in progress';
                      } else if (studentDetails.candidateStatusValueId == 381) {
                        needtoInsert.ScreeningStatus = 'Offered';
                      } else if (studentDetails.candidateStatusValueId == 514) {
                        needtoInsert.ScreeningStatus = 'Initial';
                      } else {
                        needtoInsert.ScreeningStatus = 'Shortlisted/Rejected/On Hold';
                      }
                      // needtoInsert.ScreeningStatus = 'Shortlisted/Rejected/On Hold';
                      studentCb(null, needtoInsert);
                    });
                  });
                });
              }
            });
          } else {
            var unregStudent = server.models.UnregisterCampusStudent;
            unregStudent.findById(studentDetails.studentId, function(err, unregResponse) {
              if (err) {
                errorResponse('there was an error', studentCb);
              } else {
                var needtoInsert = {};
                needtoInsert.CompanyName = data.CompanyName;
                // needtoInsert.campusId = data.campusId;
                needtoInsert.CampusName = data.campusName;
                // needtoInsert.companyId = data.companyId;
                needtoInsert.EventName = data.EventName;
                // needtoInsert.empEventId = data.eventId;
                // needtoInsert.studentId = unregResponse.unregStudentId;
                needtoInsert.FirstName = unregResponse.firstName;
                needtoInsert.MiddleName = unregResponse.middleName;
                needtoInsert.LastName = unregResponse.lastName;
                needtoInsert.Email = unregResponse.emailId;
                needtoInsert.Skills = [];
                needtoInsert.Interests = [];
                if (studentDetails.candidateStatusValueId == 376) {
                  needtoInsert.ScreeningStatus = 'Shortlisted/Rejected/On Hold';
                } else if (studentDetails.candidateStatusValueId == 377) {
                  needtoInsert.ScreeningStatus = 'Shortlisted';
                } else if (studentDetails.candidateStatusValueId == 378) {
                  needtoInsert.ScreeningStatus = 'Rejected';
                } else if (studentDetails.candidateStatusValueId == 379) {
                  needtoInsert.ScreeningStatus = 'On Hold';
                } else if (studentDetails.candidateStatusValueId == 380) {
                  needtoInsert.ScreeningStatus = 'Interview in progress';
                } else if (studentDetails.candidateStatusValueId == 381) {
                  needtoInsert.ScreeningStatus = 'Offered';
                } else if (studentDetails.candidateStatusValueId == 514) {
                  needtoInsert.ScreeningStatus = 'Initial';
                } else {
                  needtoInsert.ScreeningStatus = 'Shortlisted/Rejected/On Hold';
                }
                // console.log('---------------------- ', data, studentDetails);
                // console.log('...................', needtoInsert);
                studentCb(null, needtoInsert);
              }
            });
          }
        }
      }
    });
};
/**
 *convertToCsv- To convert the data to csv
 *@constructor
 * @param {object} data - contains data need to be converted as csv
 * @param {function} done - deals with response
 */
var convertToCsv = function(data, done) {
  // console.log(data);
  // console.log('this is final data', data);
  var json2csv = require('json2csv');
  var fs = require('fs');
  var fields = ['CompanyName', 'EventName', 'FirstName', 'MiddleName', 'LastName', 'Email', 'ScreeningStatus'];
  var csv = json2csv({
    data: data,
    fields: fields,
  });
  fs.writeFile('./commonValidation/exported.csv', csv, function(err) {
    if (err) throw err;
  });
};

var convertToCsv1 = function(data, done) {
  // console.log(data);
  // console.log('this is final data', data);
  var json2csv = require('json2csv');
  var fs = require('fs');
  var fields = ['CampusName', 'EventName', 'FirstName', 'MiddleName', 'LastName', 'Email', 'Skills', 'Interests', 'ScreeningStatus'];
  var csv = json2csv({
    data: data,
    fields: fields,
  });
  fs.writeFile('./commonValidation/exported.csv', csv, function(err) {
    if (err) throw err;
  });
};
/**
 *exportData- To export data with the input as employerEventId
 *@constructor
 * @param {number} employerEventId - contains employer event Id
 * @param {function} employerCallBack - deals with response
 */
var exportData = function(employerEventId, employerCallBack) {
  var eventList = server.models.EventStudentList;
  eventList.find({
    'where': {
      'employerEventId': employerEventId,
    },
  }, function(err, employerRes) {
    if (err) {
      errorResponse('error occured', employerCallBack);
    } else if (employerRes == 0) {
      errorResponse('there was no event with that employer id', employerCallBack);
    } else {
      var employerDrive = server.models.EmployerDriveCampuses;
      employerDrive.findOne({
        'where': {
          'employerEventId': employerEventId,
        },
      }, function(err, response) {
        if (err) {
          errorResponse('error occured', employerCallBack);
        } else {
          var campus = server.models.Campus;
          campus.findOne({
            'where': {
              'campusId': response.campusId,
            },
          }, function(err, campusResponse) {
            if (err) {
              errorResponse('error occured', employerCallBack);
            } else {
              var employerEvent = server.models.EmployerEvent;
              employerEvent.findById(employerEventId, function(err, employerResponse) {
                if (err) {
                  errorResponse('error occured', employerCallBack);
                } else {
                  var company = server.models.Company;
                  company.findById(employerResponse.companyId, function(err, companyResponse) {
                    if (err) {
                      errorResponse('error occured', employerCallBack);
                    } else {
                      var data = {};
                      data.campusId = campusResponse.campusId;
                      data.campusName = campusResponse.name;
                      data.EventName = employerResponse.eventName;
                      data.eventId = employerResponse.empEventId;
                      data.companyId = companyResponse.companyId;
                      data.CompanyName = companyResponse.name;
                      data.flag = 'empEvent';
                      getStudentData(data, function(err, studentResponse) {
                        if (err) {
                          errorResponse('there was an error', employerCallBack);
                        } else {
                          employerCallBack(null, studentResponse);
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
};
/**
 *exportDetails- To export to csv by taking campusEventId as an input
 *@constructor
 * @param {number} campusEventId - unique id for each and every event done by campus
 * @param {function} campusCallback - deals with response
 */
var exportDetails = function(campusEventId, campusCallback) {
  var campusEvent = server.models.CampusEvent;
  campusEvent.findById(campusEventId, function(err, campusEventResponse) {
    if (err) {
      errorResponse('there was an error', campusCallback);
    } else {
      var campus = server.models.Campus;
      campus.findById(campusEventResponse.campusId, function(err, campusDetails) {
        if (err) {
          errorResponse('there was an error', campusCallback);
        } else {
          var company = server.models.Company;
          company.findById(campusEventResponse.companyId, function(err, companyDetails) {
            if (err) {
              errorResponse('there was no company id in that event', campusCallback);
            } else {
              // console.log(campusEventResponse);
              var data = {};
              data.campusId = campusDetails.campusId;
              data.campusName = campusDetails.name;
              data.eventName = campusEventResponse.eventName;
              data.eventId = (campusEventResponse.campusEventId) ? campusEventResponse.campusEventId : campusEventResponse.employerEventId;
              data.companyId = companyDetails.companyId;
              data.companyName = companyDetails.name;
              data.flag = 'campusEvent';
              // console.log('campuseventdataaaaaaaaaaaaaaaa', data);
              getStudentData(data, function(err, studentResponse) {
                if (err) {
                  errorResponse('there was an error', campusCallback);
                } else {
                  campusCallback(null, studentResponse);
                }
              });
            }
          });
        }
      });
    }
  });
};
exports.exportDetails = exportDetails;
exports.exportData = exportData;
// var createCsv = function(data, studentDet, callBack) {
//   var async = require('async');
//   async.map(studentDet, filterDetails, function(err, res) {
//     if (err) {
//       errorResponse('there was an error', callBack);
//     } else {
//       console.log('this is an response', res);
//       callBack(null, res);
//     }
//   });
// };
// var filterDetails = function(data, studentDataCb) {
//   var student = server.models.Student;
//   var unregStudent = server.models.UnregisterCampusStudent;
//   if (data.registrationInd == 'y' || data.registrationInd == 'Y') {
//     dataToExport.push(data);
//   } else {
//     unregStudent.findById(data.studentId, function(err, unregStudentResponse) {
//       if (err) {
//         errorResponse('there was an error in findint he dtails', studentDataCb);
//       } else {
//         unregStudentData.push(unregStudentResponse);
//       }
//       finaldata = dataToExport.concat(unregStudentData);
//       console.log(finaldata);
//       studentDataCb(null, finaldata);
//     });
//   }
// };
//=====================================================
// var exportDetails = function(campusId, callBack) {
//   var eventList = server.models.EventStudentList;
//   var company = server.models.Company;
//   var employerEvent = server.models.EmployerEvent;
//   var student = server.models.Student;
//   var unregStudent = server.models.UnregisterCampusStudent;
//   company.findById(companyId, function(err, companyRes) {
//     if (err) {

//     } else {
//       companyName = companyRes.name;
//       console.log(companyRes.name);
//     }
//   });
//   employerEvent.findById(employerEventId, function(err, employerResponse) {
//     if (err) {

//     } else {
//       eventName = employerResponse.eventName;
//       console.log(employerResponse.eventName);
//     }
//   });
//   eventList.find({'where': {'companyId': companyId,
//                     'employerEventId': employerEventId}},
//   function(err, response) {
//     if (err) {
//       errorResponse('there was an error', callBack);
//     } else {
//       response.map(function(studentDetails) {
//         console.log(studentDetails.studentId);
//         student.findById(studentDetails.studentId, function(err, studentDetailsResponse) {
//           if (err) {
//             console.log('err');
//           } else if (studentDetailsResponse == null) {
//             unregStudent.findById(studentDetails.studentId, function(err, unregResponse) {
//               if (err) {
//                 console.log('err');
//               } else {
//                 var data = {};
//                 data.companyName = companyName;
//                 data.companyId = companyId;
//                 data.eventName = eventName;
//                 data.employerEventId = employerEventId;
//                 data.studentId = unregResponse.unregStudentId;
//                 data.firstName = unregResponse.firstName;
//                 data.middleName = unregResponse.middleName;
//                 data.lastName = unregResponse.lastName;
//                 data.emailId = unregResponse.emailId;
//                 unregStudentData.push(data);
//                 console.log(unregResponse);
//               }
//             });
//           } else {
//             var data = {};
//             data.companyName = companyName;
//             data.companyId = companyId;
//             data.eventName = eventName;
//             data.employerEventId = employerEventId;
//             data.studentId = studentDetailsResponse.unregStudentId;
//             data.firstName = studentDetailsResponse.firstName;
//             data.middleName = studentDetailsResponse.middleName;
//             data.lastName = studentDetailsResponse.lastName;
//             data.emailId = studentDetailsResponse.emailId;
//             dataToExport.push(data);
//             console.log(studentDetailsResponse);
//           }
//           finaldata = dataToExport.concat(unregStudentData);
//           convertToCsv(finaldata);
//         });
//       });
//     }
//   });
// };
