'use strict';
var server = require('../server/server');
var errorResponse = require('../ErrorHandling/customizeErrorTypes.js').errorFunction;
var getLookups = require('../commonCampusFiles/get-campus-dashboard').getLookups;
var inprocess = [];
var count = 0;
var jobsOffered = [];
var jobNameAndHiringDet = [];
function getProfile(companyDetails, cb) {
  getCompanySize(companyDetails.data[0], function(err, response) {
    if (err) {
      errorResponse('error occured', cb);
    } else {
      getCompanyContact(companyDetails.data[0].companyId, function(err10, res10) {
        if (err10) {
          errorResponse('err', cb);
        } else {
          getOrg(companyDetails.data[0].companyId, function(err11, res11) {
            if (err11) {
              errorResponse('err', cb);
            } else {
              // getJobDetails(companyDetails.data[0].companyId, function(err12, res12) {
              //   if (err12) {
              //     errorResponse('err', cb);
              //   } else {
              var final = createObject(companyDetails, response, res10, res11);
              cb(null, final);
              //   }
              // });
            }
          });
        }
      });
    }
  });
}
// var CampusDetails = [];
// var avgCompDetails = [];
// var CompGraph = [];
// var finalDataOfJob = [];
// function getJobDetails(companyId, compCb) {
//   jobsOffered = [];
//   var hiringAgg = server.models.CompanyHiringAggregates;
//   var async = require('async');
//   hiringAgg.find({'where': {'companyId': companyId}}, function(err13, res13) {
//     if (err13) {
//       errorResponse('err', compCb);
//     } else {
//       async.map(res13, getTotal, function(err20, res20) {
//         if (err20) {
//           errorResponse('err', compCb);
//         } else {
//           var jobRole = server.models.JobRole;
//           jobRole.find({'where': {'companyId': companyId}}, function(err14, res14) {
//             if (err14) {
//               errorResponse('an error', compCb);
//             } else {
//               async.map(res14, getJobRole, function(err15, res15) {
//                 if (err15) {
//                   errorResponse('err', compCb);
//                 } else {
//                   jobNameAndHiringDet = [];
//                   async.map(jobsOffered, getName, function(err18, res18) {
//                     if (err18) {
//                       errorResponse('an error', compCb);
//                     } else {
//                       GroupBy(companyId, function(err22, res22) {
//                         if (err22) {
//                           errorResponse('err', compCb);
//                         } else {
//                           getAvg(companyId, function(err24, res24) {
//                             if (err24) {
//                               errorResponse('err', compCb);
//                             } else {
//                               var async = require('async');
//                               async.map(CompGraph, findJobName, function(err25, res25) {
//                                 if (err25) {
//                                   errorResponse('err', compCb);
//                                 } else {
//                                   var final = {};
//                                   final.CampusDetails = CampusDetails;
//                                   final.JobDetails = finalDataOfJob;
//                                   compCb(null, final);
//                                 }
//                               });
//                             }
//                           });
//                         }
//                       });
//                     }
//                   });
//                 }
//               });
//             }
//           });
//         }
//       });
//     }
//   });
//   function findJobName(ob, cb6) {
//     var jobRole = server.models.JobRole;
//     jobRole.findOne({'where': {'jobRoleId': ob.jobRoleId}}, function(err26, res26) {
//       if (err26) {
//         errorResponse('err', cb6);
//       } else {
//         var data = {};
//         data.jobRoleName = res26.jobRoleName;
//         data.jobRoleId = ob.jobRoleId;
//         data.Year = ob.Year;
//         data.AvgCompPakage = ob.AvgCompPakage;
//         finalDataOfJob.push(data);
//         cb6(null, 'done');
//       }
//     });
//   }
//   function getTotal(ob, cb3) {
//     //console.log(ob);
//     cb3(null, ob);
//   }
//   function getName(ob, cb) {
//     var jobRole = server.models.JobRole;
//     jobRole.findOne({'where': {'jobRoleId': ob.jobRoleId}}, function(err19, res19) {
//       if (err19) {
//         errorResponse('err', cb);
//       } else {
//         var finalData = {};
//         finalData.JobName = res19.jobRoleName;
//         finalData.jobRoleId = ob.jobRoleId;
//         finalData.JobsOffered = ob.JobsOffered;
//         jobNameAndHiringDet.push(finalData);
//         cb(null, 'done');
//       }
//     });
//   }
//   function getJobRole(ob, cb) {
//     var hiringAgg = server.models.CompanyHiringAggregates;
//     hiringAgg.find({'where': {'jobRoleId': ob.jobRoleId, 'companyId': companyId}}, function(err15, res15) {
//       if (err15) {
//         errorResponse('err', cb);
//       } else {
//         var async = require('async');
//         async.map(res15, getJobsCount, function(err16, res16) {
//           if (err16) {
//             errorResponse('err', cb);
//           } else {
//             if (res15 != 0) {
//               var data = {};
//               data.jobRoleId = res15[0].jobRoleId;
//               data.JobsOffered = count;
//               count = 0;
//               jobsOffered.push(data);
//               cb(null, res16);
//             } else {
//               cb(null, null);
//             }
//           }
//         });
//       }
//     });
//   }
//   function getJobsCount(ob1, cb1) {
//     var date = new Date().getFullYear();
//     if (ob1.calendarYear <= date && ob1.calendarYear > date - 5) {
//       count = count + ob1.noOfOffers;
//       cb1(null, 'done');
//     } else {
//       cb1(null, 'done');
//     }
//   }
// }
function getOrg(companyId, orgCb) {
  console.log(companyId);
  var org = server.models.Organization;
  org.find({'where': {'companyId': companyId}}, function(err12, res12) {
    if (err12) {
      errorResponse('err', orgCb);
    } else if (res12 == 0) {
      orgCb(null, 'no org');
    } else {
      console.log()
      orgCb(null, res12);
    }
  });
}
function getCompanyContact(companyId, contactCb) {
  var companyContact = server.models.CompanyContact;
  companyContact.findOne({'where': {and: [
  {'companyId': companyId},
  {'primaryInd': 'Y'}],
},
}, function(err8, res8) {
    if (err8) {
      errorResponse('err', contactCb);
    } else if (res8 == null) {
      contactCb(null, null);
    } else {
      var contact = server.models.Contact;
      contact.findOne({'where': {'contactId': res8.contactId}}, function(err9, res9) {
        if (err9) {
          errorResponse('err', contactCb);
        } else {
          contactCb(null, res9);
        }
      });
    }
  });
}
function getDrivesInProcess(compDet, driveCb) {
  var empEvent = server.models.EmployerEvent;
  empEvent.find({'where': {'companyId': compDet.companyId}},
  function(err1, res1) {
    if (err1) {
      errorResponse('an error occured', driveCb);
    } else {
      var async = require('async');
      async.map(res1, getInprocessDrives, function(err2, res2) {
        if (err2) {
          errorResponse('error occured', driveCb);
        } else {
          driveCb(null, inprocess);
        }
      });
    }
  });
}
function getCompanySize(company, sizeCb) {
  lookup('COMPANY_SIZE_CODE', company.companySizeValueId, function(err4, res4) {
    if (err4) {
      errorResponse('there was an error', sizeCb);
    } else {
      getCompanyType(company, function(err5, res5) {
        if (err5) {
          errorResponse('err', sizeCb);
        } else {
          var compData = {};
          compData.companySize = res4.lookupValue;
          compData.compType = res5.type;
          compData.industryType = res5.inistuteType;
          sizeCb(null, compData);
        }
      });
    }
  });
}
function getCompanyType(company, typeCb) {
  lookup('COMPANY_TYPE_CODE', company.companyTypeValueId, function(err5, res5) {
    if (err5) {
      errorResponse('there was an error', typeCb);
    } else {
      getInistuteType(company, function(err6, res6) {
        if (err6) {
          errorResponse('an error', typeCb);
        } else {
          var comp = {};
          comp.type = res5.lookupValue;
          comp.inistuteType = res6.lookupValue;
          typeCb(null, comp);
        }
      });
    }
  });
}
function getInistuteType(company, inisCb) {
  lookup('INDUSTRY_TYPE_CODE', company.industryTypeValueId, function(err7, res7) {
    if (err7) {
      errorResponse('there was an error', inisCb);
    } else {
      inisCb(null, res7);
    }
  });
}
function getInprocessDrives(ob, cb) {
  inprocess = [];
  getLookups('EMPLOYER_EVENT_STATUS_CODE', 'In Progress', function(lookupRes) {
    if (ob.eventStatusValueId == lookupRes.lookupValueId) {
      inprocess.push(ob);
      cb(null, 'done');
    } else {
      cb(null, 'done');
    }
  });
}
function lookup(type, ob, lookupCallback) {
  var lookup = server.models.LookupType;
  var lookupvalue = server.models.LookupValue;
        //var type = 'SKILL_TYPE_CODE';
  lookup.find({
    'where': {
      lookupCode: type,
    },
  }, function(err, re) {
    if (err) {
      lookupCallback(err, null);
    } else {
      lookupvalue.findOne({
        'where': {
          lookupValueId: ob,
          lookupTypeId: re[0].lookupTypeId,
        },
      }, function(err, re1) {
        if (err) {
          lookupCallback('error in lookup', re1);
        } else {
          lookupCallback(null, re1);
        };
      });
    }
  });
}
function createObject(companyDetails, size, contact, orgDetails) {
  var object = {};
  object.Name = companyDetails.data[0].name;
  // object.Drives = inprocess;
  object.brandingImage = companyDetails.data[0].brandingImage;
  object.facebook = companyDetails.data[0].facebook;
  object.linkedin = companyDetails.data[0].linkedin;
  object.twitter = companyDetails.data[0].twitter;
  object.youtube = companyDetails.data[0].youtube;
  object.logo = companyDetails.data[0].logo;
  object.size = size.companySize;
  object.compType = size.compType;
  object.industryType = size.industryType;
  object.description = companyDetails.data[0].description;
  object.objective = companyDetails.data[0].objective;
  object.orgDetails = orgDetails;
  object.missionStatement = companyDetails.data[0].missionStatement;
  object.websiteAddress = companyDetails.data[0].websiteAddress;
  object.contact = contact;
  // object.HiringDetails = HiringDetails;
  // object.jobsOfPast5Years = jobsOffered;
  return object;
}
var ThisYear = [];
function GroupBy(company_Id, cb4) {
  var mysql = require('mysql');

  var datasource = require('../server/datasources.json');
  console.log(datasource.ScoraXChangeDB);
  var mysql = require('mysql');
  var con = mysql.createConnection({
    host: datasource.ScoraXChangeDB.host,
    user: datasource.ScoraXChangeDB.user,
    password: datasource.ScoraXChangeDB.password,
    database: datasource.ScoraXChangeDB.database,
  });

  con.connect(function(err) {
    if (err) throw err;
    var sum = 'SUM(no_of_offers)';
    con.query('SELECT campus_id,company_Id,COUNT(*),' + sum + ',calendar_year FROM COMPANY_HIRING_AGGREGATES WHERE company_Id = ' + company_Id + ' GROUP BY campus_id,company_id,calendar_year', function(err, result, fields) {
      if (err) throw err;
      var async = require('async');
      async.map(result, findYear1, function(err21, res21) {
        if (err21) {
          errorResponse('err', cb4);
        } else {
          async.map(ThisYear, getCampusName, function(err23, res23) {
            if (err23) {
              errorResponse('err', cb4);
            } else {
              cb4(null, CampusDetails);
            }
          });
        }
      });
    });
  });
}
function findYear1(ob, cb) {
  var date = new Date().getFullYear();
  if (ob.calendar_year < date && ob.calendar_year >= date - 4) {
    console.log('in');
    ThisYear.push(ob);
    cb(null, 'done');
  } else {
    console.log('out');
    cb(null, null);
  }
}
var total = [];
var thisYear = [];
function getCampusName(ob, cb5) {
  for (var key in ob) {
    if (key == 'SUM(no_of_offers)') {
      total.push(ob[key]);
      passAnObj();
    }
  }
  function passAnObj() {
    var campus = server.models.Campus;
    campus.findOne({'where': {'campusId': ob.campus_id}}, function(err, res) {
      if (err) {
        errorResponse('err', cb5);
      } else {
        var data = {};
        data.campusName = res.shortName;
        data.TotalJobsForThatCampus = total[0];
        CampusDetails.push(data);
        cb5(null, 'done');
      }
    });
  }
}
function getAvg(company_Id, cb4) {
  var mysql = require('mysql');
  con.connect(function(err) {
    if (err) throw err;
    var avg = 'AVG(sum_of_offers)';
    con.query('SELECT job_role_id,COUNT(*),' + avg + ',calendar_year FROM COMPANY_HIRING_AGGREGATES WHERE company_Id = ' + company_Id + ' GROUP BY job_role_id,calendar_year', function(err, result, fields) {
      console.log(err);
      if (err) throw err;
      var async = require('async');
      async.map(result, findYear, function(err21, res21) {
        if (err21) {
          errorResponse('err', cb4);
        } else {
          async.map(thisYear, getAvgData, function(err23, res23) {
            if (err23) {
              errorResponse('err', cb4);
            } else {
              cb4(null, CompGraph);
            }
          });
        }
      });
    });
  });
}
function findYear(ob, cb) {
  var date = new Date().getFullYear();
  if (ob.calendar_year < date && ob.calendar_year >= date - 4) {
    console.log('in');
    thisYear.push(ob);
    cb(null, 'done');
  } else {
    console.log('out');
    cb(null, null);
  }
}
var Avg = [];
function getAvgData(ob, cb5) {
  for (var key in ob) {
    if (key == 'AVG(sum_of_offers)') {
      Avg.push(ob[key]);
      passAnObj();
    }
  }
  function passAnObj() {
    var data = {};
    console.log('aaaaaaaaaaaaaaaa', ob);
    data.jobRoleId = ob.job_role_id;
    data.Year = ob.calendar_year;
    data.AvgCompPakage = Avg[0];
    Avg = [];
    CompGraph.push(data);
    cb5(null, 'done');
  }
}
var datasource = require('../server/datasources.json');
console.log(datasource.ScoraXChangeDB);
var mysql = require('mysql');
var con = mysql.createConnection({
  host: datasource.ScoraXChangeDB.host,
  user: datasource.ScoraXChangeDB.user,
  password: datasource.ScoraXChangeDB.password,
  database: datasource.ScoraXChangeDB.database,
});
var CampusDetails = [];
function getJobDetails(companyId, compCb) {
  var date = new Date().getFullYear();
  var salaryAndRecrutment = 'SELECT SUM(no_of_offers) lastYearToatalOffers, CAST(MAX(maximum_offer) as UNSIGNED) lastYearMaximumSalary from COMPANY_HIRING_AGGREGATES' +
  ' WHERE company_id=' + companyId + ' AND calendar_year=' + (date - 1) + '';
  MySql(salaryAndRecrutment, function(err, sqlRes) {
    if (err) {
      errorResponse('No data', compCb);
    } else {
      var campus = 'SELECT COUNT(DISTINCT(campus_id)) lastYearTotalCampusVisited from COMPANY_HIRING_AGGREGATES' +
                    ' WHERE company_id=' + companyId + ' AND calendar_year=' + (date - 1) + '';
      MySql(campus, function(err, sqlRes1) {
        if (err) {
          errorResponse('an error', compCb);
        } else {
          var offer = 'SELECT campus_id, SUM(no_of_offers) totalOffers from COMPANY_HIRING_AGGREGATES' +
          ' WHERE company_id=' + companyId + ' AND calendar_year BETWEEN ' +  (date - 5) + ' AND ' + (date - 1) + ' GROUP BY campus_id';
          MySql(offer, function(err, sqlRes2) {
            if (err) {
              errorResponse('an error', compCb);
            } else {
              var avgOffer = 'SELECT CAST(SUM(sum_of_offers)/SUM(no_of_offers) as UNSIGNED) averageOffer ,calendar_year from COMPANY_HIRING_AGGREGATES' +
              ' WHERE company_id=' + companyId + ' AND calendar_year BETWEEN ' +  (date - 5) + ' AND ' + (date - 1) + ' GROUP BY calendar_year';
              MySql(avgOffer, function(err, sqlRes3) {
                if (err) {
                  errorResponse('an err', compCb);
                } else {
                  var interns =  'SELECT SUM(no_of_interns) totalIntersLastYear from COMPANY_PROGRAM_AGGREGATES' +
                  ' WHERE company_id=' + companyId + ' AND calendar_year=' + (date - 1) + '';
                  MySql(interns, function(err, sqlRes4) {
                    if (err) {
                      errorResponse('err', compCb);
                    } else {
                      createJobsObject(sqlRes, sqlRes1, sqlRes2, sqlRes3, sqlRes4, function(err, finalRes) {
                        if (err) {
                          errorResponse('an error occured', compCb);
                        } else {
                          compCb(null, finalRes);
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}
function createJobsObject(salaryRecurtment, numberOfCampus, totalOffers, avgOffer, interns, createCb) {
  var async = require('async');
  CampusDetails = [];
  async.map(totalOffers, getCampus, function(err, res) {
    if (err) {
      errorResponse('err', createCb);
    } else {
      var final = {};
      final.recurtmentDeatails = salaryRecurtment;
      final.campusVisited = numberOfCampus;
      final.Interns = interns;
      final.CampusAndJobDetails = CampusDetails;
      final.avgOfferGraph = avgOffer;
      createCb(null, final);
    }
  });
}
function getCampus(ob, cb5) {
  var campus = server.models.Campus;
  campus.findOne({'where': {'campusId': ob.campus_id}}, function(err, res) {
    if (err) {
      errorResponse('err', cb5);
    } else {
      var data = {};
      data.campusName = res.name;
      data.TotalJobsForThatCampus = ob.totalOffers;
      CampusDetails.push(data);
      total = [];
      cb5(null, 'done');
    }
  });
}
function MySql(statment, sqlCb) {
  console.log(statment);
  con.query(statment, function(err, result, fields) {
    if (err) {
      errorResponse(err, sqlCb);
    } else {
      sqlCb(null, result);
    }
  });
}
exports.getProfile = getProfile;
exports.getDrivesInProcess = getDrivesInProcess;
exports.getJobDetails = getJobDetails;
