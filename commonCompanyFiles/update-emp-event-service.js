'use strict';
var validateModel = require('../ServicesImpl/CommonImpl/models-validation.js').inputValidation;
var eventJson = require('../common/models/EMPLOYER_EVENT.json').properties;
var lookupValidation = require('../commonValidation/lookupMethods.js').validateLookups;
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var server = require('../server/server');
var findEntity = require('../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
var employerEvent = server.models.EmployerEvent;
var updateEventPanel = require('./update-employer-event-panel-service').updatePanel;
var count = 0;
var eventArray = [];
var empEventArray = {};
  /**
   *updateEmpEventService-function deals with updating the employer event service
   *@constructor
   * @param {object} inputData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function updateEmpEventService(inputData, cb) {
  // console.log('dddddddddddddddddddddddddddddd');
  // validateModel(inputData.employerEvent, eventJson, function(err, resp) {
  //   if (resp.valid == false) {
  //     cb(resp.errors, resp);
  //   } else {
      inputData.employerEvent.map(function(eventObj) {
        if (eventObj.empEventId && eventObj.companyId && eventObj.empDriveId) {
          var compLookups = [{
            'lookupValueId': eventObj.eventTypeValueId,
            'lookupTypeId': 'EMPLOYER_EVENT_TYPE_CODE',
          },
            {
              'lookupValueId': eventObj.eventStatusValueId,
              'lookupTypeId': 'EMPLOYER_EVENT_STATUS_CODE',
            },
          ];
          lookupValidation(compLookups, function(lookupValue) {
            console.log();
            if (lookupValue == true) {
              var inputFilterObject = {};
              inputFilterObject['empEventId'] = eventObj.empEventId;
              inputFilterObject['companyId'] = eventObj.companyId;
              inputFilterObject['empDriveId'] = eventObj.empDriveId;
              findEntity(inputFilterObject, employerEvent, function(error, response) {
                if (error) {
                  throwError(error, cb);
                  logger.error('Error while fetching employer event record');
                } else if (response) {
                  console.log(response);
                  response.data[0].updateAttributes(eventObj, function(err, info) {
                    if (err) {
                      throwError(err, cb);
                    } else {
                      logger.info('employer event record Updated Successfully');
                      info['requestStatus'] = true;
                      info['updateDatetime'] = new Date();
                      count++;

                      eventArray.push(info);
                      // console.log('--------eventArray--as parent--------', info);
                      if (inputData.employerEvent.length == count) {
                        count = 0;
                        updateEventPanel(inputData.employerEventPanel, function(error, itemResponse) {
                          if (error) {
                            throwError(error, cb);
                            logger.error('Error while updating comp pkg item');
                          } else if (itemResponse) {
                            empEventArray['employerEvent'] = eventArray;

                            empEventArray['employerEventPanel'] = itemResponse;
                            // console.log('--------employerEventPanel---as child-------', empEventArray);
                            cb(null, empEventArray);
                            eventArray = [];
                          } else {
                            throwError('Invalid Comp Pkg Item Input', cb);
                            logger.error('Invalid Comp Pkg Item Input');
                          }
                        });
                      }
                    }
                  });
                } else {
                  throwError('Invalid empEventId , CompanyId, empDriveId ', cb);
                  logger.error('Invalid empEventId , CompanyId, empDriveId');
                }
              });
            } else {
              throwError('Invalid eventTypeValueId or eventStatusValueId', cb);
              logger.error('Invalid eventTypeValueId or eventStatusValueId');
            }
          });
        } else {
          throwError('empEventId, empDriveId and CompanyId are required', cb);
          logger.error('empEventId, empDriveId and CompanyId are required');
        }
      });
  //   }
  // });
}
exports.updateEmpEventService = updateEmpEventService;
