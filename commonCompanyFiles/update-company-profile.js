'use strict';
var companyValidation = require('../commonValidation/companyValidation');
var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var allmethods = require('../commonValidation/allmethods');
var Company = server.models.Company;
var lookupMethods = require('../commonValidation/lookupMethods');
  /**
   *updateCompanyProfile-function deals with updating the company profile
   *@constructor
   * @param {object} companyData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function updateCompanyProfile(companyData, cb) {
  if (companyData) {
    if (companyData.rating) {
      logger.error('You cannot perform update operation:');
      throwError('You cannot perform update operation', cb);
    } else {
      logger.info('validating Json ');
      //validatinf input Json
      companyValidation.validateCompanyJson(companyData, function(err, value) {
        if (err) {
          logger.error('Invalid Json');
          throwError('Invalid Json', cb);
        } else {
          var sizeVal, typeVal, industryVal;
          var sizeValObj = {};
          var typeValObj = {};
          var industryValObj = {};
          var sizeValCode = 'COMPANY_SIZE_CODE';
          var typeValCode = 'COMPANY_TYPE_CODE';
          var industryValCode = 'INDUSTRY_TYPE_CODE';

          sizeValObj['lookupValueId'] = companyData.companySizeValueId;
          typeValObj['lookupValueId'] = companyData.companyTypeValueId;
          industryValObj['lookupValueId'] = companyData.industryTypeValueId;
          lookupMethods.typeCodeFunction(sizeValObj, sizeValCode, function(sizeCheck) {
            sizeVal = sizeCheck;
            lookupMethods.typeCodeFunction(typeValObj, typeValCode, function(typeCheck) {
              typeVal = typeCheck;
              lookupMethods.typeCodeFunction(industryValObj, industryValCode, function(industryCheck) {
                industryVal = industryCheck;

                if (sizeVal == true && typeVal == true && industryVal == true) {
                  if (companyData.companyId != undefined && companyData.companyId != null && companyData.companyId != '') {
                    //find the record with companyId
                    Company.findOne({
                      'where': {
                        'companyId': companyData.companyId,
                      },
                    }, function(err, companyList) {
                      if (err) {
                        cb(err, companyData);
                      } else if (companyList) {
                        //update the attributes in the company table
                        companyData['searchName'] = (companyList.name) ? companyList.name.toUpperCase() : null;
                        companyData['searchShortName'] = (companyList.shortName) ? companyList.shortName.toUpperCase() : null;
                        companyList.updateAttributes(companyData, function(err, info) {
                          if (err) {
                            throwError('Invalid Input', cb);
                          } else {
                            //after successfully updated the record requestStatus will become true
                            logger.info('updated company profile successfully');
                            info['requestStatus'] = true;
                            info['updateDatetime'] = new Date();
                            cb(null, info);
                          }
                        });
                      } else {
                        logger.error('Invalid CompanyID');
                        throwError('Invalid CompanyID', cb);
                      }
                    });
                  } else {
                    logger.error('Need CompanyID for Updation');
                    throwError('Need CompanyID for Updation', cb);
                  }
                } else {
                  logger.error('Invalid companySizeValueId or companyTypeValueId or industryTypeValueId ');
                  throwError('Invalid companySizeValueId or companyTypeValueId or industryTypeValueId', cb);
                }
              });
            });
          });
        }
      });
    }
  } else {
    logger.error("Input can't be Null");
    throwError("Input can't be null", cb);
  }
}
exports.updateCompanyProfile = updateCompanyProfile;
