'use strict';
var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var validation = require('../commonValidation/addressValidation');
var updateAddress = require('../commonValidation/updateAddress');
var allmethods = require('../commonValidation/allmethods');
var updateAddService = require('../commonValidation/update-service-address');
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var EmployerPersonAddress = server.models.EmployerPersonAddress;
  /**
   *employeePersonAddressUpdate-for updating the employee person address update
   *@constructor
   * @param {object} empPersonData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function employeePersonAddressUpdate(empPersonData, cb) {
  if (empPersonData) {
    //  console.log("validating fields: ",empPersonData);
    if (empPersonData.companyId && empPersonData.employerPersonId && empPersonData.addressId) {
      EmployerPersonAddress.findOne({
        'where': {
          'and': [{
            'companyId': empPersonData.companyId,
          }, {
            'addressId': empPersonData.addressId,
          }, {
            'employerPersonId': empPersonData.employerPersonId,
          }],
        },
      }, function(err, OrgList) {
        if (err) {
          //throws error
          cb(err, empPersonData);
        } else if (OrgList) {
          //updating record in address table with addressId
          logger.info('record found for provided addressId , companyId and employeePersonId');
          updateAddService.updateAddresService(empPersonData, function(err, empPersonAddressList) {
            if (err) {
              logger.error('error while updating employee person address record');
              cb(err, empPersonData);
            } else {
              logger.info('successfully employee person address updated');
              cb(null, empPersonAddressList);
            }
          });
        } else {
          //throws error incase of invalid address_id or companyId
          throwError('Invalid address_id or companyId or employerPersonId', cb);
          logger.error('Invalid address_id or companyId or employerPersonId');
        }
      });
    } else {
      throwError('companyId and employerPersonId and addressId are required ', cb);
      logger.error('companyId and addressId and employerPersonId are required');
    }
  } else {
    logger.error("Input can't be Null");
    throwError('Input is Null: ', cb);
  }
}

exports.employeePersonAddressUpdate = employeePersonAddressUpdate;
