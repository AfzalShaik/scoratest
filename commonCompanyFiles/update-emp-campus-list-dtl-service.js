'use strict';
var validateModel = require('../ServicesImpl/CommonImpl/models-validation.js').inputValidation;
var employerCampusListDtlJson = require('../common/models/EMPLOYER_CAMPUS_LIST_DTL.json').properties;
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var server = require('../server/server');
var findEntity = require('../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
var employerCampusListDtl = server.models.EmployerCampusListDtl;
var count = 0;
var dtlArray = [];
  /**
   *updateDtl-function deals with updating the employer campus list details
   *@constructor
   * @param {object} dtlData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function updateDtl(dtlData, cb) {
  validateModel(dtlData, employerCampusListDtlJson, function(err, resp) {
    if (resp.valid == false) {
      cb(resp.errors, resp);
    } else {
      dtlData.map(function(dtlObj) {
        if (dtlObj.listCampusId && dtlObj.companyId && dtlObj.listId && dtlObj.campusId) {
          var inputFilterObject = {};
          inputFilterObject['listCampusId'] = dtlObj.listCampusId;
          inputFilterObject['companyId'] = dtlObj.companyId;
          inputFilterObject['listId'] = dtlObj.listId;
          inputFilterObject['campusId'] = dtlObj.campusId;
          findEntity(inputFilterObject, employerCampusListDtl, function(error, response) {
            if (error) {
              throwError(error, cb);
              logger.error('Error while fetching emp list dtl record');
            } else if (response) {
              response.data[0].updateAttributes(dtlObj, function(err, info) {
                if (err) {
                  throwError('error while updating emp list dtl', cb);
                } else {
                  logger.info('emp list dtl item record Updated Successfully');
                  info['requestStatus'] = true;
                  info['updateDatetime'] = new Date();
                  count++;
                  dtlArray.push(info);
                  dtlArray['empDtl'] = dtlArray;
                  if (dtlData.length == count) {
                    count = 0;
                    cb(null, dtlArray);
                  }
                }
              });
            } else {
              throwError('Invalid listId, campusId, listCampusId and CompanyId', cb);
              logger.error('Invalid listId, campusId,  listCampusId and CompanyId');
            }
          });
        } else {
          throwError('listCampusId,  listId, campusId and CompanyId are required', cb);
          logger.error('listCampusId,  listId, campusId and CompanyId are required');
        }
      });
    }
  });
}
exports.updateDtl = updateDtl;
