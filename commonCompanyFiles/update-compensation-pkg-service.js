'use strict';
var validateModel = require('../ServicesImpl/CommonImpl/models-validation.js').inputValidation;
var compensationJson = require('../common/models/COMPENSATION_PACKAGE.json').properties;
var lookupValidation = require('../commonValidation/lookupMethods.js').validateLookups;
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var server = require('../server/server');
var findEntity = require('../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
var compensationPackage = server.models.CompensationPackage;
var updateCompItem = require('./update-compensation-pkg-item-service').updateItem;
var count = 0;
var compArray = [];
var packageArray = {};
  /**
   *updatePackageServices-function deals with updating the pakage service details
   *@constructor
   * @param {object} inputData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function updatePackageService(inputData, cb) {
  packageArray = {};
  compArray = [];
  validateModel(inputData.compensationPkg, compensationJson, function(err, resp) {
    if (resp.valid == false) {
      cb(resp.errors, resp);
    } else {
      inputData.compensationPkg.map(function(compObj) {
        if (compObj.compPackageId && compObj.companyId) {
          var compLookups = [{
            'lookupValueId': compObj.compApprovalStatusValueId,
            'lookupTypeId': 'COMP_APPROVAL_STATUS_CODE',
          }];
          lookupValidation(compLookups, function(lookupValue) {
            if (lookupValue == true) {
              var inputFilterObject = {};
              inputFilterObject['compPackageId'] = compObj.compPackageId;
              inputFilterObject['companyId'] = compObj.companyId;
              findEntity(inputFilterObject, compensationPackage, function(error, response) {
                if (error) {
                  throwError(error, cb);
                  logger.error('Error while fetching compensation package record');
                } else if (response) {
                  // console.log(response);
                  
                  response.data[0].updateAttributes(compObj, function(err, info) {
                    if (err) {
                      throwError(err, cb);
                    } else {
                      logger.info('compensation package record Updated Successfully');
                      info['requestStatus'] = true;
                      info['updateDatetime'] = new Date();
                      count++;
                    
                      compArray.push(info);

                      if (inputData.compensationPkg.length == count) {
                        // console.log(count);
                        count = 0;
                        updateCompItem(inputData.compensationPkgItem, function(error, itemResponse) {
                          if (error) {
                            throwError(JSON.stringify(error), cb);
                            logger.error('Error while updating comp pkg item');
                          } else if (itemResponse) {
                            packageArray['compensationPkg'] = compArray;
                            packageArray['compensationPkgItem'] = itemResponse;
                            cb(null, packageArray);
                          } else {
                            throwError('Invalid Comp Pkg Item Input', cb);
                            logger.error('Invalid Comp Pkg Item Input');
                          }
                        });
                      }
                    }
                  });
                } else {
                  throwError('Invalid compPackageId and CompanyId', cb);
                  logger.error('Invalid compPackageId and CompanyId');
                }
              });
            } else {
              throwError('Invalid compApprovalStatusValueId', cb);
              logger.error('Invalid compApprovalStatusValueId');
            }
          });
        } else {
          throwError('compPackageId and CompanyId are required', cb);
          logger.error('compPackageId and CompanyId are required');
        }
      });
    }
  });
}
exports.updatePackageService = updatePackageService;
