'use strict';
var companyModelValidation = require('../commonValidation/all-models-validation');
var compensationValidation = require('../commonValidation/compensation-package-validation');
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var findEntity = require('../ServicesImpl/CommonImpl/getEntityDetails.js');
var server = require('../server/server');
var employerCampusListJson = require('../commonValidation/all-models-json').employerCampusListJson;
var employerCampusLists = server.models.EmployerCampusListHdr;
  /**
   *updateEmployerCampusListService-function deals with updating the employer campus list service
   *@constructor
   * @param {object} empCampusData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function updateEmployerCampusListService(empCampusData, cb) {
  if (empCampusData) {
    if (empCampusData.listId && empCampusData.companyId && empCampusData.jobRoleId) {
      var validValue;
      companyModelValidation.validateModelsJson(empCampusData, employerCampusListJson, function(err, validValue) {
        validValue = validValue;
        if (validValue == false) {
          throwError(err, cb);
          logger.error('validation error');
        } else {
          var inputFilterObject = {};
          inputFilterObject['listId'] = empCampusData.listId;
          inputFilterObject['companyId'] = empCampusData.companyId;
          inputFilterObject['jobRoleId'] = empCampusData.jobRoleId;
          findEntity.entityDetailsById(inputFilterObject, employerCampusLists, function(error, response) {
            if (error) {
              throwError(error, cb);
              logger.error('Error while fetching employer package list record');
            } else if (response) {
              compensationValidation.validateCompensationPackageLookup(empCampusData, function(lookupErr, lookupValue) {
                if (lookupErr) {
                  throwError(lookupErr, cb);
                  logger.error('error while validating package lookups');
                } else if (lookupValue == true) {
                  response.data.updateAttributes(empCampusData, function(err, info) {
                    if (err) {
                      throwError('error while updating employer campus list', cb);
                    } else {
                      logger.info('employer campus list record Updated Successfully');
                      info['requestStatus'] = true;
                      info['updateDatetime'] = new Date();
                      cb(null, info);
                    }
                  });
                } else {
                  throwError('invalid  approvalStatusValueId', cb);
                }
              });
            } else {
              throwError('Invalid companyId or listId or jobRoleId', cb);
              logger.error('Invalid companyId or listId or jobRoleId');
            }
          });
        }
      });
    } else {
      throwError('listId, jobRoleId and companyId are required', cb);
      logger.error('listId, jobRoleId and companyId are required');
    }
  } else {
    throwError('Input cannot be blank', cb);
    logger.error('Input cannot be blank');
  }
}
exports.updateEmployerCampusListService = updateEmployerCampusListService;
