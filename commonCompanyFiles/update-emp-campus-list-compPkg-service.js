'use strict';
var validateModel = require('../ServicesImpl/CommonImpl/models-validation.js').inputValidation;
var employerCampusListCompPkgJson = require('../common/models/EMPLOYER_CAMPUS_LIST_COMP_PKG.json').properties;
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var server = require('../server/server');
var findEntity = require('../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
var employerCampusListCompPkg = server.models.EmployerCampusListCompPkg;
var count = 0;
var listArray = [];
  /**
   *updateList-function deals with updating list of employerxampus list
   *@constructor
   * @param {object} listData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function updateList(listData, cb) {
  validateModel(listData, employerCampusListCompPkgJson, function(err, resp) {
    if (resp.valid == false) {
      cb(resp.errors, resp);
    } else {
      listData.map(function(listObj) {
        if (listObj.listCompPkgId && listObj.companyId && listObj.listId && listObj.compPackageId) {
          var inputFilterObject = {};
          inputFilterObject['listCompPkgId'] = listObj.listCompPkgId;
          inputFilterObject['companyId'] = listObj.companyId;
          inputFilterObject['listId'] = listObj.listId;
          inputFilterObject['compPackageId'] = listObj.compPackageId;
          findEntity(inputFilterObject, employerCampusListCompPkg, function(error, response) {
            if (error) {
              throwError(error, cb);
              logger.error('Error while fetching emp list pkg record');
            } else if (response) {
              response.data[0].updateAttributes(listObj, function(err, info) {
                if (err) {
                  throwError('error while updating emp list pkg', cb);
                } else {
                  logger.info('emp list pkg item record Updated Successfully');
                  info['requestStatus'] = true;
                  info['updateDatetime'] = new Date();
                  count++;
                  listArray.push(info);
                  listArray['empListPkg'] = listArray;
                  if (listData.length == count) {
                    count = 0;
                    cb(null, listArray);
                  }
                }
              });
            } else {
              throwError('Invalid listId, listCompPkgId, listCompPkgId and CompanyId', cb);
              logger.error('Invalid listId, listCompPkgId,  listCompPkgId and CompanyId');
            }
          });
        } else {
          throwError('listCompPkgId, listId, compPackageId and CompanyId are required', cb);
          logger.error('listCompPkgId, listId, compPackageId and CompanyId are required');
        }
      });
    }
  });
}
exports.updateList = updateList;
