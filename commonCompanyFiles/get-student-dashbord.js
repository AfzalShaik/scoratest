'use strict';
var server = require('../server/server');
var campusEvenetStudentSearch = server.models.CampusEventStudentSearchVw;
var skills = [];
var lookup = require('../commonValidation/lookupMethods').lookupMethod;
var errorResponse = require('../ErrorHandling/customizeErrorTypes.js').errorFunction;
var cleanArray = require('../commonValidation/common-mass-upload').cleanArray;
var eventStudent = server.models.EventStudentList;
var lookup = require('../commonValidation/lookupMethods').lookupMethod;
function events(studentId, cb) {
  eventStudent.find({'where': {'studentId': studentId},
'include': ['eventStudentListIbfk3rel', 'eventStudentListIbfk5rel', 'eventStudentListIbfk8rel']}, function(err, res) {
    if (err) {
      errorResponse('err', cb);
    } else {
      cb(null, res);
    }
  });
}
var thisAcademicYearEvents = [];
function visitedDetails(studentId, cb) {
  var endDate = new Date();
  var startDate = new Date();
  var currentYear = new Date().getFullYear();
  var month = new Date().getMonth();
  if (month > 6) {
    startDate.setFullYear((currentYear), 5, 1);
    endDate.setFullYear((currentYear + 1), 4, 31);
  } else if (month <= 6) {
    startDate.setFullYear((currentYear - 1), 5, 1);
    endDate.setFullYear((currentYear), 4, 31);
  }
  var enroll = server.models.Enrollment;
  enroll.findOne({'where': {'studentId': studentId}},
  function(err, res) {
    if (err) {
      errorResponse(err, cb);
    } else {
          var statment = 
          'select *  from CAMPUS_EVENT where  campus_drive_id in (select  campus_drive_id ' +
          ' from CAMPUS_DRIVE where department_id in(select department_id from PROGRAM where ' +
          'program_id=' + res.programId + ')' +
          ' and update_datetime between ' + JSON.stringify(startDate) + ' and ' + JSON.stringify(endDate) + ')' +
          ' and update_datetime between ' + JSON.stringify(startDate) + ' and ' + JSON.stringify(endDate) + '';
           MySql(statment, function(err, eventsResponse){
            if(err) {

            } else {
              console.log(statment);
              var async = require('async');
              async.map(eventsResponse, getCompanyNameAndDetails, function(er, eventDetails){
                if(err) {

                } else {
                  cb(null, eventDetails);
                }
              });
            }
          });
        }
      });
}
function getCompanyNameAndDetails(obj, cb) {
  var company = server.models.Company;
  company.findOne({'where':{'companyId': obj['company_id']}},
  function(err, responseOfCompany){
    if(err) {

    } else {
      var data = {};
      data.eventName = obj['event_name'];
      data.eventId = obj['campus_event_id'];
      data.scheduledDate = obj['scheduled_date'];
      data.companyId = obj['company_id'];
      data.companyName = responseOfCompany.name;
      data.CompanyShortName = responseOfCompany.shortName;
      cb(null, data);
    }
  });
}
function getEvents(depData, cb1) {
  var campusDrive = server.models.CampusDrive;
  campusDrive.find({'where': {'departmentId': depData.departmentId}},
  function(err, res) {
    if (err) {
      errorResponse(err, cb1);
    } else {
      var async = require('async');
      async.map(res, getEventsBasedOnDriveId, function(err1, res1) {
        if (err1) {
          errorResponse(err1, cb1);
        } else {
          cb1(null, res1);
        }
      });
    }
  });
}
function getEventsBasedOnDriveId(obj, cb2) {
  var campusEvent = server.models.CampusEvent;
  campusEvent.find({'where': {'campusDriveId': obj.campusDriveId},
'include': ['campusEventIbfk4rel']},
function(err, res) {
  if (err) {
    errorResponse(err, cb2);
  } else {
    cb2(null, res);
  }
});
}
function getThisYearEvents(object, cb3) {
  if (object != 0) {
    var date = new Date().getFullYear();
    var month = new Date().getMonth();
    var pastDate = new Date(object.createDatetime).getFullYear();
    var pastMonth = new Date(object.createDatetime).getMonth();
    if (pastMonth < 6 && pastDate == date) {
      thisAcademicYearEvents.push(object);
      cb3(null, thisAcademicYearEvents);
    }
    if (pastMonth > 6 && pastDate == date - 1) {
      thisAcademicYearEvents.push(object);
      cb3(null, thisAcademicYearEvents);
    }
    // thisAcademicYearEvents.push(object);
    // cb3(null, thisAcademicYearEvents);
  }
}
var program = {};
var MyClass = [];
var selected = [];
var comp = [];
var max = 0;
var avg = 0;
var goingToVisit = 0;
var visited = 0;
function numberData(obj, cb) {
  program.programId = obj.enrollmentIbfk2rel.programId;
  visitedDetails(obj.studentId, function(err, res) {
    if (err) {
      errorResponse(err, cb);
    } else {
      console.log('resssssssssssss', res);
      var async = require('async');
      async.map(res, getStudents, function(err1, res1) {
        if (err1) {
          errorResponse(err1, cb);
        } else {
          async.map(MyClass, getOffered,
                function(err2, res2) {
                  if (err2) {
                    errorResponse(err2, cb);
                  } else {
                    async.map(selected, getCompDetails,
                        function(err3, res3) {
                          if (err3) {
                            errorResponse(err3, cb);
                          } else {
                            comp.push(200);
                            async.map(comp, getMax, function(err4, res4) {
                              if (err) {
                                errorResponse(err4, cb);
                              } else {
                                getEventsBasedOnDate(res, function(err5, res5) {
                                 // console.log(res5);
                                  async.map(res5, getGoingToVistAndVisited, function(err6, res6){
                                    if(err6) {
                                      errorResponse(err6, cb);
                                    } else {
                                      getFilterdStudents(selected, function(err7, res7){
                                        var details = {};
                                   //     console.log(res7);
                                        details.selectedStudentsInMyclass = res7.length;
                                        details.highestComp = max;
                                        details.avgComp = avg / comp.length;
                                        details.totalCompaniesVisited = visited;
                                        details.goingToVisit = goingToVisit;
                                        comp = [];
                                        visited = 0;
                                        goingToVisit = 0;
                                        avg = 0;
                                        max = 0;
                                        cb(null, details);
                                      })
                                    }
                                  })
                                })
                              }
                            });
                          }
                        });
                  }
                });
        }
      });
    }
  });
}
function getStudents(obj, cb1) {
  eventStudent.find({'where': {'campusEventId': obj.campusEventId}},
function(err, res) {
  if (err) {
    errorResponse(err, cb1);
  } else {
    var async = require('async');
    async.map(res, getMyClassStudentsFromEvent,
        function(err2, res2) {
          if (err2) {
            errorResponse(err2, cb1);
          } else {
            cb1(null, res2);
          }
        });
  }
});
}
function getMyClassStudentsFromEvent(obj, cb2) {
  var enroll = server.models.Enrollment;
  enroll.findOne({'where': {'studentId': obj.studentId,
  'programId': program.programId}},
    function(err4, res4) {
      if (err4) {
        errorResponse(err4, cb2);
      } else {
        if (res4 != null) {
          MyClass.push(obj);
          cb2(null, 'done');
        } else {
          cb2(null, 'done');
        }
      }
    });
}
function getOffered(object, cb) {
  lookup('CANDIDATE_STATUS_TYPE', function(err, res) {
    var offered = res.find(getNumber);
    if (object.candidateStatusValueId == offered.lookupValueId) {
      selected.push(object);
      cb(null, 'done');
    } else {
      cb(null, 'done');
    }
  });
}
function getCompDetails(obj, cb4) {
  var compPakage = server.models.CompensationPackageItem;
  compPakage.findOne({'where': {'compPackageId': obj.compPackageId}}, function(err, res) {
    if (err) {
      errorResponse(err, cb4);
    } else if (res == null) {
      cb4(null, 'done');
    } else {
      var num = parseFloat(res.amount);
      comp.push(num);
      cb4(null, 'done');
    }
  });
}
function getMax(obj, cb) {
  max = Math.max(parseFloat(obj), max);
  avg = avg + parseFloat(obj);
  cb(null, 'done');
}
function getNumber(obj) {
  return obj.lookupValue === 'Offered';
}
function getEventsBasedOnDate(events, cb) {
  var count = 0;
  var resArr = [];
  events.filter(function(item){
    count++;
    var i = resArr.findIndex(x => x.companyId == item.companyId);
    if(i <= -1){
      resArr.push(item);
    } if(count == events.length) { cb(null, resArr); }
  });
}
function getGoingToVistAndVisited(obj, cb){
  var date = new Date().getFullYear();
  var month = new Date().getMonth();
  var past = new Date(obj.scheduledDate).getFullYear();
  var pastMonths = new Date(obj.scheduledDate).getMonth();
  if(obj.scheduledDate == null) {
    goingToVisit++;
    cb(null, 'done');
  } else if (month> pastMonths) {
    visited++;
    cb(null, 'done');
  } else {
    cb(null, 'done');
  }
}
function getFilterdStudents(student, cb) {
  var count = 0;
  var students = [];
  selected.filter(function(item){
    count++;
    var i = students.findIndex(x => x.studentId == item.studentId);
    // console.log(item.studentId);
    if(i <= -1){
      students.push(item);
    } if(count == selected.length) { cb(null, students); }
  });
}
var mysql = require('mysql');
var datasource = require('../server/datasources.json');
// console.log(datasource.ScoraXChangeDB);
var mysql = require('mysql');
var con = mysql.createConnection({
  host: datasource.ScoraXChangeDB.host,
  user: datasource.ScoraXChangeDB.user,
  password: datasource.ScoraXChangeDB.password,
  database: datasource.ScoraXChangeDB.database,
});
var companyDetails = [];
function graphData (obj, cb) {
  var date = new Date().getFullYear();
  var past = date - 5;
  // console.log(obj);
  var program = obj.programId;
  var noOfOffersAndGraph = 'select sum(no_of_offers) totalOffers , sum(total_offers)/sum(no_of_offers) averageOffer ,max(maximum_offer) maximumOffer , academic_year FROM CAMPUS_PLACEMENT_AGGREGATES' +
  ' where academic_year between '+ past + ' and '+ date + ' and program_id=' + program + ' group by academic_year';
  MySql(noOfOffersAndGraph, function(err, sqlRes) {
    if (err) {
      errorResponse(err, cb);
    } else {
      var company = 'select company_id, max(maximum_offer) maximumOffer, sum(no_of_offers) totalOffers FROM CAMPUS_PLACEMENT_AGGREGATES where academic_year between '+past+' and '+date+' and program_id='+program+' group by company_id';
      MySql(company, function(err1, sqlRes1){
        if(err1){
          errorResponse(err1, cb);
        } else {
          var compinesVisted = 'select count(distinct(company_id)) totalCompinesVisited , academic_year FROM CAMPUS_PLACEMENT_AGGREGATES where academic_year between '+past+' and '+date+' and program_id='+program+' group by academic_year';
          MySql(compinesVisted, function(err2, sqlRes2){
            if(err2){
              errorResponse(err2, cb);
            } else {
              var details = {};
              var async = require('async');
              async.map(sqlRes1, getCompanyName, function(err11, res11){
                var details = {};
                details.avgStudentsMaxSaleryAvgSalery = sqlRes;
                details.companyWithOffers = companyDetails;
                details.totalCompinesVisted = sqlRes2;
                cb(null, details);
                companyDetails = [];
              })
            }
          })
        }
      })
    }
  });
}
function getCompanyName(obj, cb){
  var company = server.models.Company;
  company.findOne({'where':{'companyId': obj.company_id},
'fields':{'shortName': true}}, function(err11, res11){
    var data  ={};
    data.CompanyName = res11.shortName;
    data.companyId = obj.company_id;
    data.maximumOffer = obj.maximumOffer;
    data.totalOffers = obj.totalOffers;
    companyDetails.push(data);
    console.log('ooooooooooooooooooooooooooooooooo', companyDetails);
    cb(null, 'done');
  });
}
var mysql = require('mysql');
var con = mysql.createConnection({
  host: datasource.ScoraXChangeDB.host,
  user: datasource.ScoraXChangeDB.user,
  password: datasource.ScoraXChangeDB.password,
  database: datasource.ScoraXChangeDB.database,
});
var selectedPeople = [];
var highestComp = 0;
var totalComp = 0;
var uniqueCompaniesGoingToVisit = [];
var uniqueCompaniesVisited = [];
function getSummaryOfStudentDashbord(studentId, callBack) {
  // var enrollment = server.models.Enrollment;
  var endDate = new Date();
  var startDate = new Date();
  var currentYear = new Date().getFullYear();
  var month = new Date().getMonth();
  if (month > 6) {
    startDate.setFullYear((currentYear), 5, 1);
    endDate.setFullYear((currentYear + 1), 4, 31);
  } else if (month <= 6) {
    startDate.setFullYear((currentYear - 1), 5, 1);
    endDate.setFullYear((currentYear), 4, 31);
  }
  var statment = 'select student_id studentId, program_id programId from ENROLLMENT a where program_id in (select program_id from ENROLLMENT where student_id='+studentId+')'+
  'and planed_completion_date between '+ JSON.stringify(startDate) +' and '+ JSON.stringify(endDate) + 
  'and exists (select 1 from EVENT_STUDENT_LIST where student_id = a.student_id and candidate_status_value_id=381)';
  MySql(statment, function(err, response){
    if(err) {
      callBack(err, null);
    } else {
      var async = require('async');
      async.map(response, getSelectedStudents, function(err, selectedReponse){
        if(err) {
          callBack(err, null);
        } else {
          async.map(selectedPeople, getMaxAndHighestComp, function(err, compDetails){
            if(err) {
              callBack(err, null);
            } else if (response == 0) {
              callBack(null, {});
            } else {
                  var uniqueCompaniesVisited = 'select distinct company_id  from CAMPUS_EVENT where campus_drive_id in ( ' +
                    ' select campus_drive_id from CAMPUS_DRIVE where department_id in ( ' +
                    ' select department_id from PROGRAM where program_id = ' + response[0].programId + ') and scheduled_date ' +
                    ' between ' + JSON.stringify(startDate)+ ' and '+ JSON.stringify(endDate) + ')';
                   MySql(uniqueCompaniesVisited, function(err, visitedResponse){
                    if(err) {
                      callBack(err, null);
                    } else {
                      var goingToVisitCompines = ' select distinct company_id  from CAMPUS_EVENT where campus_drive_id in ( ' +
                       ' select campus_drive_id from CAMPUS_DRIVE where department_id in ( ' +
                      ' select department_id from PROGRAM where program_id = ' + response[0].programId + ') and scheduled_date > ' + JSON.stringify(new Date()) + ');';
                      // var async = require('async');
                      // async.map(driveResponse, getEventsOfthatDrive, function(err, driveResponse){
                      //   if(err) {
                      //     callBack(err, null);
                      //   } else {
                        MySql(goingToVisitCompines, function(err, distinctCompinesGoingToVisit){
                          if(err) {
                            callBack(err, null);
                          } else {
                            var data = {};
                            data.totalPeopleSelectedInClass = selectedPeople.length;
                            data.highestComp = highestComp;
                            data.averageComp = totalComp/(selectedPeople.length);
                            data.companiesVisited = visitedResponse.length;
                            data.companiesGoingToVisit = distinctCompinesGoingToVisit.length;
                            callBack(null, data);
                            selectedPeople = [];
                            highestComp = 0;
                            totalComp = 0;
                            uniqueCompaniesVisited = [];
                            uniqueCompaniesGoingToVisit = [];
                          }
                        })
                          // con.end();
                      //   }
                      // });
                    }
                   });
              //   }
              // });
            }
          });
        }
      });
    }
  });
  function getEventsOfthatDrive(object, cb) {
    var events = 'select * from CAMPUS_EVENT where ' +
    'campus_drive_id = '+ object.campusDriveId +'';
    MySql(events, function(err, eventResponse){
      if(err) {
        callBack(err, null);
      } else {
        var async = require('async');
        async.map(eventResponse, getGoingToVistAndVisitedCompanies,
          function(err, responseOfCompanyCount){
            if(err) {
              cb(err, null);
            } else {
              cb(null, null);
            }
        });
      }
    });
  }
  function getGoingToVistAndVisitedCompanies(eventObject, callBac) {
    var endDate = new Date();
    var startDate = new Date();
    var currentYear = new Date().getFullYear();
    var month = new Date().getMonth();
    if (month > 6) {
      startDate.setFullYear((currentYear), 5, 1);
      endDate.setFullYear((currentYear + 1), 4, 31);
    } else if (month <= 6) {
      startDate.setFullYear((currentYear - 1), 5, 1);
      endDate.setFullYear((currentYear), 4, 31);
    }
    if(eventObject['scheduled_date'] > new Date()) {
      if(!uniqueCompaniesGoingToVisit.includes(parseInt(eventObject['company_id']))) {
        uniqueCompaniesGoingToVisit.push(eventObject['company_id']);
        callBac(null, 'done');
      } else {
        callBac(null, 'done');
      }
    } else if (eventObject['scheduled_date'] < new Date() && eventObject['scheduled_date'] < endDate) {
      if(!uniqueCompaniesVisited.includes(parseInt(eventObject['company_id']))) {
        uniqueCompaniesVisited.push(eventObject['company_id']);
        callBac(null, 'done');
      } else {
        callBac(null, 'done');
      }
    } else {
      callBac(null, 'done');
    }
  }
  function getSelectedStudents(obj, cb) {
    if(obj.studentId != studentId) {
      var selected = 'select DISTINCT student_id studentId, comp_package_id compPackageId  from EVENT_STUDENT_LIST where candidate_status_value_id=381 and student_id='+ obj.studentId +'';
      // var eventStudent = server.models.EventStudentList;
      MySql(selected, function(err, selectedResponse){
        // console.log('selected res', selectedResponse);
        if(err) {
          cb(err, null);
        } else if (selectedResponse == 0) {
          cb(null, 'done');
        } else {
          var data = {};
          data.studentId = selectedResponse[0].studentId;
          data.compPackageId = selectedResponse[0].compPackageId;
          selectedPeople.push(data);
          cb(null, selectedResponse);
        }
      });
    } else {
      cb(null, 'done');
    }
  }
  function getMaxAndHighestComp(obj , cb) {
    console.log(obj);
    var compData = server.models.CompensationPackageItem;
    if(obj.studentId && obj.compPackageId) {
      // console.log(obj.compPackageId);
      var comp = 'select total_comp_pkg_value amount from COMPENSATION_PACKAGE where comp_package_id = '+ obj.compPackageId +'';
      MySql(comp, function(err, compResponse){
        console.log('comp res', compResponse);
        if(err) {
          cb(err, null);
        } else if (compResponse == 0) {
          cb(null, null);
        } else {
          // console.log('comppppppppppppp', compResponse);
          var compNumber = parseInt(compResponse[0].amount);
          getMaxAnAvg(compNumber, function(err, response){
            if(err) {
              cb(err, null);
            } else {
              cb(null, 'done');
            }
          });
        }
      });
    } else {
      cb(null, 'done'); 
    }
  }
  function getMaxAnAvg(compDetailsData, callBc) {
    // console.log('dettttttttttttttttttttttttttttt', companyDetails);
    totalComp = compDetailsData + totalComp;
    highestComp = Math.max(compDetailsData, highestComp);
        // console.log('dettttttttttttttttttttttttttttt', totalComp, highestComp);
    callBc(null, 'done');
  }
}
function MySql(statment, sqlCb) {
  console.log(statment);
   con.query(statment, function(err, result, fields) {
     if (err) {
       errorResponse(err, sqlCb);
     } else {
       sqlCb(null, result);
     }
   });
 }
exports.events = events;
exports.getSummaryOfStudentDashbord = getSummaryOfStudentDashbord;
exports.visitedDetails = visitedDetails;
exports.numberData = numberData;
exports.graphData = graphData;
