'use strict';
var empEventPanelValidation = require('../commonValidation/all-models-validation');
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var findEntity = require('../ServicesImpl/CommonImpl/getEntityDetails.js');
var server = require('../server/server');
var employerEventPanelJson = require('../commonValidation/all-models-json').employerEventPanel;
var employerEventPanel = server.models.EmployerEventPanel;
  /**
   *updateEmpPanelService-function deals with updating the employee panel data
   *@constructor
   * @param {object} empData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function updateEmpPanelService(empData, cb) {
  if (empData) {
    if (empData.empDriveId && empData.companyId && empData.empEventId) {
      empEventPanelValidation.validateModelsJson(empData, employerEventPanelJson, function(err, validValue) {
        validValue = validValue;
        if (validValue == false) {
          throwError(err, cb);
          logger.error('validation error');
        } else {
          var inputFilterObject = {};
          inputFilterObject['empEventId'] = empData.empEventId;
          inputFilterObject['companyId'] = empData.companyId;
          inputFilterObject['empEventPanelId'] = empData.empEventPanelId;
          findEntity.entityDetailsById(inputFilterObject, employerEventPanel, function(error, response) {
            if (error) {
              throwError(error, cb);
              logger.error('Error while fetching employer campus list Program record');
            } else if (response) {
              response.data.updateAttributes(empData, function(err, info) {
                if (err) {
                  throwError('error while updating emp campus Program', cb);
                } else {
                  logger.info('emp campus Program record Updated Successfully');
                  info['requestStatus'] = true;
                  info['updateDatetime'] = new Date();
                  cb(null, info);
                }
              });
            } else {
              throwError('Invalid companyId  or empEventId or empEventPanelId', cb);
              logger.error('Invalid companyId or empEventId or empEventPanelId');
            }
          });
        }
      });
    } else {
      throwError('empEventPanelId, empEventId and companyId are required', cb);
      logger.error('empEventPanelId, empEventId and companyId are required');
    }
  } else {
    throwError('Input cannot be blank', cb);
    logger.error('Input cannot be blank');
  }
}
exports.updateEmpPanelService = updateEmpPanelService;
