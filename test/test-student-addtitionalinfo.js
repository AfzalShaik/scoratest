'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect    = require('chai').expect;
var request = require('request');
chai.use(chaiHttp);
//Program
describe(' Additional Info ;', function() {
//program post method with valid data
  it('Additional Info POST returns 200', function(done) {
    var input = {

      'studentId': 3,
      'instituteName': 'jntu',
      'programName': 'Project',
      'scoreGrade': 90,
      'url': 'youtube',
      'Duration': 2,
      'DurationtypeValueId': 1,
      'comment': 's',
      'createDatetime': '2017-09-04T05:04:11.138Z',
      'updateDatetime': '2017-09-04T05:04:11.138Z',
      'createUserId': 11,
      'updateUserId': 11,
    };

    var url = 'http://localhost:3000/api/StudentAdditionalInfos';
    var request = require('request');

    request({
      url: url,
      method: 'POST',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) { return err; }

      expect(response.statusCode).to.equal(200);

      done();
    });
  });
// trying to upbate by providing academicsId which was auto generated
  it('Additional Info POST returns 200', function(done) {
    var input = {
      'academicsId': 980,
      'studentId': 3,
      'instituteName': 'jntu',
      'programName': 'Project',
      'scoreGrade': 90,
      'url': 'youtube',
      'Duration': 2,
      'DurationtypeValueId': 1,
      'comment': 's',
      'createDatetime': '2017-09-04T05:04:11.138Z',
      'updateDatetime': '2017-09-04T05:04:11.138Z',
      'createUserId': 11,
      'updateUserId': 11,
    };

    var url = 'http://localhost:3000/api/StudentAdditionalInfos';
    var request = require('request');

    request({
      url: url,
      method: 'POST',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) { return err; }

      expect(response.statusCode).to.equal(500);

      done();
    });
  });
// get method
  it('Assessment  GET returns 200', function(done) {
    var input =
      {

        'academicsId': 9,
      };

    var url = 'http://localhost:3000/api/StudentAdditionalInfos';
    var request = require('request');

    request({
      url: url,
      method: 'GET',
      json: true,

    }, function(err, response, body) {
      if (err) { return err; }
      expect(response.statusCode).to.equal(200);
      done();
    });
  });
  //update method with valid data
  it('Assessment  PUT returns 200', function(done) {
    var input =
      {
        'academicsId': 87,
        'instituteName': 'string',
        'programName': 'string',
        'scoreGrade': 50,
        'url': 'string',
        'Duration': 2,
      };

    var url = 'http://localhost:3000/api/StudentAdditionalInfos/updateStudentInfo';
    var request = require('request');

    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) { return err; }
      expect(response.statusCode).to.equal(200);
      done();
    });
  });
  // update method test with in valid academics id
  it('Assessment  PUT returns 200', function(done) {
    var input =
      {
        'academicsId': 980,
        'instituteName': 'string',
        'programName': 'string',
        'scoreGrade': 50,
        'url': 'string',
        'Duration': 2,
      };

    var url = 'http://localhost:3000/api/StudentAdditionalInfos/updateStudentInfo';
    var request = require('request');

    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) { return err; }
      expect(response.statusCode).to.equal(200);
      done();
    });
  });
}
);

