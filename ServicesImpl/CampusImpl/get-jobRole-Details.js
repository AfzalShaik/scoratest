'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
var request = require('request');
var getDetailsById = require('../CommonImpl/getEntityDetails.js');
var throwError = require('../../ErrorHandling/customizeErrorTypes').errorFunction;
var cleanArray = require('../../commonValidation/common-mass-upload.js').cleanArray;
var forFind = [];
var jobName = [];
var shortlisted = 0;
var remaning = 0;
var totalShortlisted = 0;
var total = [];
var shortRemaning = {};
var selected = 0;
var tt = [];
var offerShortlist = [];
var totalDetails = [];
var details = [];
var selectedStudents = [];
var response = [];
var compAvg = 0;
var totalCompanyCount = 0;
var totalCompanySelectedCount = 0;
var sheduledData;
function getAllJobRoleDetials(companyId, cb) {
  var jobRole = server.models.JobRole;
  jobRole.find({'where': {'companyId': companyId}},
  function(err, res) {
    if (err) {
      cb(err, null);
    } else {
      var async = require('async');
      async.map(res, getJobsCountAndSelectedDetails,
        function(err, response) {
          if (err) {
            cb(err, null);
          } else {
            // console.log(response);
            var data = {};
            data.JobRoleSummary = response;
            data.totalCmpanySelected = totalCompanySelectedCount;
            data.totalCompanyCount = totalCompanyCount;
            cb(null, data);
            totalCompanySelectedCount = 0;
            totalCompanyCount = 0;
          }
        });
    }
  });
}
function getJobsCountAndSelectedDetails(obj, callBack) {
  var date = new Date();
  var lastYearDate = new Date();
  var presentYear = new Date().getFullYear();
  var month = new Date().getMonth();
  if (month > 6) {
    date.setDate((presentYear + 1), 5, 1);
    lastYearDate.setFullYear((presentYear), 5, 1);
  } else if (month <= 6) {
    date.setDate((presentYear), 5, 1);
    lastYearDate.setFullYear((presentYear - 1), 5, 1);
  }
  var jobsCount = 'select sum(no_of_positions) totalPostions' +
   ' from EMPLOYER_DRIVE where job_role_id=' +
  obj.jobRoleId + ' and create_datetime  between '  + JSON.stringify(lastYearDate) +
  ' and '  + JSON.stringify(date) + ';';
  MySql(jobsCount, function(err, totalJobPostions) {
    if (err) {
      callBack(err, null);
    } else if (totalJobPostions[0].totalPostions == null) {
      console.log('--');
      var data = {};
      data.jobRoleName = obj.jobRoleName;
      data.totalJobs = 0;
      data.openPostions = 0;
      data.shortlisted = 0;
      data.totalStudentsInvolved = 0;
      data.averageComp = 0;
      data.postionsFilled = 0;
      callBack(null, data);
    } else {
      var shortlistedCount = 'select count(*) shortlisted from EVENT_STUDENT_LIST eventStudent where employer_event_id in (' +
        ' select emp_event_id from EMPLOYER_EVENT emp where emp_drive_id in (' +
        ' select emp_drive_id from EMPLOYER_DRIVE where job_role_id = ' + obj.jobRoleId + ' and ' +
        ' create_datetime between ' + JSON.stringify(lastYearDate) + ' and ' + JSON.stringify(date) + ') ' +
        ' and eventStudent.update_datetime > emp.scheduled_date and student_subscribe_ind = "Y" and ' +
        ' create_datetime between ' + JSON.stringify(lastYearDate) + ' and ' + JSON.stringify(date) + ')';
      MySql(shortlistedCount, function(err, shortlistedPeople) {
        if (err) {
          callBack(err, null);
        } else {
          var selected = 'select * from EVENT_STUDENT_LIST' +
          ' where employer_event_id in (' +
          ' select emp_event_id from EMPLOYER_EVENT where ' +
          ' emp_drive_id in (' +
          ' select emp_drive_id from EMPLOYER_DRIVE where job_role_id = ' + obj.jobRoleId +
          ' and create_datetime between ' + JSON.stringify(lastYearDate) + ' and ' + JSON.stringify(date) + ')' +
          ' and create_datetime between ' + JSON.stringify(lastYearDate) + ' and ' + JSON.stringify(date) + ') and ' +
           'student_subscribe_ind = "Y" and candidate_status_value_id = 381' +
           ' and create_datetime between ' + JSON.stringify(lastYearDate) + ' and ' + JSON.stringify(date) + '';
          MySql(selected, function(err, selectedPeople) {
            // console.log(selected);
            if (err) {
              callBack(err, null);
            } else {
              // var totalAmount = 'select sum(total_comp_pkg_value) totalCompValue from COMPENSATION_PACKAGE where comp_package_id in(' +
              //   ' select comp_package_id from EVENT_STUDENT_LIST where employer_event_id in ( ' +
              //   ' select employer_event_id from EMPLOYER_EVENT where emp_drive_id in ( ' +
              //   ' select emp_drive_id from EMPLOYER_DRIVE where job_role_id = ' + obj.jobRoleId + ' and ' +
              //   ' create_datetime between ' + JSON.stringify(lastYearDate) + ' and ' + JSON.stringify(date) + ') and create_datetime between ' + JSON.stringify(lastYearDate) + ' ' +
              //   ' and ' + JSON.stringify(date) + ')and update_datetime between ' + JSON.stringify(lastYearDate) + ' and ' + JSON.stringify(date) +
              //   ' and student_subscribe_ind = "Y" and candidate_status_value_id = 381)';
              var totalAmount = 'select ' +
              ' cast(sum(total_comp_pkg_value) as unsigned) totalCompValue ' +
              ' from  JOB_ROLE jr, ' +
              '    EMPLOYER_DRIVE ed, ' +
              '    EMPLOYER_EVENT ee, ' +
              '    EVENT_STUDENT_LIST esl, ' +
              '    COMPENSATION_PACKAGE cp ' +
              ' where cp.comp_package_id = esl.comp_package_id ' +
              ' and   esl.candidate_status_value_id = 381 ' +
              ' and   esl.employer_event_id = ee.emp_event_id ' +
              ' and   ee.emp_drive_id = ed.emp_drive_id ' +
              ' and   jr.job_role_id = ed.job_role_id ' +
              ' and   ed.job_role_id = ' + obj.jobRoleId +
              ' and    esl.create_datetime between ' + JSON.stringify(lastYearDate) + ' and ' + JSON.stringify(date) + ' ' +
              ' and 	ed.create_datetime between ' + JSON.stringify(lastYearDate) + ' and ' + JSON.stringify(date) + ' ' +
              ' and ee.create_datetime between ' + JSON.stringify(lastYearDate) + ' and ' + JSON.stringify(date) + '';
              MySql(totalAmount, function(err, totalCompAmount) {
                if (err) {
                  callBack(err, null);
                } else {
                  var unResgTotalAmount = 'select ' +
                  ' cast(sum(total_comp_value) as unsigned) totalCompValueForUnReg ' +
                  ' from  JOB_ROLE jr, ' +
                  '    EMPLOYER_DRIVE ed, ' +
                  '    EMPLOYER_EVENT ee, ' +
                  '    EVENT_STUDENT_LIST esl, ' +
                  '    COMPENSATION_PACKAGE cp ' +
                  ' where cp.comp_package_id = esl.comp_package_id ' +
                  ' and   esl.candidate_status_value_id = 381 ' +
                  ' and   esl.employer_event_id = ee.emp_event_id ' +
                  ' and   ee.emp_drive_id = ed.emp_drive_id ' +
                  ' and   jr.job_role_id = ed.job_role_id ' +
                  ' and   ed.job_role_id = ' + obj.jobRoleId +
                  ' and    esl.create_datetime between ' + JSON.stringify(lastYearDate) + ' and ' + JSON.stringify(date) + ' ' +
                  ' and 	ed.create_datetime between ' + JSON.stringify(lastYearDate) + ' and ' + JSON.stringify(date) + ' ' +
                  ' and ee.create_datetime between ' + JSON.stringify(lastYearDate) + ' and ' + JSON.stringify(date) + '';
                  MySql(unResgTotalAmount, function(err, unregResponse) {
                    if (err) {
                      callBack(err, null);
                    } else {
                      var totalStudentsInvolved = 'select count(*) totalInvolved from EVENT_STUDENT_LIST where employer_event_id in (' +
                      'select emp_event_id from EMPLOYER_EVENT emp where emp_drive_id in (' +
                      'select emp_drive_id from EMPLOYER_DRIVE where job_role_id = ' + obj.jobRoleId + ' and ' +
                      'create_datetime between ' + JSON.stringify(lastYearDate) + ' and ' + JSON.stringify(date) + ')' +
                      'and create_datetime between ' + JSON.stringify(lastYearDate) + ' and ' + JSON.stringify(date) + ')' +
                      'and create_datetime between ' + JSON.stringify(lastYearDate) + ' and ' + JSON.stringify(date) + '';
                      var data = {};
                      MySql(totalStudentsInvolved, function(err, involvedStudents) {
                        if (err) {
                          callBack(err, null);
                        } else {
                        // console.log(totalAmount);
                          var totalCompByAddingBoth = totalCompAmount[0].totalCompValue + unregResponse[0].totalCompValueForUnReg;
                          data.jobRoleName = obj.jobRoleName;
                          data.totalJobs = totalJobPostions[0].totalPostions;
                          data.openPostions = totalJobPostions[0].totalPostions - selectedPeople.length;
                          data.shortlisted = shortlistedPeople[0].shortlisted;
                          data.totalStudentsInvolved = involvedStudents[0].totalInvolved;
                          data.averageComp =
                         (parseInt(totalCompByAddingBoth) / selectedPeople.length) ? parseInt(totalCompByAddingBoth / selectedPeople.length) : 0;
                          data.postionsFilled = selectedPeople.length;
                          totalCompanySelectedCount = parseInt(selectedPeople.length) + totalCompanySelectedCount;
                          totalCompanyCount = parseInt(totalJobPostions[0].totalPostions) + totalCompanyCount;
                          callBack(null, data);
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}
function MySql(statment, sqlCb) {
  var datasource = require('../../server/datasources.json');
  var mysql = require('mysql');
  // console.log(statment);
  var con = mysql.createConnection({
    host: datasource.ScoraXChangeDB.host,
    user: datasource.ScoraXChangeDB.user,
    password: datasource.ScoraXChangeDB.password,
    database: datasource.ScoraXChangeDB.database,
  });
  con.query(statment, function(err, result, fields) {
    if (err) {
      sqlCb(err, null);
    } else {
      sqlCb(null, result);
      con.end();
    }
  });
}
function getDeailsOfJob(obj, jobRoleCallback) {
  forFind = [];
  jobName = [];
  shortlisted = 0;
  remaning = 0;
  total = [];
  shortRemaning = {};
  selected = 0;
  tt = [];
  offerShortlist = [];
  totalDetails = [];
  details = [];
  selectedStudents = [];
  response = [];
  var employerDrive = server.models.EmployerDrive;
  var date = new Date();
  var lastYearDate = new Date();
  var presentYear = new Date().getFullYear();
  var month = new Date().getMonth();
  if (month > 6) {
    date.setDate((presentYear + 1), 5, 1);
    lastYearDate.setFullYear((presentYear), 5, 1);
  } else if (month <= 6) {
    date.setDate((presentYear), 5, 1);
    lastYearDate.setFullYear((presentYear - 1), 5, 1);
  }
  employerDrive.find({'where': {'jobRoleId': obj.jobRoleId,
    'createDatetime': {
      between: [lastYearDate, date],
    }}}, function(err, jobResponse) {
    if (err) {
      throwError('there was an error', jobRoleCallback);
    } else if (jobResponse == 0) {
      // var data = {};
      var data = {
        'jobRoleName': obj.jobRoleName,
        'totalJobs': 0,
        'openPostions': 0,
        'postionsFilled': 0,
        'shortlisted': 0,
        'remaning': 0,
        'totalStudentsInvolved': 0,
        'averageComp': 0,
      };
      // data[] = sample;
      jobRoleCallback(null, data);
    } else {
      var async = require('async');
      forFind.push(jobResponse.length);
      async.map(jobResponse, getTheCount, function(err, responseObject) {
        if (err) {
          throwError('error', jobRoleCallback);
        } else {
          var final = {};
          console.log(response);
          final.totalJobs = response[0];
          tt.push(response[0].totalJobs);
          async.map(responseObject, getDetails, function(err, jobResponse) {
            if (err) {
              throwError('there was an error', jobRoleCallback);
            } else {
              async.map(jobResponse, getFinal, function(err, res) {
                console.log('resssssssssssssssssssssssssssssss', res);
                if (err) {
                  jobRoleCallback(err, null);
                } else if (res == 0) {
                  var data = {};
                  var sample = {
                    'totalJobs': 0,
                    'openPostions': 0,
                    'postionsFilled': 0,
                    'shortlisted': 0,
                    'remaning': 0,
                    'totalStudentsInvolved': 0,
                    'averageComp': 0,
                  };
                  data[obj.jobRoleName] = sample;
                  jobRoleCallback(null, data);
                } else {
                  var data = {};
                  tt = final[final.length - 1];
                  jobName[0] = obj.jobRoleId;
                  jobName[1] = obj.jobRoleName;
                  details = total[1];
                  details.jobRoleName = obj.jobRoleName;
                  data = details;
                  jobRoleCallback(null, data);
                  response = [];
                  remaning = 0;
                }
              });
            }
          });
          function getFinal(array, cb2) {
            async.map(array, getStudentDetails, function(err, detailsOfStudent) {
              if (err) {
                throwError('thereeeee', jobRoleCallback);
              } else {
                console.log('came to final place');
                cb2(null, detailsOfStudent);
              }
            });
          }
        }
      });
    }
  });
// }
  function typeCodeFunction(value, type, lookupCallback) {
    var lookup = server.models.LookupType;
    var lookupvalue = server.models.LookupValue;
    lookup.find({
      'where': {
        lookupCode: type,
      },
    }, function(err, re) {
      if (err) {
        lookupCallback(err, null);
      } else {
        lookupvalue.find({
          'where': {
            lookupValue: value,
            lookupTypeId: re[0].lookupTypeId,
          },
        }, function(err, re1) {
          if (err) {
            lookupCallback('error', re1);
          } else {
            lookupCallback(null, re1);
          };
        });
      }
    });
  }
  var count = 0;
  var find = 0;
  function getTheCount(obj, callBack) {
    console.log('in count');
    count = obj.noOfPositions + count;
    totalCompanyCount = obj.noOfPositions + totalCompanyCount;
    find++;
    // response.push(obj);
    if (forFind[0] == find) { response.push(count); }
    callBack(null, obj);
  }
  function getDetails(object, cb) {
    console.log('in details');
    var empEvent = server.models.EmployerEvent;
    empEvent.find({'where': {'empDriveId': object.empDriveId,
    'scheduledDate': {
      lt: new Date(),
    }}}, function(err, res) {
      if (err) {
        throwError('there was errrrr', cb);
      } else {
        details.push(res);
        cb(null, res);
      }
    });
  }
  function getStudentDetails(studentObject, studentCallBack) {
    sheduledData = studentObject.scheduledDate;
    var eventStudentList = server.models.EventStudentList;
    console.log('studennnnnnnnnnnnnnt', studentObject.empEventId);
    eventStudentList.find({'where': {'employerEventId': studentObject.empEventId}},
  function(err, totalStudents) {
    if (err) {

    } else {
      eventStudentList.find({'where': {'employerEventId': studentObject.empEventId,
    'updateDatetime': {
      gte: studentObject.scheduledDate,
    }}, or: [
      {'studentSubscribeInd': 'Y'},
      {'studentSubscribeInd': 'y'},
    ]},
      function(err, studentRes) {
        if (err) {
          throwError('erro in student details fetching', studentCallBack);
        } else {
          shortlisted = studentRes.length + shortlisted;
          console.log('sss', studentRes);
          var async = require('async');
            // console.log('student res', studentRes);
          async.map(totalStudents, getDetailsAndAll, function(err2, res2) {
            if (err2) {

            } else {
              shortRemaning.totalJobs = response[0];
              shortRemaning.openPostions = response[0] - selected;
              shortRemaning.postionsFilled = selected;
              shortRemaning.shortlisted = shortlisted;
              // shortRemaning.remaning = remaning;
              shortRemaning.totalStudentsInvolved = selected + remaning;
              shortRemaning.averageComp = parseFloat(compAvg) / selected;
              total[1] = shortRemaning;
              totalDetails.push(shortRemaning);
              studentCallBack(null, shortRemaning);
              compAvg = 0;
              // shortlisted = 0;
            }
          });
        }
      });
    }
  });
    function getDetailsAndAll(obj, cb1) {
      if (obj.candidateStatusValueId == 381 && obj.updateDatetime >= studentObject.scheduledDate) {
        var data = {};
        data.studentId = obj.studentId;
        data.campusId = obj.campusId;
        data.updateDatetime = obj.updateDatetime;
        data.compPakage = obj.compPackageId;
        getCompDetails(data.compPackageId, function(err, compDetails) {
          if (err) {
            cb1(err, null);
          } else {
            selected++;
            totalCompanySelectedCount++;
            cb1(null, 'done');
          }
        });
      } else {
        remaning++;
        cb1(null, 'done');
      }
    }
  }
}
function getCompDetails(compId, cb) {
  var comp = server.models.CompensationPackageItem;
  comp.findOne({'where': {'compPackageId': compId},
    'fields': {
      'amount': true,
    }},
  function(err, resp) {
    if (err) {
      cb(err, null);
    } else {
      countComp(resp.amount, function(er, countRes) {
        if (err) {
          cb(err, null);
        } else {
          cb(null, 'done');
        }
      });
    }
  });
}
function countComp(amount, countCb) {
  compAvg = amount + compAvg;
  countCb(null, compAvg);
}
exports.getDeailsOfJob = getDeailsOfJob;
exports.getAllJobRoleDetials = getAllJobRoleDetials;
//   console.log(forFind[0], find);
//   if (forFind[0] == find) {
    // console.log(forFind[0], find);
//   }
// var lookUp = require('../../ServicesImpl/CampusImpl/campusEventManagment.js').typeCodeFunction;
// var look = [];
  // var l = getLookUps();
  // look[0] = l[0];
  // look[1] = l[1];
  // var finalObject = [];
          // console.log('...........................', countResponse);
         // console.log('response', responseObject);
               // console.log(jobResponse);
                        // console.log(responseObject[responseObject.length - 1]);
        //  console.log(responseObject);
                         // var l = getLookUps();
                  // var unique = detailsOfStudent[0].filter(function(elem, index, self) {
                  //   return index == self.indexOf(elem.studentId);
                  //   console.log('this is the element', elem);
                  // });
                 // console.log('============',unique);
                 // console.log(detailsOfStudent);
                                   // console.log(detailsOfStudent);
                  // finalObject[0] = final[final.length - 1];
                  // finalObject[1] = detailsOfStudent[0];
                  // finalObject[2] = detailsOfStudent[1];
                  // finalObject[3] = detailsOfStudent[2];
                  // console.log(detailsOfStudent);
                               // console.log('*********************', jobRoleResponse);
                                                 /// console.log(details.openPostions);
                                                                 //  console.log(l);
                 // console.log(detailsOfStudent);
                         //  jobRoleCallback(null, final);
                           // console.log(forFind, count);
                             // console.log('these are objects', object);
                               // empEvent.find({'where': {'empDriveId': object.empDriveId}}, function(err, response) {
  //   if(err) {
  //     throwError('there was an error', cb);
  //   } else {
  //     console.log(response);
  //     details.push(response);
  //   }
  // });
  // cb(null, details);
  // function getLookUps() {
//   var getLookUpsDetails = [];
//   var getshortListValue = [];
//   lookUp('Offered', 'CANDIDATE_STATUS_TYPE', function(err, lookUpRes) {
//     getLookUpsDetails.push(lookUpRes);
//    // console.log(lookUpRes[0].lookupValueId);
//    // console.log(lookUpRes);
//   });
//   return getLookUpsDetails;
// }
// lookUp('Shortlisted', 'CANDIDATE_STATUS_TYPE', function(err, lookUpRes) {
//   getshortListValue.push(lookUpRes[0].lookupValueId);
//   //console.log(lookUpRes);
// });
// getLookUpsDetails = getLookUpsDetails.concat(getshortListValue);
// function getShortlist() {
//   var getshortListValue = [];
//   lookUp('Shortlisted', 'CANDIDATE_STATUS_TYPE', function(err, lookUpRes) {
//     getshortListValue.push(lookUpRes);
//    // console.log(lookUpRes[0].lookupValueId);
//    // console.log(lookUpRes);
//   });
//   return getshortListValue;
// }
         // console.log(shortlistedRes, offerRes);
           // console.log('????????????????????????????', studentObject);
    // console.log('listttttttttttttttttttttt ', studentRes.length);
              // else if (studentRes == 0) {
      // console.log('done');}
      // console.log('listttttttttttttttttttttt ', studentRes.length);
      // var i = getLookUps();
      // var j = getShortlist();
                    // for (var i = 0; i < studentRes.length; i++) {
              //  // console.log(studentRes[i].candidateStatusValueId);
              //   if (studentRes[i].candidateStatusValueId == offerRes[0].lookupValueId) {
              //     var data = {};
              //     // var student = server.models.Student;
              //     data.studentId = studentRes[i].studentId;
              //     data.campusId = studentRes[i].campusId;
              //     data.updateDatetime = studentRes[i].updateDatetime;
              //     data.compPakage = studentRes[i].compPackageId;
              //     selectedStudents.push(data);
              //     selected++;
              //   } else if (studentRes[i].candidateStatusValueId == shortlistedRes[0].lookupValueId) {
              //     shortlisted++;
              //   } else {
              //     remaning++;
              //   }
              // }
                          // shortRemaning.totalJobs = tt[0];
            // shortRemaning.openPostions = tt[0] - selected;
            // shortRemaning.postionsFilled = selected;
            // shortRemaning.shortlisted = shortlisted;
            // shortRemaning.remaning = remaning;
            // shortRemaning.totalStudentsInvolved = selected + shortlisted + remaning;
            // shortRemaning.selectedStudents = selectedStudents;
            // total[1] = shortRemaning;
            // studentCallBack(null, shortRemaning);
            // function getLookups(code, value, callBc) {
//   var lookup = require('../../commonValidation/lookupMethods').lookupMethod;
//   lookup(code, function(err, lookUpResponse) {
//     var lookupValueId = lookUpResponse.find(findStatus);

//     function findStatus(statusVal) {
//       return statusVal.lookupValue === value;
//     }
//     callBc(lookupValueId);
//   });
// }
     // }
         // var student = server.models.Student;
             // for (var i = 0; i < studentRes.length; i++) {
      // console.log(studentRes[i].candidateStatusValueId);
