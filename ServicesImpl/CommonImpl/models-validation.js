'use strict';
var revalidator = require('revalidator');
var async = require('async');
var validation;
/**
   *inputValidation-function deals with validating
   *@constructor
   * @param {object} comparedData - contains the data need to get validated
   * @param {object} jsonData - contains the json data
   * @param {function} cb - deals with the response
   */
function inputValidation(comparedData, jsonData, cb) {
  comparedData.map(function(obj) {
    validation = revalidator.validate(obj, {
      properties: jsonData,
    });
  });
  cb(null, validation);
}
exports.inputValidation = inputValidation;

function validateInput(comparedData, jsonData, cb) {
  async.map(comparedData, getInfo, function(e, r) {
    if (e) {
      cb(e.errors, null);
    } else {
      cb(null, r);
    }
  });

  function getInfo(obj, callback) {
    validation = revalidator.validate(obj, {
      properties: jsonData,
    });
    console.log('..................', validation);
    if (validation.valid == true) {
      callback(null, obj);
    } else {
      callback(validation, null);
    }
  }
}
exports.validateInput = validateInput;

// testing
var Joi = require('joi');
var validVal;
var allModels = require('../../commonValidation/all-models-validation').validateModelsJson;

var data = {
  firstName: Joi.string().max(50).required(),
  lastName: Joi.string().max(50).required(),
  email: Joi.string().max(254).required(),
  admissionNo: Joi.string().max(50).required(),
  startDate: Joi.date().iso(),
  plannedCompletionDate: Joi.date().iso(),
  error: Joi.string().max(100),
  skipUserInd: Joi.string().max(50).required(),
};
function validateJsonInput(comparedData, jsonData, cb) {
  async.map(comparedData, getInfo, function(e, r) {
    if (e) {
      cb(e, null);
    } else {
      cb(null, r);
    }
  });
  function getInfo(obj, callback) {
   //validating input json fields
    var schema = Joi.object().keys(data);
    Joi.validate(obj, schema, function(err, value) {
      if (err) {
        var error = new Error('Validation Error');
        obj.error = 'true';
        obj.message = 'Invalid data type in rowNumber: ' + obj.rowNumber;
        callback(null, obj);
      } else {
        validVal = true;
        obj.error = 'false';
        obj.message = null;
        callback(null, obj);
      }
    });
  }
}
exports.validateJsonInput = validateJsonInput;
