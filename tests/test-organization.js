var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect    = require("chai").expect;
var request = require("request");
chai.use(chaiHttp);

//Organization
describe(' Organization Cases ;', function () {

  it('OrganizationPOST returns 200', function (done) {

      var input =
      {

  "companyId": 1007,
  "shortName": "yits",
  "name": "yitsol",
  "description": "yits",
  "createDatetime": "2017-08-02T12:13:42.043Z",
  "updateDatetime": "2017-08-02T12:13:42.043Z",
  "createUserId": 1,
  "updateUserId": 1,
  "facebook": "fb.com",
  "linkedin": "linkdin.com",
  "twitter": "twitter.com",
  "youtube": "youtube.com",
  "brandingImage": "branding image",
  "logo": "logo"
}

    var url = "http://localhost:3000/api/Organizations";
    var request = require('request');

    request({
      url: url,
      method: "POST",
      json: true,
      body: input

    }, function(err, response, body) {
      if(err) { return err; }

        expect(response.statusCode).to.equal(200);

      done();
    });
  });
//Organization Update method
  it('Organization PUT returns 200', function (done) {

      var input =

        {"organizationId":56767,
        "companyId":1007,
        "name":"YITSOL MADEENAGUDA"}



    var url = "http://localhost:3000/api/Organizations/UpdateOrganization";
    var request = require('request');

    request({
      url: url,
      method: "PUT",
      json: true,
      body: input

    }, function(err, response, body) {
      if(err) { return err; }

        expect(response.statusCode).to.equal(200);

      done();
    });
  });

  //Organization Get method
    it('Organization GET returns 200', function (done) {

        var input =

            {
            "organizationId": 56767,
            "companyId": 1007
            }

      var url = "http://localhost:3000/api/Organizations/getOrganizationDet?organizationId=56767&companyId=1007";
      var request = require('request');

      request({
        url: url,
        method: "GET",
        json: true

      }, function(err, response, body) {
        if(err) { return err; }

          expect(response.statusCode).to.equal(200);

        done();
      });
    });

});
