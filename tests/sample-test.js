'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect = require('chai').expect;
var request = require('request');
var Joi = require('joi');
chai.use(chaiHttp);
describe('supports decimal numbers', function() {
  it('supports decimal numbers', function(done) {
    var schema = Joi.number().less(999999999999.999999);
    var input = 489898.433;

    schema.validate(input, function(err, value) {
      console.log('valueeeeeeeeeeeeeeeee', value);
      console.log('errrrrrrrrrrrrrrrrrrr', err);
      expect(err).to.not.exist();
      expect(value).to.equal(input);
      done();
    });
  });
});
