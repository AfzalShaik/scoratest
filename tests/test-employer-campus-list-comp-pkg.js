'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect = require('chai').expect;
var request = require('request');
chai.use(chaiHttp);

//Employer campus list comp pkg
describe(' Employer Campus List comp pkg Cases ;', function() {
  it('Employer Campus List comp pkg POST returns 200', function(done) {
    var input = [{

      'listId': 4444,
      'companyId': 1026,
      'compPackageId': 2222,
      'startDate': '2016-08-11T00:00:00.000Z',
      'endDate': '2017-08-11T00:00:00.000Z',
      'endReason': 'endReason of compkg',
      'createDatetime': '2017-08-11T11:42:10.000Z',
      'updateDatetime': '2017-08-11T11:42:10.000Z',
      'createUserId': 1,
      'updateUserId': 1,
    }];
    var url = 'http://localhost:3000/api/EmployerCampusListCompPkgs';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'POST',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) {
        return err;
      }
      console.log('response.statusCode', response.statusCode);
      expect(response.statusCode).to.equal(200);

      done();
    });
  });
  //Campus List comp pkg item put
  it('Employer Campus List comp pkg PUT returns 200', function(done) {
    var input = {
      'listCompPkgId': 6666,
      'listId': 4444,
      'companyId': 1026,
      'compPackageId': 2222,
      'startDate': '2016-08-11T00:00:00.000Z',
      'endDate': '2017-08-11T00:00:00.000Z',
      'endReason': 'endReason of compkg',
      'createDatetime': '2017-08-11T11:42:10.000Z',
      'updateDatetime': '2017-08-18T12:09:36.003Z',
      'createUserId': 1,
      'updateUserId': 1,

    };

    var url = 'http://localhost:3000/api/EmployerCampusListCompPkgs/updateEmployerCampusListCompPkg';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) {
        return err;
      }
      console.log('response.statusCode', response.statusCode);
      expect(response.statusCode).to.equal(200);

      done();
    });
  });

  //Campus List comp pkg get
  it('Employer Campus List comp pkg GET returns 200', function(done) {
    var input = {
      'compPackageItemId': 7767,
      'compPackageId': 2222,
      'companyId': 1026,
    };

    var url = 'http://localhost:3000/api/EmployerCampusListCompPkgs/getEmployerCampusListCompPkg?listCompPkgId=6666';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'GET',
      json: true,
    }, function(err, response, body) {
      if (err) {
        return err;
      }
      console.log('body', response.statusCode);
      expect(response.statusCode).to.equal(200);

      done();
    });
  });
});
