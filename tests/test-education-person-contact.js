var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect    = require("chai").expect;
var request = require("request");
chai.use(chaiHttp);

//education person contact
describe(' education person contact Cases ;', function () {
//education person contact POST service
  it('education person contact POST returns 200', function (done) {

      var input =

{
  "educationPersonId":556,
  "contactTypeValueId": 15,
  "contactInfo": "contact info",
  "primaryInd": "N",
  "createDatetime": "2017-08-08T11:31:26.153Z",
  "updateDatetime": "2017-08-08T11:31:26.153Z",
  "createUserId": 1,
  "updateUserId": 1,
  "campusId": 388589390
}

    var url = "http://localhost:3000/api/EducationPersonContacts/createEducationContact";
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: "POST",
      json: true,
      body: input

    }, function(err, response, body) {
      if(err) { return err; }

        expect(response.statusCode).to.equal(200);

      done();
    });
  });
//education person contact PUT service
  it('education person contact PUT returns 200', function (done) {

      var input =

      {
    "campusId":388589390,
    "contactId":589,
    "educationPersonId":555,
    "contactInfo":"contact tested"
    }





    var url = "http://localhost:3000/api/EducationPersonContacts/updateEducationPersonContact";
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: "PUT",
      json: true,
      body: input

    }, function(err, response, body) {
      if(err) { return err; }

        expect(response.statusCode).to.equal(200);

      done();
    });
  });

  //education person contact GET service
    it('education person contact GET returns 200', function (done) {

        var input =
        {
          "educationPersonId": 556,
      "contactId": 656,
      "campusId": 388589390
    }

      var url = "http://localhost:3000/api/EducationPersonContacts/getEducationPersonContact?educationPersonId=556&campusId=388589390&contactId=656";
      var request = require('request');

      // console.log('Request:');
      // console.log(input);
      request({
        url: url,
        method: "GET",
        json: true

      }, function(err, response, body) {
        if(err) { return err; }

          expect(response.statusCode).to.equal(200);

        done();
      });
    });
});
