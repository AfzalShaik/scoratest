'use strict';
module.exports = function(CURRENCY) {
  //UpdateOrganizationContact remote method starts here
   /**
 *updateCurrency- To update Currency of the input
 *@constructor
 * @param {object} contactData - contains all the data need to get updated
 * @param {function} cb - deals with response
 */
  CURRENCY.updateCurrency = function(contactData, cb) {
    var modelData = require('../../commonValidation/sample.js');
    modelData.sampleFunction(contactData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };

  //UpdateOrganizationContact method to update organization contact
  CURRENCY.remoteMethod('updateCurrency', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateCurrency',
      verb: 'PUT',
    },
  });
};
