'use strict';
var validateModel = require('../../ServicesImpl/CommonImpl/models-validation.js').validateInput;
var employerDriveDetailsJson = require('./EMPLOYER_DRIVE.json').properties;
var employerDriveCampusesJson = require('./EMPLOYER_DRIVE_CAMPUSES.json').properties;
delete employerDriveCampusesJson['empDriveId'];
var lookup = require('../../commonValidation/lookupMethods').getLookupId;
var logger = require('../../server/boot/lib/logger');
var server = require('../../server/server');
var company = server.models.Company;
var emplEvent = server.models.EmployerEvent;
var employerDriveModel = server.models.EmployerDrive;
var async = require('async');
//var emplDrive = server.models.E
module.exports = function(EMPLOYER_DRIVE) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  var lookupMethods = require('../../commonValidation/lookupMethods');
  EMPLOYER_DRIVE.observe('before save', function employerDriveBeforeSave(ctx, next) {
    if (ctx.isNewInstance && ctx.instance.empDriveId == undefined) {
      var employerDriveTypeCode = 'EMPLOYER_DRIVE_TYPE_CODE';
      var employerDriveTypObj = {};
      employerDriveTypObj['lookupValueId'] = ctx.instance.driveTypeValueId;
      lookupMethods.typeCodeFunction(employerDriveTypObj, employerDriveTypeCode, function(employerEventTypeCodeStatus) {
        if (employerEventTypeCodeStatus) {
          ctx.instance.updateUserId = ctx.instance.createUserId;
          ctx.instance.createDatetime = new Date();
          ctx.instance.updateDatetime = new Date();
          logger.info('EmployerEvent Creation Initiated');
          next();
        } else {
          errorFunction('employerEventTypeCode validation error', next);
        }
      });
    } else if (ctx.isNewInstance) {
      errorFunction('empDriveId is system generated value', next);
    } else {
      next();
    }
  });
  // remote method definition to get employer drive details
  /**
   *getEmployerDriveDetails- To get employer drive details by taking required fields
   *@constructor
   * @param {number} empDriveId - Unique id for ecah and every drive
   * @param {function} callBc - deals with response
   */
  EMPLOYER_DRIVE.getEmployerDriveDetails = function(empDriveId, companyId, callBc) {
    var inputObj = {};
    var employerDriveModel = server.models.EmployerDrive;
    var entityDetailsUsingIncludeFilter = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (empDriveId || companyId) {
      inputObj['empDriveId'] = (empDriveId) ? empDriveId : undefined;
      inputObj['companyId'] = (companyId) ? companyId : undefined;
      var includeModels = ['employerDriveLookupValue', 'employerDriveIbfk2rel', 'employerDriveIbfk3rel'];
      // below function will give details for an entity based on loopback include filter
      entityDetailsUsingIncludeFilter(inputObj, employerDriveModel, includeModels, callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method definition to get employer drive details
  EMPLOYER_DRIVE.remoteMethod('getEmployerDriveDetails', {
    description: 'To get employer drive details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'empDriveId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'companyId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getEmployerDriveDetails',
      verb: 'GET',
    },
  });
  //  emp drive remoteMethod
  /**
   *updateEmployerDrive- To update employer drive Details
   *@constructor
   * @param {object} driveData - contains all the data need to get updated
   * @param {function} cb - deals with response
   */
  EMPLOYER_DRIVE.updateEmployerDrive = function(driveData, cb) {
    var updateDrive = require('../../commonCompanyFiles/update-employer-drive.js');
    updateDrive.updateEmpDriveService(driveData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };

  //update emp drive remoteMethod
  EMPLOYER_DRIVE.remoteMethod('updateEmployerDrive', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateEmployerDrive',
      verb: 'PUT',
    },
  });

  var employerDriveCreation = function(employerDrive) {
    var employerDriveModel = server.models.EmployerDrive;
    return new Promise(function(resolve, reject) {
      employerDriveModel.create(employerDrive, function(err, employerDriveResp) {
        if (err) {
          reject(err);
        } else if (employerDriveResp) {
          resolve(employerDriveResp);
        }
      });
    });
  };
  var employerDriveCampusesCreation = function(employerDrive, employerDriveCampuses) {
    return new Promise(function(resolve, reject) {
      var async = require('async');

      var empDriveId = employerDrive[0].empDriveId;
      async.map(employerDriveCampuses, createIndividualemployerDriveCampuses.bind({
        empDriveId: empDriveId,
      }),
        function(error, employerDriveCampusesCreateResp) {
          if (error) {
            reject(error);
          } else if (employerDriveCampusesCreateResp) {
            var response = {};
            response.employerDrive = employerDrive;
            response.employerDriveCampuses = employerDriveCampusesCreateResp;
            resolve(response);
          }
        });
    });
  };

  function createIndividualemployerDriveCampuses(employerDriveCampus, callback) {
    var employerDriveCampuses = server.models.EmployerDriveCampuses;
    employerDriveCampus.empDriveId = this.empDriveId;
    employerDriveCampuses.create(employerDriveCampus, function(err, employerDriveCampusResp) {
      callback(null, employerDriveCampusResp);
    });
  }
  EMPLOYER_DRIVE.createCompleteEmployerDrive = function(inputData, cb) {
    if (inputData.employerDrive.length == 1 && inputData.employerDriveCampuses.length > 0) {
      validateModel(inputData.employerDrive, employerDriveDetailsJson, function(err, employerDriveValidationResp) {
        validateModel(inputData.employerDriveCampuses, employerDriveCampusesJson, function(error, employerDriveCampusesValidationResp) {
          if (employerDriveValidationResp && employerDriveCampusesValidationResp) {
            employerDriveCreation(inputData.employerDrive).then(function(employerDriveResolveResp) {
              employerDriveCampusesCreation(employerDriveResolveResp, inputData.employerDriveCampuses).then(function(employerDriveCampusesResolveResp) {
                cb(null, employerDriveCampusesResolveResp);
              }, function(employerDriveCampusesRejectResp) {
                cb(employerDriveCampusesRejectResp, null);
              });
            }, function(employerDriveRejectResp) {
              cb(employerDriveRejectResp, null);
            });
          } else {
            errorResponse(cb);
          }
        });
      });
    }
  };
  //Create employerDrive
  EMPLOYER_DRIVE.remoteMethod('createCompleteEmployerDrive', {
    description: 'createCompleteEmployerDrive ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/createCompleteEmployerDrive',
      verb: 'POST',
    },
  });

  //-----getEventDetails service remote Method

  EMPLOYER_DRIVE.getEventDetails = function(inData, cb) {
    var company = server.models.Company;
    var emplEvent = server.models.EmployerEvent;
    var employerDriveModel = server.models.EmployerDrive;
    var jobRole = server.models.JobRole;
    var compnId = inData.companyId;
    var empeventid = inData.empEventId;
    var outPut = [];

    company.findOne({'where': {'companyId': compnId}}, function(err, outData) {
      if (err) {
        cb(err, null);
      } else {
        emplEvent.findOne({'where': {'empEventId': empeventid}}, function(eventErr, eventResp) {
          employerDriveModel.findOne({'where': {'empDriveId': eventResp.empDriveId}}, function(driveErr, driveOut) {
            jobRole.findOne({'where': {'jobRoleId': driveOut.jobRoleId}}, function(jobErr, jobOut) {
              var resp = {};
              resp.companyDetails = outData;
              resp.eventResponse = eventResp;
              resp.driveDetails = driveOut;
              resp.jobRoleDetails = jobOut;
              cb(null, resp);
            });
          });
        });
      }
    });
  };

  EMPLOYER_DRIVE.remoteMethod('getEventDetails', {
    description: 'getEventDetails',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/getEventDetails',
      verb: 'POST',
    },
  });
  EMPLOYER_DRIVE.closeDrive = function(companyId, empDriveId, cb) {
    var employerEvent = server.models.EmployerEvent;
    lookup('EMPLOYER_EVENT_STATUS_CODE', 'Closed', function(driveDetails) {
      var count = 0;
      employerEvent.find({'where': {'empDriveId': empDriveId, 'companyId': companyId}},
    function(err, res) {
      if (err) {
        errorFunction('err', cb);
      } else {
        async.map(res, getStatus, function(err1, res1) {
          if (err1) {
            cb(err1, null);
          } else {
            console.log(res1.length, count);
            if (count == res1.length) {
              lookup('EMPLOYER_DRIVE_STATUS_CODE', 'Closed', function(driveData) {
                EMPLOYER_DRIVE.updateAll({'empDriveId': empDriveId, 'companyId': companyId},
                {'driveStatusValueId': driveData.lookupValueId},
                function(err2, res2) {
                  if (err2) {
                    errorFunction('err', cb);
                  } else {
                    cb(null, 'Drive Closed Sucessfully');
                  }
                });
              });
            } else {
              errorFunction('some events are not closed', cb);
            }
          }
        });
      }
      function getStatus(obj, cb1) {
        if (obj.eventStatusValueId == driveDetails.lookupValueId) {
          count++;
          cb1(null, 'done');
        } else {
          cb1(null, 'done');
        }
      }
    });
    });
  };
  EMPLOYER_DRIVE.remoteMethod('closeDrive', {
    description: 'getEventDetails',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'companyId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'empDriveId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/closeDrive',
      verb: 'GET',
    },
  });
};
