'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
module.exports = function(EVENT_TEST_SCORE) {
    /**
 *getTestScoreDetails- To get test score details based on the id
 *@constructor
 * @param {number} eventTestScoreId - Unique id for ecah and every testcorefile
 * @param {function} callBc - deals with response
 */
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  EVENT_TEST_SCORE.getTestScoreDetails = function(eventTestScoreId, callBc) {
    var inputObj = {};
    var findEntity = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
    if (eventTestScoreId) {
      inputObj['eventTestScoreId'] = eventTestScoreId;
      var scoredetails = server.models.EventTestScore;
    // below function will give score details for an entity based on loopback include filter
      findEntity(inputObj, scoredetails, function(err, res) {
        if (err) {
          errorResponse(callBc);
        } else {
          callBc(null, res);
        }
      });
    }  else {
      errorResponse(callBc);
    } };
 // getting score details remote method
  EVENT_TEST_SCORE.remoteMethod('getTestScoreDetails', {
    description: 'To get student test score details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: {arg: 'eventTestScoreId ',
             type: 'number',
             required: true,
             http: {source: 'query'}},
    http: {
      path: '/getTestScoreDetails',
      verb: 'get',
    },
  });
  //testinggggggggggggggggg

  EVENT_TEST_SCORE.test = function(eventTestScoreId, callBc) {
    // console.log('00000000000000000');
    formSubmit(567, cb);
    function formSubmit(input, cb) {
      // console.log('in formSubmit');
      cb(null, input);
    }
    function cb(nul, data) {
      // console.log('in callBc');
      callBc(null, 567);
    }
  };
  EVENT_TEST_SCORE.remoteMethod('test', {
    description: 'To get student test score details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: {arg: 'eventTestScoreId ',
             type: 'number',
             required: true,
             http: {source: 'query'}},
    http: {
      path: '/test',
      verb: 'get',
    },
  });
};
