'use strict';
var logger = require('../../server/boot/lib/logger');
var server = require('../../server/server');
var async = require('async');
var cleanArray = require('../../commonValidation/common-mass-upload.js').cleanArray;
module.exports = function(EMPLOYER_CAMPUS_LIST_DTL) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  var lookupMethods = require('../../commonValidation/lookupMethods');
  EMPLOYER_CAMPUS_LIST_DTL.observe('before save', function employerCampusListDTLBeforeSave(ctx, next) {
    if (ctx.isNewInstance && ctx.instance.listCampusId == undefined) {
      ctx.instance.updateUserId = ctx.instance.createUserId;
      ctx.instance.createDatetime = new Date();
      ctx.instance.updateDatetime = new Date();
      logger.info('EmployerCampusListDTL Creation Initiated');
      next();
    } else if (ctx.isNewInstance) {
      errorFunction('EmployerCampusListDTL listCampusId is system generated value', next);
    } else {
      next();
    }
  });

  // remote method definition to get employer campus list details
  /**
   *getEmployerCampusListDtl- To get employer details taking required fields
   *@constructor
   * @param {object} listCampusId - Unique id for ecah and campus
   * @param {function} callBc - deals with response
   */
  EMPLOYER_CAMPUS_LIST_DTL.getEmployerCampusListDtl = function(listId, listCampusId, callBc) {
    var inputObj = {};
    var employerCampusListDtl = server.models.EmployerCampusListDtl;
    var employerCampusListDtlDetails = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (listId || listCampusId) {
      inputObj['listId'] = listId;
      inputObj['listCampusId'] = (listCampusId) ? listCampusId : undefined;
      var includeModels = ['employerCampusListDTLCompany', 'employerCampusListHdrData', 'employerCampusListDtlCampus'];
      // below function will give details for an entity based on loopback include filter
      employerCampusListDtlDetails(inputObj, employerCampusListDtl, includeModels, function(err, response) {
        if (response) {
          var campusSearchVw = server.models.CampusSearchVw;
          campusSearchVw.findOne({'where': {'campusId': response.data[0].campusId}}, function(err, resp) {
            response.data[0].campusInfo = resp;
            callBc(null, response);
          });
        } else {
          callBc(null, null);
        }
      });
    } else {
      errorResponse(callBc);
    }
  };
  // remote method definition to get employer campus list details
  EMPLOYER_CAMPUS_LIST_DTL.remoteMethod('getEmployerCampusListDtl', {
    description: 'To get employer campus list details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'listId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'listCampusId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getEmployerCampusListDtl',
      verb: 'GET',
    },
  });
  //UpdateEmployerCampusListDtl remote method starts here
  /**
   *updateEmployerCampusListDtl- To update employer campus list details
   *@constructor
   * @param {object} programData - contains all the data need to get updated
   * @param {function} cb - deals with response
   */
  EMPLOYER_CAMPUS_LIST_DTL.updateEmployerCampusListDtl = function(programData, cb) {
    var programUpdate = require('../../commonCompanyFiles/update-emp-campus-list-dtl');
    programUpdate.updateEmpCampusProgramService(programData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };

  //UpdateEmployerCampusListDtl method creation
  EMPLOYER_CAMPUS_LIST_DTL.remoteMethod('updateEmployerCampusListDtl', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateEmployerCampusListDtl',
      verb: 'PUT',
    },
  });

  // remote method definition to get employer campus list hdr and dtl details anirudh
  EMPLOYER_CAMPUS_LIST_DTL.getEmployerCampusList = function(jobRoleId, empDriveId, flag, callBc) {
    var inputObj = {};
    var employerCampusListHdr = server.models.EmployerCampusListHdr;
    var employerDriveCampuses = server.models.EmployerDriveCampuses;
    //  var entityDetailsById = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
    if (jobRoleId || empDriveId) {
      inputObj['jobRoleId'] = (jobRoleId) ? jobRoleId : undefined;
      inputObj['empDriveId'] = (empDriveId) ? empDriveId : undefined;
      if (empDriveId && flag == 'true') {
        inputObj['empEventId'] = null;
      }
      var modelsName = (jobRoleId) ? employerCampusListHdr : employerDriveCampuses;
      getDrivesList(inputObj, modelsName, function(err, response) {
        if (err) {
          errorFunction(err, callBc);
        } else if (response == null) {
          callBc(null, []);
        } else {
          if (inputObj.jobRoleId) {
            var hrdResponse = response.data;
            async.map(hrdResponse, getCampusList, function(error, campusList) {
              var output = [];
              output = cleanArray(campusList);
              async.map(output, getCampusId, function(finalErr, finalOutput) {
                var outputArray = [];
                outputArray = cleanArray(finalOutput);
                var out = {};
                out.data = outputArray;
                if (finalErr) {
                  errorFunction(finalErr, callBc);
                } else {
                  callBc(null, out);
                }
              });
            });
          } else {
            var hrdResponse = response.data;
            var output = [];
            output = cleanArray(hrdResponse);
            var listObj = {};
            listObj.campusListArray = [];
            listObj.listArray = [];
            for (var i = 0; i < output.length; i++) {
              listObj.listArray.push(output[i].listId);
              // listObj.campusListArray.push({'campusId': output[i].campusId});
            }
            var out = listObj.listArray;
            var uniqueEmpResp = out.filter(function(elem, index, self) {
              return index == self.indexOf(elem);
            });
            // console.log('............................. ', JSON.stringify(uniqueEmpResp));
            getListAndCampus(empDriveId, uniqueEmpResp, function(err, campResp) {
              if (err) {
                callBc(err, null);
              } else {
                var finalArray = [];
                finalArray = cleanArray(campResp);
                async.map(finalArray, getCampusId, function(finalErr, finalOutput) {
                  var outputArray = [];
                  outputArray = cleanArray(finalOutput);
                  var out = {};
                  out.data = outputArray;
                  if (finalErr) {
                    errorFunction(finalErr, callBc);
                  } else {
                    callBc(null, out);
                  }
                });
              }
            });
          }
        }
      });
    } else {
      errorResponse(callBc);
    }
  };

  function getListAndCampus(empDriveId, uniqueList, listCB) {
    async.map(uniqueList, getCampusInformation, function(campusError, campusOutput) {
      if (campusError) {
        listCB(campusError, null);
      } else {
        listCB(null, campusOutput);
      }
    });

    function getCampusInformation(obj, cb) {
      var employerDriveCampuses = server.models.EmployerDriveCampuses;
      employerDriveCampuses.find({
        'where': {
          'and': [{
            'empDriveId': empDriveId,
          }, {
            'listId': obj,
          }],
        },
      }, function(listErr, listRep) {
        // console.log('------------------------- ', listRep);
        if (typeof listRep !== 'undefined') {
          // the array is defined and has at least one element
          var listObj = {};
          listObj.campusListArray = [];
          listObj.listId = listRep[0].listId;
          for (var i = 0; i < listRep.length; i++) {
            listObj.campusListArray.push({
              'campusId': listRep[i].campusId,
            });
          }
          cb(null, listObj);
        } else {
          cb(null, null);
        }
      });
    }
  }

  function getCampusList(obj, callBack) {
    var employerCampusListDtl = server.models.EmployerCampusListDtl;
    var inputObj = {};
    var findCampusList = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
    inputObj['listId'] = obj.listId;
    employerCampusListDtl.find({
      'where': {
        'listId': obj.listId,
      },
    }, function(rrr, resp) {
      if (typeof resp !== 'undefined') {
        // the array is defined and has at least one element
        var listObj = {};
        listObj.campusListArray = [];
        listObj.listId = resp[0].listId;
        for (var i = 0; i < resp.length; i++) {
          listObj.campusListArray.push({
            'campusId': resp[i].campusId,
          });
        }
        callBack(null, listObj);
      } else {
        callBack(null, null);
      }
    });
  }

  function getCampusId(object, callBc) {
    var campusSearchVw = server.models.CampusSearchVw;
    var searchCampus = campusSearchVw.searchCampus;
    var employerCampusListDtl = server.models.EmployerCampusListDtl;
    var employerCampusListHdr = server.models.EmployerCampusListHdr;
    var listOfCampus = (object.campusListArray) ? object.campusListArray : object;
    async.map(listOfCampus, getCampus, function(campusErr, campusOut) {
      var campusOutPut = {};
      campusOutPut.CampDetails = campusOut;
      var listObj = {};
      if (object.listId) {
        employerCampusListHdr.findOne({
          'where': {
            'listId': object.listId,
          },
        }, function(listErr, listResp) {
          campusOutPut.listDetails = listResp;
          callBc(null, campusOutPut);
        });
      } else {
        callBc(null, campusOutPut);
      }
    });

    function getCampus(obj, campusCallBC) {
      var campusSearchVw = server.models.CampusSearchVw;
      var searchCampus = campusSearchVw.searchCampus;
      var campusId = (obj.campusId) ? obj.campusId : obj;
      searchCampus(campusId, undefined, undefined, undefined, undefined, undefined, undefined, 'N', undefined, undefined, undefined, undefined, function(err, searchResponse) {
        // employerCampusListHdr.findOne({
        //   'where': {
        //     'listId': object.listId,
        //   },
        // }, function(listErr, listResp) {
        var campusInfo = {};
        campusInfo = (searchResponse) ? searchResponse[0] : null;
        // campusHdr.EmployerCampusListHdr = listResp;
        campusCallBC(null, campusInfo);
        // });
      });
    }
  }
  // remote method definition to get employer campus list hdr and dtl details
  EMPLOYER_CAMPUS_LIST_DTL.remoteMethod('getEmployerCampusList', {
    description: 'To get employer campus list details',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'jobRoleId',
      type: 'number',
      http: {
        source: 'query',
      },
    }, {
      arg: 'empDriveId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'flag',
      type: 'string',
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getEmployerCampusList',
      verb: 'GET',
    },
  });
  // getEmployerList method to get list info based on campusId
  EMPLOYER_CAMPUS_LIST_DTL.getEmployerList = function(campusArray, callBack) {
    if (campusArray) {
      async.map(campusArray.campusList, getListData, function(err, listResponse) {
        if (err) {
          errorFunction(err, callBack);
        } else {
          callBack(null, listResponse);
        }
      });
    } else {
      errorFunction('Invalid Input', callBack);
    }

    function getListData(obj, cb) {
      EMPLOYER_CAMPUS_LIST_DTL.find({
        'where': {
          'campusid': obj.campusId,
        },
      }, function(listErr, listResp) {
        if (listErr) {
          errorFunction(listErr, cb);
        } else {
          cb(null, listResp);
        }
      });
    }
  };

  EMPLOYER_CAMPUS_LIST_DTL.remoteMethod('getEmployerList', {
    description: 'Send Valid Details ',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/getEmployerList',
      verb: 'POST',
    },
  });

  function getDrivesList(inputObj, modelsName, callBack) {
    var entityDetailsById = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
    entityDetailsById(inputObj, modelsName, function(err, response) {
      console.log(inputObj, response);
      if (err) {
        callBack(null, null);
      } else {
        callBack(null, response);
      }
    });
  }
  //............get campus list for event.....................
  EMPLOYER_CAMPUS_LIST_DTL.getCampusByEvent = function(data, eventCB) {
    var employerDriveCampuses = server.models.EmployerDriveCampuses;
    employerDriveCampuses.find({
      'where': {
        'and': [{
          'empEventId': data.empEventId,
        }, {
          'empDriveId': data.empDriveId,
        }],
      },
    }, function(driveErr, driveResp) {
      if (driveErr) {
        eventCB(driveErr, null);
      } else {
        if (driveResp.length > 0) {
          async.map(driveResp, getCampusInfo, function(campusErr, campusList) {
            if (campusErr) {
              eventCB(campusErr, null);
            } else {
              eventCB(null, campusList);
            }
          });
        } else {
          eventCB('Invalid data', null);
        }
      }
    });
  };
  EMPLOYER_CAMPUS_LIST_DTL.remoteMethod('getCampusByEvent', {
    description: 'Send Valid Details ',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/getCampusByEvent',
      verb: 'POST',
    },
  });

  function getCampusInfo(obj, campusCB) {
    var campusSearchVw = server.models.CampusSearchVw;
    var searchCampus = campusSearchVw.searchCampus;
    var employerCampusListHdr = server.models.EmployerCampusListHdr;
    searchCampus(obj.campusId, undefined, undefined, undefined, undefined, undefined, undefined, 'N', undefined, undefined, undefined, undefined, function(err, searchResponse) {
      if (searchResponse) {
        var resp = (searchResponse) ? searchResponse[0] : null;
        employerCampusListHdr.findOne({
          'where': {
            'listId': obj.listId,
          },
        }, function(listErr, listResp) {
          var campusHdr = {};
          resp.EmployerCampusListHdr = listResp;
          campusCB(null, resp);
        });
      } else {
        campusCB(err, null);
      }
    });
  }
  //............................getCampusByDrive..................................

  function getCampusByDrive(input, callBC) {
    var employerDriveCampuses = server.models.EmployerDriveCampuses;
  }

  EMPLOYER_CAMPUS_LIST_DTL.remoteMethod('getCampusByDrive', {
    description: 'Send Valid Details ',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/getCampusByDrive',
      verb: 'POST',
    },
  });
};
