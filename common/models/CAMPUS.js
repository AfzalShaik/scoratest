'use strict';
//requiring server path so that we can use exported functions or methods when ever we need
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
// exporting function to use it in another modules if requires
module.exports = function(CAMPUS) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  // when request comes to campus model then we execute logic or change request object before saving into database
  CAMPUS.createCampus = function(data, cb) {
    if (data.tireValueId) {
      // console.log('going in');
      // console.log(data.tireValueId);
      var lookupValue = server.models.LookupValue;
      var lookupType = server.models.LookupType;
      var status = [];
      lookupType.findOne({'where': {lookupCode: 'INSTITUTE_TIER'}}, function(err, lookupResponse) {
        if (err) {
          cb(err, null);
        } else {
          lookupValue.find({'where': {'lookupTypeId': lookupResponse.lookupTypeId}}, function(err, res) {
            if (err) {
              cb(err, null);
            } else {
              res.map(function(campusDet) {
                if (campusDet.lookupValueId == data.tireValueId) {
                  status.push('true');
                }
              });
              if (status[0] == 'true') {
                data.searchName = (data.name) ? data.name.toUpperCase() : null;
                data.searchShortName = (data.shortName) ? data.shortName.toUpperCase() : null;
                data.updateUserId = data.createUserId;
                data.createDatetime = new Date();
                data.updateDatetime = new Date();
                CAMPUS.create(data, function(err, res) {
                  if (err) {
                    cb(err, null);
                  } else {
                    cb(null, res);
                  }
                });
              } else {
                // console.log('the id was invalid');
                errorFunction('Invalid TierValueId', cb);
              }
            }
          });
        }
      });
    } else {
      data.searchName = (data.name) ? data.name.toUpperCase() : null;
      data.searchShortName = (data.shortName) ? data.shortName.toUpperCase() : null;
      data.updateUserId = data.createUserId;
      data.createDatetime = new Date();
      data.updateDatetime = new Date();
      CAMPUS.create(data, function(err, res) {
        if (err) {
          cb(err, null);
        } else {
          cb(null, res);
        }
      });
    }
  };
  CAMPUS.remoteMethod('createCampus', {
    description: 'Send Valid campusId ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: {
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    },
    http: {
      path: '/createCampus',
      verb: 'POST',
    },
  });
  // remote method definition to get campus details based on campus id
  /**
 *getCampus - To get campus details
 *@constructor
 * @param {number} campusId - contains unique campus id
 * @param {function} callBc - deals with response
 */
  CAMPUS.getCampus = function(campusId, callBc) {
    if (campusId) {
      var campusDetById = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
      var inputFilterObject = {};
      inputFilterObject['campusId'] = campusId;
      var campus = server.models.Campus;
      campusDetById(inputFilterObject, campus, callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to get campus details based on campus id
  CAMPUS.remoteMethod('getCampus', {
    description: 'To get campus details based on campus id',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getCampus',
      verb: 'get',
    },
  });
  /**
 *updateCampus- To update campus details
 *@constructor
 * @param {number} campusData - contains object need to get updated
 * @param {function} callBc - deals with response
 *///updateCampus custom method starts here
  CAMPUS.updateCampus = function(campusData, cb) {
    var updateCampusDet = require('../../commonCampusFiles/update-campus-profile');
    updateCampusDet.updateCampusProfile(campusData, function(err, resp) {
      logger.info('upating campus profile ');
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };
  //remoteMethod for campus update
  CAMPUS.remoteMethod('updateCampus', {
    description: 'Send Valid campusId ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateCampus',
      verb: 'PUT',
    },
  });
    //remoteMethod for campus Profile
  CAMPUS.getCampusDashboard = function(inputObj, callBc) {
    var getCampusDashboardDetails = require('../../commonCampusFiles/get-campus-dashboard.js').getCampus;
    getCampusDashboardDetails(inputObj, function(err, response) {
      if (err) {
        callBc(err, null);
      } else {
        callBc(null, response);
      }
    });
  };
  CAMPUS.remoteMethod('getCampusDashboard', {
    description: 'Send Valid campusId ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/getCampusDashboard',
      verb: 'POST',
    },
  });
//remote method to get graphs for each department

  CAMPUS.getDepartmentData = function(inputObj, callBc) {
    var getGraphsInfo = require('../../commonCampusFiles/get-campus-graphs').getDepartmentGraphs;
    getGraphsInfo(inputObj, function(err, response) {
      if (err) {
        callBc(err, null);
      } else {
        callBc(null, response);
      }
    });
  };
  CAMPUS.remoteMethod('getDepartmentData', {
    description: 'Send Valid campusId ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/getDepartmentData',
      verb: 'POST',
    },
  });
  CAMPUS.getCampusProfile = function(campusId, callBc) {
    var getProfile = require('../../commonCampusFiles/campus-profile').getCampusProfile;
    getProfile(campusId, function(err, profileRes) {
      if (err) {
        errorFunction('error', callBc);
      } else {
        callBc(null, profileRes);
      }
    });
  };
  CAMPUS.remoteMethod('getCampusProfile', {
    description: 'Send Valid campusId ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getCampusProfile',
      verb: 'GET',
    },
  });
  CAMPUS.getGraphAndTableData = function(campusId, callBc) {
    var getProfile = require('../../commonCampusFiles/campus-profile').getGraphAndTableData;
    var date = new Date().getFullYear();
    var month = new Date().getMonth();
    if (month < 6) {
      date = date - 1;
    }
    getProfile(campusId, date, function(err, profileRes) {
      if (err) {
        errorFunction('error', callBc);
      } else {
        callBc(null, profileRes);
      }
    });
  };
  CAMPUS.remoteMethod('getGraphAndTableData', {
    description: 'Send Valid campusId ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getGraphAndTableData',
      verb: 'GET',
    },
  });
  CAMPUS.getCampusGraphDataForDashboard = function(campusId, callBc) {
    var graphDataOfCampus = require('../../commonCampusFiles/campus-dashboard').campusDashboard;
    graphDataOfCampus(campusId, function(err, campusGraphDataResponse) {
      if (err) {
        callBc(err, null);
      } else {
        callBc(err, campusGraphDataResponse);
      }
    });
  };
  CAMPUS.remoteMethod('getCampusGraphDataForDashboard', {
    description: 'Send Valid campusId ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getCampusGraphDataForDashboard',
      verb: 'GET',
    },
  });
  CAMPUS.getRecentPlacements = function(campusId, cb) {
    var endDate = new Date();
    var currentYear = new Date().getFullYear();
    var startDate = new Date();
    var month = new Date().getMonth();
    startDate.setFullYear(currentYear, (month - 1), 1);
    var eventList = server.models.EventStudentList;
    eventList.find({'where': {'campusId': campusId, 'candidateStatusValueId': 381, 'updateDatetime': {
      between: [startDate, endDate],
    }, order: 'updateDatetime DESC'}}, function(err, response) {
      if (err) {
        cb(err, null);
      } else {
        var async = require('async');
        async.map(response, getNameAndDetails,
          function(err, resp) {
            if (err) {
              cb(err, null);
            } else {
              var finalArray = resp.reverse();
              cb(null, finalArray);
            }
          });
      }
    });
  };
  function getNameAndDetails(eventStudentObject, callBack) {
    var student = server.models.Student;
    student.findOne({'where': {'studentId': eventStudentObject.studentId}},
    function(err, studentDetails) {
      if (err) {
        callBack(err, null);
      } else {
        var company = server.models.Company;
        company.findOne({'where': {'companyId': eventStudentObject.companyId}},
        function(err, companyResponse) {
          if (err) {
            callBack(err, null);
          } else {
            var enrollment = server.models.Enrollment;
            enrollment.findOne({'where': {'studentId': eventStudentObject.studentId}},
            function(err, enrollObject) {
              if (err) {
                callBack(err, null);
              } else {
                var program = server.models.Program;
                program.findOne({'where': {'programId': enrollObject.programId}},
                function(err, programResponse) {
                  if (err) {
                    callBack(err, null);
                  } else {
                    var department = server.models.Department;
                    department.findOne({'where': {'departmentId': programResponse.departmentId}},
                    function(err, departmentResponse) {
                      if (err) {
                        callBack(err, null);
                      } else {
                        var data = {};
                        data['Student Name'] = studentDetails.firstName + ' ' + studentDetails.lastName;
                        data['Placed in Company'] = companyResponse.name;
                        data['Department'] = departmentResponse.shortName;
                        // data.updateDatetime = eventStudentObject.updateDatetime;
                        data['Program'] = programResponse.programName;
                        callBack(null, data);
                      }
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
  }
  CAMPUS.remoteMethod('getRecentPlacements', {
    description: 'Send Valid campusId ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getRecentPlacements',
      verb: 'GET',
    },
  });
  CAMPUS.upCommingEvents = function(campusId, cb) {
    var campusEvents = server.models.CampusEvent;
    campusEvents.find({'where': {'campusId': campusId, 'scheduledDate': {
      gte: new Date().getDate(),
    }}},
    function(err, resp) {
      if (err) {
        cb(err, null);
      } else {
        var async = require('async');
        async.map(resp, getThisEventCount,
          function(err, response) {
            if (err) {
              cb(err, null);
            } else {
              cb(null, response);
            }
          });
      }
    });
    var shortlisted = 0;
    function getThisEventCount(obj, cb) {
      var eventList = server.models.EventStudentList;
      eventList.find({'where': {'employerEventId': obj.empEventId}},
      function(err, response) {
        if (err) {
          cb(err, null);
        } else {
          var async = require('async');
          async.map(response, getStudentShortListedCount,
            function(err, resp) {
              if (err) {
                cb(err, null);
              } else {
                var date = new Date(obj.scheduledDate).getDay() + '/' +
                           new Date(obj.scheduledDate).getMonth() + '/' +
                           new Date(obj.scheduledDate).getFullYear();
                var data = {};
                data['Event Name'] = obj.eventName;
                data['No of Students'] = shortlisted;
                data['Date'] = date;
                data['Event Type'] = obj.eventTypeValueId;
                cb(null, data);
                shortlisted = 0;
              }
            });
        }
      });
    }
    function getStudentShortListedCount(obj, cb) {
      if (obj.candidateStatusValueId == 377) {
        shortlisted++;
        cb(null, 'done');
      } else {
        cb(null, 'done');
      }
    }
  };
  CAMPUS.remoteMethod('upCommingEvents', {
    description: 'Send Valid campusId ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/upCommingEvents',
      verb: 'GET',
    },
  });
  var uniqueCompanyIds = [];
  var details = [];
  var orgDetails = [];
  var campusDetails = [];
  var companySelectedCount = [];
  var campusNameAndSelected = [];
  var selected = 0;
  CAMPUS.getCampusHiringData = function(campusId, departmentId, cb) {
    var endDate = new Date();
    var startDate = new Date();
    var currentYear = new Date().getFullYear();
    var month = new Date().getMonth();
    // console.log(currentYear + 1);
    if (month > 6) {
      startDate.setFullYear((currentYear), 5, 1);
      endDate.setFullYear((currentYear + 1), 4, 31);
    } else if (month <= 6) {
      startDate.setFullYear((currentYear - 1), 5, 1);
      endDate.setFullYear((currentYear), 4, 31);
    }
    if (campusId && departmentId) {
      var campusDrive = server.models.CampusDrive;
      campusDrive.find({'where': {'campusId': campusId, 'departmentId': departmentId,
        'createDateTime':
        {
          between: [startDate, endDate],
        },
    }},
      function(err, res) {
        if (err) {
          cb(err, null);
        } else {
          var async = require('async');
          async.map(res, getDepBasedEventDetails, function(err, orgResp) {
            if (err) {
              cb(err, null);
            } else {
              var async = require('async');
              async.map(uniqueCompanyIds, getCompanySelectedCount, function(err, res) {
                if (err) {
                  cb(err, null);
                } else {
                  async.map(companySelectedCount, getCompanyAndNameDetails,
                    function(err, companyDataAndNames) {
                      if (err) {
                        cb(err, null);
                      } else {
                        cb(null, companyDataAndNames);
                        selected = 0;
                        uniqueCompanyIds = [];
                        details = [];
                        orgDetails = [];
                        campusDetails = [];
                        companySelectedCount = [];
                        campusNameAndSelected = [];
                      }
                    });
                }
              });
            }
          });
        }
      });
    } else {
      var statment = 'select count(student_id) totalOffers, company_id companyId from EVENT_STUDENT_LIST where candidate_status_value_id = 381 and campus_id =' + campusId + ' and update_datetime BETWEEN' +
      '' + JSON.stringify(startDate) + ' and ' + JSON.stringify(endDate) + ' group by company_id';
      MySql(statment, function(err, response) {
        if (err) {
          cb(err, null);
        } else {
          var async = require('async');
          async.map(response, getCompanyName, function(err, resp) {
            if (err) {
              cb(err, null);
            } else {
              cb(null, resp);
            }
          });
        }
      });
    }
  };
  function getDepBasedEventDetails(obje, cb) {
    var campEvent = server.models.CampusEvent;
    campEvent.find({'where': {'campusDriveId': obje.campusDriveId}},
    function(err, resp) {
      if (err) {
        cb(err, null);
      } else if (resp == null) {
        cb(null, 'done');
      } else {
        var async = require('async');
        async.map(resp, getEventName, function(err, res) {
          if (err) {
            cb(err, null);
          } else {
            cb(null, res);
          }
        });
      }
    });
  }
  function getEventName(obj, cb) {
    var eventStudent = server.models.EventStudentList;
    eventStudent.find({'where': {'campusEventId': obj.campusEventId}},
    function(er, response) {
      if (er) {
        cb(er, null);
      } else {
        var async = require('async');
        async.map(response, getCountsOfStudents,
          function(err, resp) {
            if (err) {
              cb(err, null);
            } else {
              cb(null, 'done');
            }
          });
      }
    });
  }
  function getCountsOfStudents(obj, cb) {
    if (obj.candidateStatusValueId == 381) {
      details.push(obj);
      if (uniqueCompanyIds.includes(parseInt(obj.companyId))) {
        cb(null, 'done');
      } else {
        uniqueCompanyIds.push(parseInt(obj.companyId));
        cb(null, 'done');
      }
    } else {
      cb(null, 'done');
    }
  }
  function getCompanySelectedCount(companyId, cb) {
    selected = 0;
    for (var i = 0; i < details.length; i++) {
      if (details[i].companyId == companyId) {
        selected++;
      } if (i == details.length - 1) {
        var data = {};
        data.companyId = companyId;
        data.selected = selected;
        companySelectedCount.push(data);
        cb(null, 'done');
        selected = 0;
      }
    }
  }
  function getCompanyAndNameDetails(obj, cb) {
    var company = server.models.Company;
    company.findOne({'where': {'companyId': obj.companyId}, 'fields': {
      'name': true,
    }}, function(err, companyName) {
      if (err) {
        cb(err, null);
      } else {
        var data = {};
        data.companyName = companyName.name;
        data.companyId = obj.companyId;
        data.totalOffers = obj.selected;
        // campusNameAndSelected.push(data);
        cb(null, data);
      }
    });
  }
  function getCompanyName(obj, callBc) {
    var company = server.models.Company;
    company.findOne({'where': {'companyId': obj.companyId},
      'fields': {
        'name': true,
      }}, function(err, resp) {
      if (err) {
        callBc(err, null);
      } else {
        var companyData = {};
        companyData.companyName = resp.name;
        companyData.companyId = obj.companyId;
        companyData.totalOffers = obj.totalOffers;
        callBc(null, companyData);
      }
    });
  }
  function MySql(statment, sqlCb) {
    var datasource = require('../../server/datasources.json');
    var mysql = require('mysql');
    var con = mysql.createConnection({
      host: datasource.ScoraXChangeDB.host,
      user: datasource.ScoraXChangeDB.user,
      password: datasource.ScoraXChangeDB.password,
      database: datasource.ScoraXChangeDB.database,
    });
    con.query(statment, function(err, result, fields) {
      if (err) {
        sqlCb(err, null);
      } else {
        sqlCb(null, result);
      }
    });
  }
  CAMPUS.remoteMethod('getCampusHiringData', {
    description: 'Send Valid campusId ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'departmentId',
      type: 'number',
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getCampusHiringData',
      verb: 'GET',
    },
  });
};
