'use strict';
module.exports = function(STUDENTS_MASS_UPLOAD_WORK) {
  //mass upload remote method starts here
  var server = require('../../server/server');
  var async = require('async');
  STUDENTS_MASS_UPLOAD_WORK.studentMassUpload = function(stDataa, cb) {
    // var inputFile = './commonValidation/Mass_Upload_1.csv';
    var name = stDataa.fileDetails.name;
    var container = stDataa.fileDetails.container;
    //testtttttttttttttttttt
    var pathForm = require('path');
    var inputFile = './attachments/' + container + '/' + name;
    var fullPathForm = pathForm.join(__dirname, '../../../../', inputFile);
    // console.log('...............fullPathForm................. ', fullPathForm);
    // testttttttttttttttt
    // var inputFile = './attachments/' + container + '/' + name;
    var readCsvFile = require('../../commonValidation/common-mass-upload.js').readFile;
    var cleanArray = require('../../commonValidation/common-mass-upload.js').cleanArray;
    var studentMassJson = require('./STUDENTS_MASS_UPLOAD_WORK.json');
    var throwError = require('../../ErrorHandling/customizeErrorTypes').errorFunction;
    var logger = require('../../server/boot/lib/logger');
    var output = [];
    STUDENTS_MASS_UPLOAD_WORK.destroyAll({}, function(destroyError, destroyOutput) {
      readCsvFile(fullPathForm, function(err, fileResponses) {
        // console.log(err, fileResponse);
        if (err) {
          cb(err, null);
        } else {
          // validateDuplicateAdmission(fileResponse, function(adminErr, adminResponse) {
          //   console.log('entereddddddddddddddddddddddddd');
          //   cb(null, adminResponse);
          // });
          var fileResponse = cleanArray(fileResponses);
          // console.log('fileresponseeeeeeeeeeeeeeeeeeee ', fileResponse);
          if (fileResponse.length > 0) {
            var firstObj = fileResponse[0];
            // console.log(firstObj.firstName, firstObj.firstName == '');
            if ((firstObj.firstName || firstObj.firstName == '') && (firstObj.lastName || firstObj.lastName == '') && (firstObj.email || firstObj.email == '') && (firstObj.admissionNo || firstObj.admissionNo == '') && (firstObj.startDate || firstObj.startDate == '')  && (firstObj.plannedCompletionDate || firstObj.plannedCompletionDate == '')) {
              var createMass = require('../../commonCampusFiles/student-mass-upload.js').createMassUpload;
              for (var i = 0; i < fileResponse.length; i++) {
                var obj = {};
                obj = fileResponse[i];
                obj.rowNumber = i + 1;
                obj.campusId = stDataa.campusId;
                obj.programId = stDataa.programId;
                if (obj.startDate) {
                  var start = obj.startDate.split('/');
                  start = new Date(start[1] + '-' + start[0] + '-' + start[2]);
                  obj.startDate = start;
                }
                if (obj.plannedCompletionDate) {
                  var plannedCompletionDate = obj.plannedCompletionDate.split('/');
                  plannedCompletionDate = new Date(plannedCompletionDate[1] + '-' + plannedCompletionDate[0] + '-' + plannedCompletionDate[2]);
                  obj.plannedCompletionDate = plannedCompletionDate;
                }
                output.push(obj);
              }
              createMass(output, stDataa, function(createErr, createResponse) {
                if (createErr) {
                  throwError(createErr, cb);
                } else {
                  output = [];
                  cb(null, createResponse);
                }
              });
            } else {
              throwError('Invalid Csv File Uploaded ', cb);
            }
          } else {
            throwError('Empty File Cannot be Uploaded. ', cb);
          }
        }
      });
    });
  };

  //mass upload method creation
  STUDENTS_MASS_UPLOAD_WORK.remoteMethod('studentMassUpload', {
    description: 'Send Valid Data ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/studentMassUpload',
      verb: 'POST',
    },
  });
};
