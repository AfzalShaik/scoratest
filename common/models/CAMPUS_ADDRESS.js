'use strict';
var server = require('../../server/server');
module.exports = function(CAMPUS_ADDRESS) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  var throwerror = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  // remote method definition to create address for particular campus
/**
 *createCampusAddress- To create Campus address details
 *@constructor
 * @param {object} data - Contains data need to be created
 * @param {function} callBc - deals with the response
 */
  CAMPUS_ADDRESS.createCampusAddress = function(data, callBc) {
    if (data.campusId && data.addresses.length > 0) {
      var myArray = data.addresses;
      var iterateobj = {};
      var count = 0;
      for (var j = 0; j < myArray.length; j++) {
        iterateobj = myArray[j];
        if (myArray[j].primaryInd == 'Y') {
          iterateobj = myArray[j];
          count++;
        }
      }
      // console.log('this is count', count);
      if (count <= 1) {
        if (count == 1) {
          searchForInd(data, iterateobj, function(err, res) {
            if (err) {
              errorResponse('there was an error', callBc);
            } else {
              // console.log('sucess');
            }
          });
        }
        var checkEntityExistence = require('../../ServicesImpl/CommonImpl/validateEntityRecord.js').checkEntitySingleRecordExistence;
        var inputFilterObject = {};
        inputFilterObject['campusId'] = data.campusId;
        var addressModel = server.models.Address;
        var campusModel = server.models.Campus;
      // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- too much cyclomatic complexity is bad.
      // Break down inline onsuccess or atleast provide a name for them so we understand what they do
      // These methods can then be unit tested separately.
      // Use Early returns if a flow can be broken out to avoid more code execution.
        checkEntityExistence(campusModel, inputFilterObject,
        function(checkStatus) {
          if (checkStatus) {
            addressModel.create(data.addresses, function(error, addressRes) {
              if (error) {
                callBc(error, null);
              } else if (addressRes) {
                var persistAddressDataInForeignEntity = require('../../ServicesImpl/AddressImpl/persistAddressRelationData.js').persistAddressDataInForeignEntity;
                var campusAddressModel = server.models.CampusAddress;
                var inputObj = {};
                inputObj['campusId'] = Number(data.campusId);
              // inputObjToPersist['addressId']=Number(addressRes.addressId);
                persistAddressDataInForeignEntity(addressRes.length, addressRes,
                                          campusAddressModel, inputObj, callBc);
              } else {
                errorResponse(callBc);
              }
            });
          } else {
            errorResponse(callBc);
          }
        });
      } else {
        throwerror('cant have two primary ind Y', callBc);
      }
    } else {
      errorResponse(callBc);
    }
  };
  function searchForInd(data, priCallback) {
    CAMPUS_ADDRESS.find({'where': {'campusId': data.campusId, 'primaryInd': 'Y'}},
    function(err, res) {
      if (err) {
        errorResponse('campusId or Education person id was invalid', callBc);
      } else if (res == 0) {
        data.primaryInd = data.primaryInd;
        priCallback(null, res);
      } else {
        var update = {};
        update.primaryInd = 'N';
        CAMPUS_ADDRESS.updateAll({'educationPersonId': data.educationPersonId,
        'campusId': data.campusId, 'primaryInd': 'Y'}, update, function(err, resp) {
          if (err) {
            errorResponse('there was an error');
          } else {
            // console.log('this is response of update', resp);
            priCallback(null, resp);
          }
        });
      }
    });
  }
  // remote method declaration to create address for particular campus
  CAMPUS_ADDRESS.remoteMethod('createCampusAddress', {
    description: 'Useful to create address for particular campus',
    returns: {

      type: 'array', // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- No space required. Clean up.
      root: true,

    },
    accepts: [{
      arg: 'data',
      type: 'object',
      required: true,
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/createCampusAddress',
      verb: 'post',
    },
  });
  // remote method definition to get campus address details
  /**
 *getCampusAddress- To get Campus address details
 *@constructor
 * @param {number} campusId - unique id of campus
 * @param {number} addressId - unique id of address
 * @param {function} callBc - deals with the response
 */
  CAMPUS_ADDRESS.getCampusAddress = function(campusId, addressId, callBc) {
    var inputObj = {};
    var campusAddress = server.models.CampusAddress;
    // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Dont need to abbreviate.. Could be getCampusAddressDetails
    // For all reads. use "get" prefix.
    var campusAddressDet = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').getCompleteAddress;
    if (campusId && addressId) {
      inputObj['campusId'] = campusId;
      inputObj['addressId'] = addressId;
      // below function will give address details for an entity based on loopback include filter
      campusAddressDet(inputObj, campusAddress, 'campusAddress', callBc);
    } else if (campusId) {
      inputObj['campusId'] = campusId;
      // below function will give campus address details for an entity based on loopback include filter
      campusAddressDet(inputObj, campusAddress, 'campusAddress', callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to get campus address details
  CAMPUS_ADDRESS.remoteMethod('getCampusAddress', {
    description: 'To get campus address details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'addressId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getCampusAddress',
      verb: 'get', // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Uniform use GET
    },
  });
  // remote method definition to delete campus address details
  /**
 *deleteCampusAddress- To delete campus address details
 *@constructor
 * @param {number} campusId - unique id of campus
 * @param {number} addressId - unique id of address
 * @param {function} callBc - deals with response
 */
  CAMPUS_ADDRESS.deleteCampusAddress = function(campusId, addressId, callBc) {
    if (campusId && addressId) {
      var campusAddressModel = server.models.CampusAddress;
      var addressModel = server.models.Address;
      var deleteCampusAddress = require('../../ServicesImpl/CommonImpl/deleteEntityRecord.js').deleteEntityRecordExceptPrimary;
      var primaryCheckInput = {};
      primaryCheckInput['addressId'] = addressId;
      var inputFilterObject = {};
      inputFilterObject['campusId'] = campusId;
      inputFilterObject['addressId'] = addressId;
      deleteCampusAddress(primaryCheckInput,
                          inputFilterObject,
                          addressModel,
                          campusAddressModel,
                          callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to delete campus address details
  CAMPUS_ADDRESS.remoteMethod('deleteCampusAddress', {
    description: 'To delete campus address',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'addressId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/deleteCampusAddress',
      verb: 'delete',
    },
  });
  // Update campus Address remoteMethod starts here
  /**
 *updateCampusAddress- To update Campus address details
 *@constructor
 * @param {object} addressData - Fields to get updated
 * @param {function} callBc - deals with response
 */
  CAMPUS_ADDRESS.updateCampusAddress = function(addressData, cb) {
    var campusUpdate = require('../../commonCampusFiles/update-campus-address.js');
    if (addressData.primaryInd == 'Y') {
      searchForInd(addressData, function(err, res) {
        if (err) {
          errorResponse('there was an error', cb);
        } else {
          // console.log('sucess');
        }
      });
    }
    campusUpdate.updateCampus(addressData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        var updatedData = {};
        updatedData.primaryInd = addressData.primaryInd;
        CAMPUS_ADDRESS.updateAll({'campusId': addressData.campusId, 'addressId': addressData.addressId},
        updatedData, function(err, res) {
          if (err) {
            errorResponse(err, cb);
          } else {
            cb(null, resp);
          }
        });
      }
    });
  };
  //update department address remoteMethod
  CAMPUS_ADDRESS.remoteMethod('updateCampusAddress', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateCampusAddress', // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Camel casing.
      verb: 'PUT',
    },
  });
};
