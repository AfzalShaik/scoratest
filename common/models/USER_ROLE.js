'use strict';
var server = require('../../server/server');
module.exports = function(USER_ROLE) {
  USER_ROLE.sendEmail = function(data, callBc) {
    var email = server.models.Email;
    email.send({
      to: data.emailId,
      from: 'afzal.iiit@gmail.com',
      subject: 'Contact Us',
      text: data.message,
      html: data.message,
    }, function(err, mail) {
      if (err) {
        callBc(err, null);
      } else {
        callBc(null, data);
      }
    });
  };
  USER_ROLE.remoteMethod('sendEmail', {
    description: 'Send Email',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      required: true,
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/sendEmail',
      verb: 'post',
    },
  });
};
