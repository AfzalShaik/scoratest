'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
module.exports = function(ORGANIZATION_CONTACT) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  // remote method definition to create organization contact details
   /**
 *createOrganizationContact- To Create Organization Contact Details
 *@constructor
 * @param {object} data - contains all the data need to get created
 * @param {function} callBc - deals with response
 */
  ORGANIZATION_CONTACT.createOrganizationContact = function(data, callBc) {
    if (data.companyId && data.organizationId) {
      if (data.primaryInd == 'Y') {
        searchForInd(data, function(err, res) {
          if (err) {
            errorResponse('there was an error', callBc);
          } else {
            // console.log('sucess');
          }
        });
      }
      var checkEntityExistence = require('../../ServicesImpl/CommonImpl/validateEntityRecord.js').checkEntitySingleRecordExistence;
      var inputFilterObject = {};
      inputFilterObject['companyId'] = data.companyId;
      inputFilterObject['organizationId'] = data.organizationId;
      var contactModel = server.models.Contact;
      var organizationModel = server.models.Organization;
      checkEntityExistence(organizationModel, inputFilterObject, function(checkStatus) {
        if (checkStatus) {
          contactModel.create(data, function(error, contactRes) {
            if (error) {
              logger.error('Error occured in contactModel.create()');
              callBc(error, null);
            } else if (contactRes) {
              ORGANIZATION_CONTACT.create({
                'organizationId': Number(data.organizationId),
                'companyId': Number(data.companyId),
                'contactId': Number(contactRes.contactId),
                'primaryInd': String(data.primaryInd),
              }, function(err, resp) {
                if (err) {
                  logger.error('error while creating organization contact');
                  errorResponse(callBc);
                } else {
                  // contactRes.requestStatus = true;
                  logger.info('organization contact is created');
                  callBc(null, contactRes);
                }
              });
            } else {
              logger.error('error while creating organization contact model');
              errorResponse(callBc);
            }
          });
        } else {
          logger.error('error while checking the entity presence');
          errorResponse(callBc);
        }
      });
    } else {
      logger.error('organizationId and contactId are required fields');
      errorResponse(callBc);
    }
  };
  function searchForInd(data, priCallback) {
    ORGANIZATION_CONTACT.find({'where': {'companyId': data.companyId,
    'organizationId': data.organizationId, 'primaryInd': 'Y'}}, function(err, res) {
      if (err) {
        errorResponse('company or org id was invalid', callBc);
      } else if (res == 0) {
        data.primaryInd = data.primaryInd;
      } else {
        var update = {};
        update.primaryInd = 'N';
        ORGANIZATION_CONTACT.updateAll({'companyId': data.companyId,
        'organizationId': data.organizationId, 'primaryInd': 'Y'}, update, function(err, resp) {
          if (err) {
            errorResponse('there was an error');
          } else {
            priCallback(null, resp);
          }
        });
      }
    });
  }
  // remote method declaration to create organization contact details
  ORGANIZATION_CONTACT.remoteMethod('createOrganizationContact', {
    description: 'Useful to create organization contact details',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      required: true,
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/createOrganizationContact',
      verb: 'post',
    },
  });
  // remote method definition to get  organization contact details
    /**
 *getOrganizationContact- To get Organization Contact details by taking required fields
 *@constructor
 * @param {object} organizationId - Unique id for ecah and every organization
 * @param {number} companyId - unique id for each and every company
 * @param {number} contactId - unique id for every contact
 * @param {function} callBc - deals with response
 */
  ORGANIZATION_CONTACT.getOrganizationContact = function(organizationId, companyId, contactId, callBc) {
    var inputObj = {};
    var organizationContactModel = server.models.OrganizationContact;
    var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
    var getOrganizationContactDet = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (organizationId && companyId && contactId) {
      inputObj['organizationId'] = organizationId;
      inputObj['companyId'] = companyId;
      inputObj['contactId'] = contactId;
      logger.info('based on organizationId,contactId and companyId fetching data');
      // below function will give contact details for an entity based on loopback include filter
      getOrganizationContactDet(inputObj, organizationContactModel, 'organizationContact', callBc);
    } else if (organizationId && companyId) {
      inputObj['organizationId'] = organizationId;
      inputObj['companyId'] = companyId;
      logger.info('based on organizationId and companyId fetching data');
      // below function will give contact details for an entity based on loopback include filter
      getOrganizationContactDet(inputObj, organizationContactModel, 'organizationContact', callBc);
    } else {
      logger.error('error while fetching organization contact details');
      errorResponse(callBc);
    }
  };
  // remote method declaration to get organization contact details
  ORGANIZATION_CONTACT.remoteMethod('getOrganizationContact', {
    description: 'To get organization contact details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'organizationId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'companyId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'contactId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getOrganizationContact',
      verb: 'get',
    },
  });
  // remote method definition to delete organization contact details
   /**
 *deleteOrganizationContact- To delete Organization Conatct Details
 *@constructor
 * @param {object} orginizationId - Unique Id for each and every Organization
 * @param {number} companyId - Unique Id for each and every company
 * @param {number} contactId - Unique Id for each and every contact
 * @param {function} callBc - deals with response
 */
  ORGANIZATION_CONTACT.deleteOrganizationContact = function(organizationId, companyId, contactId, callBc) {
    if (organizationId && companyId && contactId) {
      var organizationContactModel = server.models.OrganizationContact;
      var contactModel = server.models.Contact;
      var delorganizationContact = require('../../ServicesImpl/CommonImpl/deleteEntityRecord.js').deleteEntityRecordExceptPrimary;
      var primaryCheckInput = {};
      primaryCheckInput['contactId'] = contactId;
      var inputFilterObject = {};
      inputFilterObject['organizationId'] = organizationId;
      inputFilterObject['companyId'] = companyId;
      inputFilterObject['contactId'] = contactId;
      logger.info('deleting organization contactId');
      delorganizationContact(primaryCheckInput, inputFilterObject, contactModel, organizationContactModel, callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to delete organization contact details
  ORGANIZATION_CONTACT.remoteMethod('deleteOrganizationContact', {
    description: 'To delete delete organization contact details',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'organizationId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'companyId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'contactId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/deleteOrganizationContact',
      verb: 'delete',
    },
  });
  var Joi = require('joi');
   /**
 *updateOrganizationContact- To update Organization Contact Details
 *@constructor
 * @param {object} contactData - contains all the data need to get updated
 * @param {function} cb - deals with response
 *///UpdateOrganizationContact remote method starts here

  ORGANIZATION_CONTACT.updateOrganizationContact = function(contactData, cb) {
    var orgContactUpdate = require('../../commonCompanyFiles/update-organization-contact.js');
    if (contactData.primaryInd == 'Y') {
      searchForInd(contactData, function(err, res) {
        if (err) {
          errorResponse('there was an error', callBc);
        } else {
          // console.log('sucess');
        }
      });
    }
    logger.info('updating organization contact');
    //updating organization contact
    orgContactUpdate.updateOrgContact(contactData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        var updatetData = {};
        updatetData.primaryInd = contactData.primaryInd;
        ORGANIZATION_CONTACT.updateAll({'companyId': contactData.companyId,
        'contactId': contactData.contactId, 'organizationId': contactData.organizationId},
        updatetData, function(err, res) {
          if (err) {
            errorResponse(cb);
          } else {
            cb(null, resp);
          }
        });
      }
    });
  };

  //UpdateOrganizationContact method to update organization contact
  ORGANIZATION_CONTACT.remoteMethod('updateOrganizationContact', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateOrganizationContact',
      verb: 'PUT',
    },
  });
};
