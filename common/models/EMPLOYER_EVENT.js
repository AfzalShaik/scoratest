'use strict';
var validateModel = require('../../ServicesImpl/CommonImpl/models-validation.js').validateInput;

var getEmployerEventJson = require('./EMPLOYER_EVENT.json').properties;

var getEmployerEventPanelJson = require('./EMPLOYER_EVENT_PANEL.json').properties;

delete getEmployerEventPanelJson['empEventId'];

var logger = require('../../server/boot/lib/logger');

var server = require('../../server/server');
var employerEventOnCampusActions = require('../../ServicesImpl/EmployerEventActionImpl/employer-event-oncampus-actions.js').onCampusActions;
var employerEventOffCampusActions = require('../../ServicesImpl/EmployerEventActionImpl/employer-event-offcampus-actions.js').offCampusActions;

module.exports = function(EMPLOYER_EVENT) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  var lookupMethods = require('../../commonValidation/lookupMethods');
  EMPLOYER_EVENT.observe('before save', function employerEventBeforeSave(ctx, next) {
    if (ctx.isNewInstance && ctx.instance.empEventId == undefined) {
      var employerEventTypeCode = 'EMPLOYER_EVENT_TYPE_CODE';
      var employerEventTypObj = {};
      employerEventTypObj['lookupValueId'] = ctx.instance.eventTypeValueId;
      lookupMethods.typeCodeFunction(employerEventTypObj, employerEventTypeCode, function(employerEventTypeCodeStatus) {
        if (employerEventTypeCodeStatus) {
          ctx.instance.updateUserId = ctx.instance.createUserId;
          ctx.instance.createDatetime = new Date();
          ctx.instance.updateDatetime = new Date();
          logger.info('EmployerEvent Creation Initiated');
          next();
        } else {
          errorFunction('employerEventTypeCode validation error', next);
        }
      });
    } else if (ctx.isNewInstance) {
      errorFunction('empEventId is system generated value', next);
    } else {
      next();
    }
  });
  // remote method definition to get employer event details
  EMPLOYER_EVENT.getEmployerEventDetails = function(empEventId, callBc) {
    var inputObj = {};
    var employerEventModel = server.models.EmployerEvent;
    var entityDetailsUsingIncludeFilter = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (empEventId) {
      inputObj['empEventId'] = empEventId;
      var includeModels = ['employerEventLookupValue'];
      // below function will give details for an entity based on loopback include filter
      entityDetailsUsingIncludeFilter(inputObj, employerEventModel, includeModels, callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method definition to get employer event details
  EMPLOYER_EVENT.remoteMethod('getEmployerEventDetails', {
    description: 'To get employer event details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'empEventId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    }, ],
    http: {
      path: '/getEmployerEventDetails',
      verb: 'GET',
    },
  });
  //  emp Event remoteMethod
  EMPLOYER_EVENT.updateEmployerEvent = function(eventData, cb) {
    var updateEvent = require('../../commonCompanyFiles/update-emp-event-service.js');
    updateEvent.updateEmpEventService(eventData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };

  //update emp Event remoteMethod
  EMPLOYER_EVENT.remoteMethod('updateEmployerEvent', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateEmployerEvent',
      verb: 'PUT',
    },
  });

  var employerEventCreation = function(employerEventDetails) {
    var employerEvent = server.models.EmployerEvent;
    return new Promise(function(resolve, reject) {
      employerEvent.create(employerEventDetails, function(err, employerEventResp) {
        console.log('errrrrrrrrrrrrrrrrr ', err, employerEventResp);
        if (err) {
          reject(err);
        } else if (employerEventResp) {
          resolve(employerEventResp);
        }
      });
    });
  };
  var employerEventPanelCreation = function(employerEventData, employerEventPanelData) {
    return new Promise(function(resolve, reject) {
      var async = require('async');

      var empEventId = employerEventData[0].empEventId;
      async.map(employerEventPanelData, createIndividualEventPanel.bind({
        empEventId: empEventId
      }), function(error, employerEventPanelCreateResp) {
        if (error) {
          reject(error);
        } else if (employerEventPanelCreateResp) {
          var response = {};
          response.employerEvent = employerEventData;
          response.employerEventPanel = employerEventPanelCreateResp;
          resolve(response);
        }
      });
    });
  };

  function createIndividualEventPanel(employerEventPanelDetails, callback) {
    console.log('empEventId: ' + this.empEventId);
    var employerEventPanel = server.models.EmployerEventPanel;
    employerEventPanelDetails.empEventId = this.empEventId;
    employerEventPanel.create(employerEventPanelDetails, function(err, employerEventPanelResp) {
      callback(null, employerEventPanelResp);
    });
  }
  EMPLOYER_EVENT.createCompleteEmployerEvent = function(inputData, cb) {
    if (inputData.employerEvent.length == 1) {
      validateModel(inputData.employerEvent, getEmployerEventJson,
        function(err, employerEventValidationResp) {
          if (employerEventValidationResp) {
            employerEventCreation(inputData.employerEvent).then(function(employerEventResolveResp) {
              cb(null, employerEventResolveResp);
            });
          } else {
            cb(err, null);
          }
        });
    } else if (inputData.employerEvent.length == 1 && inputData.employerEventPanel.length > 0) {
      validateModel(inputData.employerEvent, getEmployerEventJson, function(err, employerEventValidationResp) {
        validateModel(inputData.employerEventPanel, getEmployerEventPanelJson, function(error, employerEventPanelValidationResp) {
          // console.log('Validation Error in compensation package: ' + JSON.stringify(err) + ' ' + JSON.stringify(error));
          // console.log('validataion response in compensation package: ' + JSON.stringify(employerEventValidationResp));
          // console.log('validataion response in compensation package item: ' + JSON.stringify(employerEventPanelValidationResp));
          console.log(employerEventValidationResp);
          if (employerEventValidationResp && employerEventPanelValidationResp) {
            employerEventCreation(inputData.employerEvent).then(function(employerEventResolveResp) {
              employerEventPanelCreation(employerEventResolveResp,
                inputData.employerEventPanel).then(function(employerEventPanelResolveRep) {
                  cb(null, employerEventPanelResolveRep);
                }, function(employerEventPanelRejectRep) {
                cb(employerEventPanelRejectRep, null);
              });
            }, function(employerEventRejectResp) {
              cb(employerEventRejectResp, null);
            });
          } else {
            errorResponse(cb);
          }
        });
      });
    } else if (inputData.employerEvent.length == 1) {
      validateModel(inputData.employerEvent, getEmployerEventJson,
        function(err, employerEventValidationResp) {
          if (employerEventValidationResp) {
            employerEventCreation(inputData.employerEvent).then(function(employerEventResolveResp) {
              cb(null, employerEventResolveResp);
            });
          } else {
            cb(err, null);
          }
        });
    }
  };
  //Create CompensationPackage
  EMPLOYER_EVENT.remoteMethod('createCompleteEmployerEvent', {
    description: 'createCompleteEmployerEvent',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/createCompleteEmployerEvent',
      verb: 'POST',
    },
  });

  /**
   * createEmployerEventAction-used for creation
   * @constructor
   * @param {object} inputData -details entered by the user
   * @param {function} cb-callbcak
   */
  EMPLOYER_EVENT.createEmployerEventAction = function(inputData, cb) {
    if (inputData.employerEventId) {
      var inputFilterObject = {};
      inputFilterObject['empEventId'] = inputData.employerEventId;
      var employerEventModel = server.models.EmployerEvent;
      employerEventModel.findOne({
        'where': {
          'and': [inputFilterObject],
        },
      }, function(error, modelResponse) {
        if (error) {
          error.requestStatus = false;
          cb(error, null);
        } else if (modelResponse) {
          if (modelResponse.eventTypeValueId == 309 || modelResponse.eventTypeValueId == 311) {
            //employer event actions for on-campus students
            employerEventOnCampusActions(modelResponse, inputData, function(err, onResp) {
              if (err) {
                cb(err, null);
              } else {
                cb(null, onResp);
              }
            });
          } else if (modelResponse.eventTypeValueId == 310) {
            //employer event actions for off-campus students
            employerEventOffCampusActions(modelResponse, inputData, function(err, offresp) {
              if (err) {
                cb(err, null);
              } else {
                cb(null, offresp);
              }
            });
          } else {
            var errorResp = {};
            errorResp.message = 'Event Not Found';
            errorResp.statusCode = 401;
            cb(errorResp, null);
          }
        } else {
          errorResponse(cb);
        }
      });
    } else {
      var errorResp = {};
      errorResp.message = 'Bad request';
      errorResp.statusCode = 400;
      cb(errorResp, null);
    };
  };

  //method to do employer events based on actions
  EMPLOYER_EVENT.remoteMethod('createEmployerEventAction', {
    description: 'createEmployerEventAction',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/createEmployerEventAction',
      verb: 'POST',
    },
  });

  //method to do employer events based on actions

  EMPLOYER_EVENT.getCompanyByEvent = function(input, empCallBC) {
    var employerEvent = server.models.EmployerEvent;
    employerEvent.findOne({
      'where': {
        'empEventId': input.empEventId,
      },
    }, function(error, response) {
      if (error) {
        empCallBC(error, null);
      } else {
        var companySearchVw = server.models.CompanySearchVw;
        var companySearchVwModel = companySearchVw.searchCompany;
        companySearchVwModel(response.companyId, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, function(err, compResp) {
          if (err) {
            empCallBC(err, null);
          } else {
            var obj = {};
            obj.eventDetails = response;
            obj.companyDetails = compResp;
            empCallBC(null, obj);
          }
        });
      }
    });
  };
  EMPLOYER_EVENT.remoteMethod('getCompanyByEvent', {
    description: 'getCompanyByEvent',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/getCompanyByEvent',
      verb: 'POST',
    },
  });
};
