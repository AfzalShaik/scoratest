'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
module.exports = function(PROGRAM) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  // when request comes to program model then we execute logic or change request object before saving into database
  PROGRAM.observe('before save', function campusBeforeSave(ctx, next) {
    if (ctx.isNewInstance) {
      ctx.instance.updateUserId = ctx.instance.createUserId;
      ctx.instance.createDatetime = new Date();
      ctx.instance.updateDatetime = new Date();
      next();
    } else {
      next();
    }
  });
  // remote method definition to get program details based on program id
    /**
 *getProgramDetails- To get program contact details by taking required fields
 *@constructor
 * @param {object} programId - Unique id for ecah and every program
 * @param {number} campusId - unique id for each and every campus
 * @param {number} departmentId - unique id for every department
 * @param {function} callBc - deals with response
 */
  PROGRAM.getProgramDetails = function(programId, campusId, departmentId, callBc) {
    var inputObj = {};
    var includeModels = {};
    includeModels = ['programTypeValueLookupValue', 'programClassValueLookupValue', 'programCatValueLookupValue', 'programMajorValueLookupValue', 'departmentDetails'];
    var program = server.models.Program;
    var entityDetailsUsingIncludeFilter = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (campusId && departmentId && programId) {
      inputObj['campusId'] = campusId;
      inputObj['departmentId'] = departmentId;
      inputObj['programId'] = programId;
      // below function will give details for an entity based on loopback include filter
      entityDetailsUsingIncludeFilter(inputObj, program, includeModels, callBc);
    } else if (campusId && departmentId) {
      inputObj['campusId'] = campusId;
      inputObj['departmentId'] = departmentId;
      // below function will give details for an entity based on loopback include filter
      entityDetailsUsingIncludeFilter(inputObj, program, includeModels, callBc);
    } else if (campusId) {
      inputObj['campusId'] = campusId;
      // below function will give details for an entity based on loopback include filter
      entityDetailsUsingIncludeFilter(inputObj, program, includeModels, callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to get program details based on program id
  PROGRAM.remoteMethod('getProgramDetails', {
    description: 'To get program details based on program id',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'programId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'campusId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'departmentId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getProgramDetails',
      verb: 'GET',
    },
  });
  //  PROGRAM remoteMethod
   /**
 *updateProgram- To update program Details
 *@constructor
 * @param {object} programData - contains all the data need to get updated
 * @param {function} cb - deals with response
 */
  PROGRAM.updateProgram = function(programData, cb) {
    var programUpdate = require('../../commonCampusFiles/update-program.js');
    logger.info('updating program service');
    programUpdate.programUpdate(programData, function(err, resp) {
      if (err) {
        logger.error('error while updating program');
        cb(err, resp);
      } else {
        logger.info('program successfully updated');
        cb(null, resp);
      }
    });
  };

  //update PROGRAM remoteMethod
  PROGRAM.remoteMethod('updateProgram', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateProgram',
      verb: 'PUT',
    },
  });
  // remote method definition to get campus details
    /**
 *searchCampusPrograms- for Searching the programs by taking required fields
 *@constructor
 * @param {object} programId - Unique id for ecah and every program
 * @param {number} campusId - unique id for each and every campus
 * @param {number} programTypeValueId - unique id for every program Type
 * @param {number} programClassValueId - unique id for every Class
 * @param {number} programCatValueId - unique id for every Cat
 * @param {number} programMajorValueId - unique id for every Major
 * @param {function} callBc - deals with response
 */
  PROGRAM.searchCampusPrograms = function(programId, campusId, programTypeValueId, programClassValueId, programCatValueId, programMajorValueId, callBc) {
    var inputObj = {};
    var program = server.models.Program;
    var entityDetailsUsingIncludeFilter = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (programClassValueId || programTypeValueId || campusId || programId || programCatValueId || programMajorValueId) {
      inputObj['programClassValueId'] = (programClassValueId) ? programClassValueId : undefined;
      inputObj['programTypeValueId'] = (programTypeValueId) ? programTypeValueId : undefined;
      inputObj['campusId'] = (campusId) ? campusId : undefined;
      inputObj['programId'] = (programId) ? programId : undefined;
      inputObj['programCatValueId'] = (programCatValueId) ? programCatValueId : undefined;
      inputObj['programMajorValueId'] = (programMajorValueId) ? programMajorValueId : undefined;
      var includeRelations = ['programClassValueLookupValue', 'campusDetails', 'programTypeValueLookupValue', 'programCatValueLookupValue', 'programMajorValueLookupValue'];
      // below function will give details for an entity based on loopback include filter
      entityDetailsUsingIncludeFilter(inputObj, program, includeRelations, callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to get program details based on program id
  PROGRAM.remoteMethod('searchCampusPrograms', {
    description: 'To get campus details based on program id',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'programId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'campusId',
      type: 'number',
        // required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'programTypeValueId',
      type: 'number',
        // required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'programClassValueId',
      type: 'number',
        // required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'programCatValueId',
      type: 'number',
        // required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'programMajorValueId',
      type: 'number',
        // required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/searchCampusPrograms',
      verb: 'GET',
    },
  });
};
