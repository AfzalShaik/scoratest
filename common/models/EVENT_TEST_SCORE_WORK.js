'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
process.setMaxListeners(0);
module.exports = function(EVENT_TEST_SCORE_WORK) {
  //mass upload remote method starts here
  /**
   *eventTestScoreUpload- To upload event test score details
   *@constructor
   * @param {object} test - contains all the data need to get uploaded
   * @param {function} cb - deals with response
   */
  var async = require('async');
  EVENT_TEST_SCORE_WORK.eventTestScoreUpload = function(testInput, cb) {
    var name = testInput.fileDetails.name;
    var container = testInput.fileDetails.container;
    var pathForm = require('path');
    var inputFile = './attachments/' + container + '/' + name;
    var fullPathForm = pathForm.join(__dirname, '../../../../', inputFile);
    // var inputFile = './commonValidation/event_test_score.csv';
    var readCsvFile = require('../../commonValidation/common-mass-upload.js').readFile;
    var cleanArray = require('../../commonValidation/common-mass-upload.js').cleanArray;
    var validateModel = require('../../ServicesImpl/CommonImpl/models-validation.js').validateInput;
    var eventTestScoreWorkJson = require('./EVENT_TEST_SCORE_WORK.json');
    var throwError = require('../../ErrorHandling/customizeErrorTypes').errorFunction;
    var logger = require('../../server/boot/lib/logger');
    var output = [];
    EVENT_TEST_SCORE_WORK.destroyAll({}, function(destroyError, destroyOutput) {
      readCsvFile(fullPathForm, function(err, fileResponse) {
        if (err) {
          cb(err, null);
        } else {
          if (fileResponse.length > 0) {
            var firstObj = fileResponse[0];
            if ((firstObj.studentEmail || firstObj.studentEmail == '') && (firstObj.testName || firstObj.testName == '') && (firstObj.testVendorName || firstObj.testVendorName == '') && (firstObj.description || firstObj.description == '') && (firstObj.score || firstObj.score == '') && (firstObj.maxScore || firstObj.maxScore == '')) {
              var createMass = require('../../commonCompanyFiles/event-test-score-upload.js').createMassUpload;
              for (var i = 0; i < fileResponse.length; i++) {
                var obj = {};
                obj = fileResponse[i];
                obj.rowNumber = i + 1;
                obj.employerEventId = testInput.empEventId;
                obj.companyId = testInput.companyId;
                output.push(obj);
              }
              createMass(output, testInput, function(createErr, createResponse) {
                if (createErr) {
                  throwError(createErr, cb);
                } else {
                  output = [];
                  cb(null, createResponse);
                }
              });
            } else {
              throwError('Invalid Csv File Uploaded ', cb);
            }
          } else {
            throwError('Empty File Cannot be Uploaded. ', cb);
          }
        }
      });
    });
  };
  //event test upload method creation
  EVENT_TEST_SCORE_WORK.remoteMethod('eventTestScoreUpload', {
    description: 'Send Valid Data ',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/eventTestScoreUpload',
      verb: 'POST',
    },
  });
  /**
   *getInfo- To filter in the test score
   *@constructor
   * @param {object} obj - contains all the data need to get filtered
   * @param {function} callback - deals with response
   */
  function getInfo(obj, callback) {
    var student = server.models.Student;
    if (obj.studentId && obj.score && obj.score < obj.maxScore && obj.maxScore > 0) {
      student.find({
        'where': {
          'studentId': obj.studentId,
        },
      }, function(error, findResp) {
        if (error) {
          obj['error'] = 'true';
          callback(null, obj);
        } else if (findResp.length > 0) {
          obj['error'] = 'false';
          callback(null, obj);
        } else {
          obj['error'] = 'true';
          callback(null, obj);
        }
      });
    } else {
      obj['error'] = 'true';
      callback(null, obj);
    }
  }

  function getFailureTests(failedArray, cb) {
    var finalArray = [];
    for (var i = 0; i < failedArray.length; i++) {
      var obj = {
        'companyId': failedArray[i].companyId,
        'employerEventId': failedArray[i].employerEventId,
        'studentId': failedArray[i].studentId,
        'testName': failedArray[i].testName,
        'description': failedArray[i].description,
        'score': failedArray[i].score,
        'error': 'studentId is not valid or score and maxScore are not valid',
        'testVendorName': failedArray[i].testVendorName,
        'maxScore': failedArray[i].maxScore,
        'rowNumber': failedArray[i].rowNumber,
        'studentEmail': failedArray[i].studentEmail,
      };
      finalArray.push(obj);
    }
    var fs = require('fs');
    var csv = require('fast-csv');
    var ws = fs.createWriteStream('./commonValidation/out.csv');
    // csv
    //   .write(finalArray, {
    //     headers: true,
    //   })
    //   .pipe(ws);
    csv
      .writeToPath('./commonValidation/out.csv', finalArray, {
        headers: true
      })
      .on('finish', function() {
        // console.log('done!');
      });
    cb(null, 'success');
  }
};
