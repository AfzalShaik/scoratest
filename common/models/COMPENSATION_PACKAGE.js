'use strict';
var validateModel = require('../../ServicesImpl/CommonImpl/models-validation.js').validateInput;
var compensationPackageJson = require('./COMPENSATION_PACKAGE.json').properties;
var compensationPackageItemJson = require('./COMPENSATION_PACKAGE_ITEM.json').properties;
delete compensationPackageItemJson['compPackageId'];

var logger = require('../../server/boot/lib/logger');
var server = require('../../server/server');
var async = require('async');
// var CompensationPackageModel = require('../../server/boot/scora.js').CompensationPackage;
module.exports = function(COMPENSATION_PACKAGE) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  var lookupMethods = require('../../commonValidation/lookupMethods');
  COMPENSATION_PACKAGE.observe('before save', function compensationPkgBeforeSave(ctx, next) {
    if (ctx.isNewInstance && ctx.instance.compPackageId == undefined) {
      /*var compApprovalCode = 'COMP_APPROVAL_STATUS_CODE';
      var compApprovalObj = {};
      compApprovalObj['lookupValueId'] = ctx.instance.compApprovalStatusValueId;
      lookupMethods.typeCodeFunction(compApprovalObj, compApprovalCode, function(compensationApprovalStatus) {
        if (compensationApprovalStatus) {
          ctx.instance.updateUserId = ctx.instance.createUserId;
          ctx.instance.createDatetime = new Date();
          ctx.instance.updateDatetime = new Date();
          logger.info('COMPENSATION_PACKAGE Creation Initiated');
          next();
        } else {
          errorFunction('compensationApprovalStatus validation error', next);
        }
      });*/
      ctx.instance.updateUserId = ctx.instance.createUserId;
      ctx.instance.createDatetime = new Date();
      ctx.instance.updateDatetime = new Date();
      logger.info('COMPENSATION_PACKAGE Creation Initiated');
      next();
    } else if (ctx.isNewInstance) {
      errorFunction('compPackageId is system generated value', next);
    } else {
      next();
    }
  });
  // remote method definition to get compensation package details
  /**
   *getCompensationPkgDetails- To get Compensation pakage details
   *@constructor
   * @param {number} compPackageId - unique id for every pakage
   * @param {number} companyId - unique id for company
   * @param {function} callBc - deals with response
   */
  COMPENSATION_PACKAGE.getCompensationPkgDetails = function(compPackageId, companyId, callBc) {
    var inputObj = {};
    var CompensationPackage = server.models.CompensationPackage;
    var compensationPackageDet = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (compPackageId && companyId) {
      inputObj['compPackageId'] = compPackageId;
      inputObj['companyId'] = companyId;
      var includeModels = ['compensationPackageCompany', 'compensationPackageApprovalStatus'];
      // below function will give details for an entity based on loopback include filter
      compensationPackageDet(inputObj, CompensationPackage, includeModels, callBc);
    } else if (companyId) {
      inputObj['companyId'] = companyId;
      var includeModels = ['compensationPackageCompany', 'compensationPackageApprovalStatus'];
      // below function will give details for an entity based on loopback include filter
      compensationPackageDet(inputObj, CompensationPackage, includeModels, callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to get compensation package details
  COMPENSATION_PACKAGE.remoteMethod('getCompensationPkgDetails', {
    description: 'To get compensation package details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'compPackageId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'companyId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getCompensationPkgDetails',
      verb: 'GET',
    },
  });
  //UpdateCompensationPackage remote method starts here
  /**
   *updateCompensationPackage- To update Compensation pakage
   *@constructor
   * @param {object} pakageData - contains all the data need to get updated
   * @param {function} cb - deals with response
   */
  COMPENSATION_PACKAGE.updateCompensationPackage = function(packageData, cb) {
    var packageUpdate = require('../../commonCompanyFiles/update-compensation-pkg-service');
    packageUpdate.updatePackageService(packageData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };

  //UpdateCompensationPackage method creation
  COMPENSATION_PACKAGE.remoteMethod('updateCompensationPackage', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateCompensationPackage',
      verb: 'PUT',
    },
  });
  /**
   *compensationPackageCreation- To create a pakage
   *@constructor
   * @param {object} compPkg - the pakage need to get created
   */
  var compensationPackageCreation = function(compPkg) {
    var CompensationPackage = server.models.CompensationPackage;
    return new Promise(function(resolve, reject) {
      CompensationPackage.create(compPkg, function(err, compPkgResp) {
        if (err) {
          reject(err);
        } else if (compPkgResp) {
          resolve(compPkgResp);
        }
      });
    });
  };
  /**
   *compensationPackageItemCreation- To create an iteam ina an pakage
   *@constructor
   * @param {number} compPkg - contains pakage details
   * @param {number} compPkgItems - contains iteams needs to get created
   */
  var compensationPackageItemCreation = function(compPkg, compPkgItems) {
    return new Promise(function(resolve, reject) {
      var async = require('async');

      var compPackageId = compPkg[0].compPackageId;
      // console.log("comPkg.compPackageId: " + compPackageId);
      async.map(compPkgItems, createIndividualCompPkgItem.bind({
        compPackageId: compPackageId,
      }), function(error, compensationPackageItemCreateResp) {
        if (error) {
          reject(error);
        } else if (compensationPackageItemCreateResp) {
          var response = {};
          response.compensationPkg = compPkg;
          response.compensationPkgItem = compensationPackageItemCreateResp;
          resolve(response);
        }
      });
    });
  };
  /**
   *compPkgItem- To create iteam individually
   *@constructor
   * @param {number} compPkgItem - contains iteam need to get created
   * @param {function} callback - deals with response
   */
  function createIndividualCompPkgItem(compPkgItem, callback) {
    // console.log("compPackageId: " + this.compPackageId);
    var CompensationPackageItem = server.models.CompensationPackageItem;
    compPkgItem.compPackageId = this.compPackageId;
    CompensationPackageItem.create(compPkgItem, function(err, compPkgItemResp) {
      callback(null, compPkgItemResp);
    });
  }
  /**
   *createCompleteCompensationPackage- To create complete compensation pakage
   *@constructor
   * @param {object} inputData - contains all the data need to get created
   * @param {function} cb - deals with response
   */
  COMPENSATION_PACKAGE.createCompleteCompensationPackage = function(inputData, cb) {
    if (inputData.compensationPkg.length == 1) {
      //console.log(inputData);
      validateModel(inputData.compensationPkg, compensationPackageJson, function(err, compPkgValidationResp) {
        validateModel(inputData.compensationPkgItem, compensationPackageItemJson, function(error, compkgItemValidationResp) {
          // console.log("Validation Error in compensation package: " + JSON.stringify(err) + " " + JSON.stringify(error));
          // console.log('======================= response in compensation package: ' + JSON.stringify(compPkgValidationResp) + ' ' + JSON.stringify(compkgItemValidationResp));
          // console.log('000000000000000000000 response in compensation package item: ' + JSON.stringify(compkgItemValidationResp));

          if (compPkgValidationResp || compkgItemValidationResp) {
            compensationPackageCreation(inputData.compensationPkg).then(function(compPkgResolveResp) {
              compensationPackageItemCreation(compPkgResolveResp, inputData.compensationPkgItem).then(function(comPkgItemResolveRep) {
                cb(null, comPkgItemResolveRep);
              }, function(comPkgItemRejectRep) {
                cb(comPkgItemRejectRep, null);
              });
            }, function(compPkgRejectResp) {
              cb(compPkgRejectResp, null);
            });
          } else {
            errorResponse(cb);
          }
        });
      });
    }
  };
  //Create CompensationPackage
  COMPENSATION_PACKAGE.remoteMethod('createCompleteCompensationPackage', {
    description: 'send compensation package and compensation item data ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/createCompleteCompensationPackage',
      verb: 'POST',
    },
  });
  // remote method declaration to get List of compensation package details
  var listArray = [];
  var packageIdArray = [];
  COMPENSATION_PACKAGE.getCompensationDetails = function(empEventId, companyId, callBc) {
    var employerDriveCampuses = server.models.EmployerDriveCampuses;
    var employerEvent = server.models.EmployerEvent;
    employerEvent.findOne({'where': {'empEventId': empEventId}}, function(driveErr, driveResp) {
      console.log('........................... ', driveErr, driveResp);
      if (driveErr) {
        callBc(driveErr, null);
      } else {
        employerDriveCampuses.find({
          'where': {
            'and': [{
              'empEventId': empEventId,
            }, {
              'companyId': companyId,
            }, {
              'empDriveId': driveResp.empDriveId,
            }],
          },
        }, function(empDriveErr, empDriveResp) {
          // console.log('-------------------------- ', empDriveErr, empDriveResp);
          if (empDriveErr) {
            callBc(empDriveErr, null);
          } else {
            for (var i = 0; i < empDriveResp.length; i++) {
              listArray.push(empDriveResp[i].listId);
            }
            var uniqueEmpResp = listArray.filter(function(elem, index, self) {
              return index == self.indexOf(elem);
            });
            async.map(uniqueEmpResp, getCompensationId, function(compErr, compResp) {
              // console.log(compResp);
              var response = {};
              response.data = (compResp.length) ? compResp[0] : null;
              callBc(null, response);
            });
          }
        });
      }
    });
  };

  function getCompensationId(obj, cb) {
    var employerCampusListCompPkg = server.models.EmployerCampusListCompPkg;
    employerCampusListCompPkg.find({
      'where': {
        'listId': obj,
      },
    }, function(idErr, idResp) {
      if (idErr) {
        cb(idErr, null);
      } else {
        for (var i = 0; i < idResp.length; i++) {
          packageIdArray.push(idResp[i].compPackageId);
        }
        var uniqueEmpResp = packageIdArray.filter(function(elem, index, self) {
          return index == self.indexOf(elem);
        });
        async.map(uniqueEmpResp, getCompesation, function(compErr, compResp) {
          if (compErr) {
            cb(compErr, null);
          } else {
            cb(null, compResp);
            // console.log(';;;;;;;;;;; ', uniqueEmpResp);
          }
        });
      }
    });
  }

  function getCompesation(object, callBack) {
    var compensationPackage = server.models.CompensationPackage;
    compensationPackage.findOne({
      'where': {
        'compPackageId': object,
      },
    }, function(comErr, compResp) {
      if (comErr) {
        callBack(comErr, null);
      } else {
        callBack(null, compResp);
      }
    });
  }
  COMPENSATION_PACKAGE.remoteMethod('getCompensationDetails', {
    description: 'To get compensation package details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'empEventId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'companyId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getCompensationDetails',
      verb: 'GET',
    },
  });
  COMPENSATION_PACKAGE.updateComp = function(input, callBc) {
    console.log(input);
    COMPENSATION_PACKAGE.updateAll({'compPackageId': input.compensationPkg[0].compPackageId},
    input.compensationPkg[0], function(err, response) {
      if (err) {
        callBc(err, null);
      } else {
        var async = require('async');
        async.map(input.compensationPkgItem, createUpdateDeleteCompItesm, function(err, response) {
          if (err) {
            callBc(err, null);
          } else {
            async.map(input.delete, deleteTheData, function(err, deleteResposne) {
              if (err) {
                callBc(err, null);
              } else {
                callBc(null, response);
              }
            });
          }
        });
      }
    });
    function deleteTheData(obj, callBack) {
      var compIteam = server.models.CompensationPackageItem;
      compIteam.destroyById(Number(obj),
        function(err, deleteResponse) {
          if (err) {
            callBack(err, null);
          } else {
            callBack(null, deleteResponse);
          }
        });
    }
    function createUpdateDeleteCompItesm(object, cb) {
      var compIteam = server.models.CompensationPackageItem;
      if (object.indicator == 'C') {
        delete object.indicator;
        compIteam.create(object, function(err, creationResponse) {
          if (err) {
            cb(err, null);
          } else {
            cb(null, creationResponse);
          }
        });
      } else if (object.indicator == 'U') {
        delete object.indicator;
        if (object.compPackageItemId) {
          compIteam.updateAll({'compPackageItemId': object.compPackageItemId,
        'compPackageId': object.compPackageId},
          object, function(err, creationResponse) {
            if (err) {
              cb(err, null);
            } else {
              cb(null, creationResponse);
            }
          });
        } else {
          cb(null, object);
        }
      } else if (object.indicator == 'I') {
        delete object.indicator;
        cb(null, object);
      } else {
        cb(null, null);
      }
    }
  };
  // remote method declaration to get compensation package details
  COMPENSATION_PACKAGE.remoteMethod('updateComp', {
    description: 'To get compensation package details',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateComp',
      verb: 'PUT',
    },
  });
};
