'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
module.exports = function(EMPLOYER_PERSON_CONTACT) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  // remote method definition to create contact for employer person contact
  EMPLOYER_PERSON_CONTACT.createEmployerContact = function(data, callBc) {
    if (data.employerPersonId && data.companyId) {
      if (data.primaryInd == 'Y') {
        searchForInd(data, function(err, res) {
          if (err) {
            errorResponse('there was an error', callBc);
          } else {
            // console.log('sucess');
          }
        });
      }
      var checkEntityExistence = require('../../ServicesImpl/CommonImpl/validateEntityRecord.js').checkEntitySingleRecordExistence;
      var inputFilterObject = {};
      inputFilterObject['companyId'] = data.companyId;
      inputFilterObject['employerPersonId'] = data.employerPersonId;
      var contactModel = server.models.Contact;
      var employerPersonModel = server.models.EmployerPerson;
      checkEntityExistence(employerPersonModel, inputFilterObject, function(checkStatus) {
        if (checkStatus) {
          contactModel.create(data, function(error, contactRes) {
            if (error) {
              logger.error('error while creating employer person contact model');
              callBc(error, null);
            } else if (contactRes) {
              EMPLOYER_PERSON_CONTACT.create({
                'employerPersonId': Number(data.employerPersonId),
                'companyId': Number(data.companyId),
                'contactId': Number(contactRes.contactId),
                'primaryInd': String(data.primaryInd),
              }, function(err, resp) {
                if (err) {
                  errorResponse(callBc);
                } else {
                  logger.info('employer person contact service created successfully');
                  // contactRes.requestStatus = true;
                  callBc(null, contactRes);
                }
              });
            } else {
              logger.error('error while creating employer person');
              errorResponse(callBc);
            }
          });
        } else {
          logger.error('entity is not found');
          errorResponse(callBc);
        }
      });
    } else {
      logger.error('companyId and employerPersonId are required');
      errorResponse(callBc);
    }
  };
  function searchForInd(data, priCallback) {
    EMPLOYER_PERSON_CONTACT.find({'where': {'employerPersonId': data.employerPersonId,
    'companyId': data.companyId, 'primaryInd': 'Y'}}, function(err, res) {
      if (err) {
        errorResponse('campusId or Education person id was invalid', callBc);
      } else if (res == 0) {
        data.primaryInd = data.primaryInd;
      } else {
        var update = {};
        update.primaryInd = 'N';
        EMPLOYER_PERSON_CONTACT.updateAll({'employerPersonId': data.employerPersonId,
        'companyId': data.companyId, 'primaryInd': 'Y'}, update, function(err, resp) {
          if (err) {
            errorResponse('there was an error');
          } else {
            // console.log('this is response of update', resp);
            priCallback(null, resp);
          }
        });
      }
    });
  }
  // remote method declaration to create contact for employer person contact
  EMPLOYER_PERSON_CONTACT.remoteMethod('createEmployerContact', {
    description: 'Useful to create contact for employer person contact',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      required: true,
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/createEmployerContact',
      verb: 'post',
    },
  });
  // remote method definition to get employer person contact details
    /**
 *getEmployerPersonContact- To getEmployerPersonContact
 *@constructor
 * @param {object} employerPersonId - Unique id for ecah and every employer person
 * @param {number} companyId - unique id for each and every  compny
 * @param {number} contactId - unique id for every contact
 * @param {function} callBc - deals with response
 */
  EMPLOYER_PERSON_CONTACT.getEmployerPersonContact = function(employerPersonId, companyId, contactId, callBc) {
    var inputObj = {};
    var employerPersonContactModel = server.models.EmployerPersonContact;
    var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
    var employerPersonContact = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (employerPersonId && companyId && contactId) {
      inputObj['employerPersonId'] = employerPersonId;
      inputObj['companyId'] = companyId;
      inputObj['contactId'] = contactId;
      // below function will give contact details for an entity based on loopback include filter
      employerPersonContact(inputObj, employerPersonContactModel, 'employerPersonContact', callBc);
    } else if (employerPersonId && companyId) {
      inputObj['employerPersonId'] = employerPersonId;
      inputObj['companyId'] = companyId;
      // below function will give contact details for an entity based on loopback include filter
      employerPersonContact(inputObj, employerPersonContactModel, 'employerPersonContact', callBc);
      logger.info('fetching employerPersonContact details');
    } else {
      logger.error('error while fetching employer person contact details');
      errorResponse(callBc);
    }
  };
  // remote method declaration to get employer person contact details
  EMPLOYER_PERSON_CONTACT.remoteMethod('getEmployerPersonContact', {
    description: 'To get employer person contact details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'employerPersonId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'companyId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'contactId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getEmployerPersonContact',
      verb: 'get',
    },
  });
  //  EMPLOYER_PERSON remoteMethod
   /**
 *updateEmployeePersonContact- To update employee person contact details
 *@constructor
 * @param {object} personData - contains all the data need to get updated
 * @param {function} cb - deals with response
 */
  EMPLOYER_PERSON_CONTACT.updateEmployeePersonContact = function(personData, cb) {
    var personUpdate = require('../../commonCompanyFiles/update-employee-person-contact.js');
    if (personData.primaryInd == 'Y') {
      searchForInd(personData, function(err, res) {
        if (err) {
          errorResponse('there was an error', callBc);
        } else {
          // console.log('sucess');
        }
      });
    }
    logger.info('updating employeePersonContact details');
    personUpdate.employeePersonContactUpdate(personData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        var updatetData = {};
        updatetData.primaryInd = personData.primaryInd;
        EMPLOYER_PERSON_CONTACT.updateAll({'companyId': personData.companyId,
        'contactId': personData.contactId, 'employerPersonId': personData.employerPersonId},
        updatetData, function(err, res) {
          if (err) {
            errorResponse(cb);
          } else {
            cb(null, resp);
          }
        });
      }
    });
  };

  //update EMPLOYER_PERSON remoteMethod
  EMPLOYER_PERSON_CONTACT.remoteMethod('updateEmployeePersonContact', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateEmployeePersonContact',
      verb: 'PUT',
    },
  });

  // remote method definition to deleteEmployerPersonContact
    /**
 *deleteEmployerPersonContact- To delete employee person contact by taking required fields
 *@constructor
 * @param {object} companyId - Unique id for ecah and every company
 * @param {number} contactId - unique id for each and every contact
 * @param {number} addressId - unique id for every address
 * @param {function} callBc - deals with response
 */
  EMPLOYER_PERSON_CONTACT.deleteEmployerPersonContact = function(companyId, contactId, employerPersonId, callBc) {
    if (companyId && contactId && employerPersonId) {
      var employerPersonContact = server.models.EmployerPersonContact;
      var contactModel = server.models.Contact;
      var deleteEmployerPersonContactDet = require('../../ServicesImpl/CommonImpl/deleteEntityRecord.js').deleteEntityRecordExceptPrimary;
      var primaryCheckInput = {};
      primaryCheckInput['contactId'] = contactId;
      var inputFilterObject = {};
      inputFilterObject['companyId'] = companyId;
      inputFilterObject['contactId'] = contactId;
      inputFilterObject['employerPersonId'] = employerPersonId;
      logger.info('deleting employer person contact details');
      deleteEmployerPersonContactDet(primaryCheckInput, inputFilterObject, contactModel, employerPersonContact, callBc);
    } else {
      logger.error('error while deleting employer person contact details');
      errorResponse(callBc);
    }
  };

  // remote method declaration to deleteEmployerPersonContact
  EMPLOYER_PERSON_CONTACT.remoteMethod('deleteEmployerPersonContact', {
    description: 'To delete employer person contact',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'companyId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'contactId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'employerPersonId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/deleteEmployerPersonContact',
      verb: 'delete',
    },
  });
};
