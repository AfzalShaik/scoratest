/**
 * Created by admin on 17-07-2017.
 */
// var server1 = require('../../server/server');
// This file will handle request before entering into execution of business logic available at common/models folder
// Onnce transaction done then we change response before sending to client
/**
 * Created by admin on 17-07-2017.
 */
// var server1 = require('../../server/server');
// This file will handle request before entering into execution of business logic available at common/models folder
// Onnce transaction done then we change response before sending to client

// TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Remove once Vijay's authorization comes into place.
'use strict';
module.exports = function(server) {
  var remotes = server.remotes();
  //Before remote method for authentication
  remotes.before('**', function(ctx, next) {
    var url = ctx.req.originalUrl.toString();
    var method = ctx.req.method.toString();
    var throwError = require('../../ErrorHandling/customizeErrorTypes').errorFunction;
    var createUserId = ctx.req.body.createUserId;
    var updateUserId = (ctx.req.body.updateUserId) ? ctx.req.updateUserId : createUserId;
    var scoraServices = server.models.ScoraServices;
    var scoraRolePrivileges = server.models.ScoraRolePrivileges;
    var searchShort = (url) ? url.indexOf('access_token') >= 0 : false;
    var headers = ctx.req.headers;
    var security = require('../config.json').security;
    console.log('=============================================== ', ctx.req.query, ctx.req.Query);
    var getInput = [];
    var arrayInput = [];
    if (url.indexOf('/api/ScoraUsers') > -1) {
      loginService(method, next);
    } else if (security == false) {
      // console.log('===============================', headers);
      // setUserInfo(headers, function(err, resp) {
      next();
      // });
    } else {
      if (headers) {
        var accessToken = headers['x-scora-auth'];
        if (accessToken) {
          var accessTokenModel = server.models.AccessToken;
          accessTokenModel.findOne({
            'were': {
              'id': accessToken,
            },
          }, function(err, token) {
            // console.log('searchShortttttttttttttttttttttttttttt', token);
            if (token) {
              var serviceName = url;
              if (method == 'GET') {
                var serviceName = serviceName.split('?');
                // console.log('serviceNameeeeeeeeee ', serviceName);
                serviceName = serviceName[0];
                // console.log('serviceNameeeeeeeeee ', serviceName);
              }
              scoraServices.findOne({
                'where': {
                  'and': [{
                    'serviceName': serviceName,
                  },
                  {
                    'methodName': method,
                  },
                  ],
                },
              }, function(err, services) {
                if (services) {
                  var serviceId = services.serviceId;
                  //.........................
                  var scoraServicePrivileges = server.models.ScoraServicePrivileges;
                  scoraServicePrivileges.findOne({
                    'where': {
                      'serviceId': serviceId,
                    },
                  }, function(privilegesErr, privileges) {
                    if (privileges) {
                      var accessLevel = privileges.accessLevel;
                      // var accessTokenModel = server.models.AccessToken;
                      // accessTokenModel.findOne({
                      //   'were': {
                      //     'id': accessToken,
                      //   },
                      // }, function(err, token) {
                      //   // console.log('searchShortttttttttttttttttttttttttttt', token);
                      //   if (token) {
                      var scoraUserRole = server.models.ScoraUserRole;
                      var userId = token.userId;
                      scoraUserRole.findOne({
                        'where': {
                          'id': userId,
                        },
                      }, function(error, role) {
                        if (role) {
                          var roleCode = role.roleCode;
                          var scoraRoleAccess = server.models.ScoraRoleAccess;
                          scoraRoleAccess.findOne({
                            'where': {
                              'and': [{
                                'roleCode': roleCode,
                              }, {
                                'accessLevel': accessLevel,
                              }],
                            },
                          }, function(roleAccessErr, roleAccess) {
                            // console.log(',,,,,,,,,,,,,,,,,,,,,,,,,,,,,', headers);
                            if (roleAccess) {
                              checkAttributeLevel(roleCode, headers, userId, ctx, function(checkErr, checkResp) {
                                if (checkResp) {
                                  var scoraRoleAccess = server.models.ScoraRoleAccess;
                                  var userDetails = scoraRoleAccess.getUserDetails;
                                  var object = {};
                                  object.userId = userId;
                                  object.role = roleCode;
                                  object.campusId = headers.campusid;
                                  object.companyId = headers.companyid;
                                  object.educationPersonId = headers.educationpersonid;
                                  object.employerPersonId = headers.employerpersonid;
                                  userDetails(object, function(err, resp) {
                                    next();
                                  });
                                } else {
                                  unauthorizedRes('You are not Autherized to Perform the action', next);
                                }
                              });
                            } else {
                              // You are not autherized to access this api
                              unauthorizedRes('You Dont Have a Access to Perform This Action', next);
                            }
                          });
                        } else {
                          unauthorizedRes('You Are Not Autherized to Perform This Action', next);
                        }
                      });
                    } else {
                      // Authentication Failed
                      unauthorizedRes('Autherization Failed', next);
                    }
                    //
                  });
                } else {
                  unauthorizedRes('API Meta Data Is Not Defined', next);
                }
              });
            } else {
              // API Meta data not defined
              // unauthorizedRes('API Meta Data Is Not Defined', next);
              unauthorizedRes('AUTHORIZATION_REQUIRED', next);
            }
          });
        } else {
          unauthorizedRes('AUTHORIZATION_REQUIRED', next);
        }
      } else {
        unauthorizedRes('AUTHORIZATION_REQUIRED', next);
      }
    }
  });
  // Modify response before sending response to requested user/client
  remotes.after('**', function(ctx, next) {
    if (ctx.req.method.toString() == 'POST') {
      ctx.result = {
        data: ctx.result,
        requestStatus: true,
      };
      next();
    } else {
      next();
    }
  });
  // error message if any unauthorized request comes to server
  var unauthorizedRes = function(error, next) {
    var adminAuthorizationError = new Error(error);
    adminAuthorizationError.statusCode = 401;
    // adminAuthorizationError.requestStatus = false;
    next(adminAuthorizationError, null);
  };
  var loginService = function(method, next) {
    console.log('hhhhhhhh');
    if (method == 'POST' || method == 'GET') {
      next();
    } else {
      unauthorizedRes('You Are Not Autherized', next);
    }
  };

  function checkAttributeLevel(roleCode, headers, userId, ctx, checkCB) {
    if (roleCode == 'PLCDIR' && roleCode === headers.role && userId == headers.userid && headers.campusid) {
      var educationPerson = server.models.EducationPerson;
      educationPerson.findOne({
        'where': {
          // 'and': [
          // {
          'id': userId,
          // }, {
          // 'campusId': headers.campusid,
          // }],
        },
      }, function(campusErr, campusOut) {
        if (campusOut) {
          // console.log('ctxxxxxxxxxxxxxxxxxxx ', campusOut);
          if (campusOut.campusId == headers.campusid) {
            var method = ctx.req.method.toString();
            var url = ctx.req.originalUrl.toString();
            var objInput = {};
            objInput = ctx.req.body;
            var campusObject = {};
            // campusObject['roleCode'] = roleCode;
            // campusObject['userId'] = userId;
            campusObject['campusId'] = campusOut.campusId;
            campusObject['educationPersonId'] = campusOut.educationPersonId;
            campusObject['method'] = method;
            campusObject['objInput'] = objInput;
            campusObject['url'] = url;
            if (method == 'GET') {
              campusObject['objInput'] = ctx.req.query;
            }
            // console.log('objInputttttttttt ', campusObject);
            campusAttributeLevel(campusObject, function(campusError, campusOutput) {
              if (campusOutput) {
                checkCB(null, campusOut);
              } else {
                checkCB('You are not autherized to access the data', null);
              }
            });
          } else {
            checkCB('You are not autherized to access the data', null);
          }
        } else {
          checkCB('You are not autherized to access the data', null);
        }
      });
    } else if (roleCode == 'RECDIR' && roleCode === headers.role && userId == headers.userid && headers.companyid) {
      var employerPerson = server.models.EmployerPerson;
      employerPerson.findOne({
        'where': {
          'and': [{
            'id': userId,
          }, {
            'companyId': headers.companyid,
          }],
        },
      }, function(companyErr, companyOut) {
        if (companyOut) {
          if (companyOut.companyId == headers.companyid) {
            var method = ctx.req.method.toString();
            var url = ctx.req.originalUrl.toString();
            var objInput = {};
            objInput = ctx.req.body;
            var companyObject = {};
            companyObject['companyId'] = companyOut.companyId;
            companyObject['employerPersonId'] = companyOut.employerPersonId;
            // companyObject['roleCode'] = roleCode;
            // companyObject['userId'] = userId;
            companyObject['method'] = method;
            companyObject['objInput'] = objInput;
            companyObject['url'] = url;
            if (method == 'GET') {
              companyObject['objInput'] = ctx.req.query;
            }
            // var eventStudentList = server.models.EventStudentList;
            // eventStudentList.find({'where': {'companyId': headers.companyid}}, function(eventStudentErr, eventStudentOut) {
            // companyObject['eventStudentArray'] = eventStudentOut;
            companyAttributeLevel(companyObject, function(companyError, companyOutput) {
              if (companyOutput) {
                checkCB(null, companyOut);
              } else {
                checkCB('You are not autherized to access the data', null);
              }
            });
            // });
          } else {
            checkCB('You are not autherized to access the data', null);
          }
        } else {
          checkCB('Record Not Found', null);
        }
      });
    } else if (roleCode == 'STUDENT' && roleCode === headers.role && userId == headers.userid) {
      var student = server.models.Student;
      student.findOne({
        'where': {
          'and': [{
            'id': userId,
          }, {
            'studentId': headers.studentid,
          }],
        },
      }, function(studentErr, studentOut) {
        if (studentOut) {
          if (studentOut.studentId == headers.studentid) {
            var method = ctx.req.method.toString();
            var url = ctx.req.originalUrl.toString();
            var objInput = {};
            objInput = ctx.req.body;
            var studentObject = {};
            studentObject['studentId'] = studentOut.companyId;
            studentObject['method'] = method;
            studentObject['objInput'] = objInput;
            studentObject['url'] = url;
            if (method == 'GET') {
              studentObject['objInput'] = ctx.req.query;
            }
            studentAttributeLevel(studentObject, function(studentError, studentOutput) {
              if (studentOutput) {
                checkCB(null, studentOut);
              } else {
                checkCB('You are not autherized to access the data', null);
              }
            });
          } else {
            checkCB('You are not autherized to access the data', null);
          }
          // checkCB(null, studentOut);
        } else {
          checkCB('Record Not Found', null);
        }
      });
    } else {
      checkCB('AUTHORIZATION_REQUIRED', null);
    }
  }
  function campusAttributeLevel(campusObject, campusCB) {
    // console.log('0000000000000000000000000000 ', ctx.req.method.toString());
    if (campusObject.method == 'POST' || campusObject.method == 'PUT' || campusObject.method == 'DELETE' || campusObject.method == 'GET') {
      var objInput = {};
      objInput = campusObject.objInput;
      console.log(objInput, '2222222222222222222');
      if (objInput.constructor === Array) {

      } else {
        if (objInput.campusId) {
          if (objInput.campusId == campusObject.campusId) {
            campusCB(null, campusObject);
          } else {
            campusCB(null, nulll);
          }
        } else if (objInput.campusEventId) {
          var campusEvent = server.models.CampusEvent;
          campusEvent.findOne({'where': {'campusEventId': objInput.campusEventId}}, function(campusEventErr, campusEventOut) {
            if (campusEventOut) {
              if (campusEventOut.campusId == campusObject.campusId) {
                campusCB(null, campusObject);
              } else {
                campusCB(null, null);
              }
            } else {
              campusCB(null, null);
            }
          });
        } else if (objInput.enrollmentId) {
          var enrollment = server.models.Enrollment;
          enrollment.find({'where': {'enrollmentId': objInput.enrollmentId}}, function(enrollmentErr, enrollmentOut) {
            if (enrollmentOut) {
              if (enrollmentOut.campusId == campusObject.campusId) {
                campusCB(null, campusObject);
              } else {
                campusCB(null, null);
              }
            } else {
              campusCB(null, null);
            }
          });
        } else if (objInput.programId) {
          var program = server.models.Program;
          program.find({'where': {'programId': objInput.programId}}, function(programErr, programOut) {
            if (programOut) {
              if (programOut.campusId == campusObject.campusId) {
                campusCB(null, campusObject);
              } else {
                campusCB(null, null);
              }
            } else {
              campusCB(null, null);
            }
          });
        } else if (objInput.departmentId) {
          var department = server.models.Department;
          department.find({'where': {'departmentId': objInput.departmentId}}, function(departmentErr, departmentOut) {
            if (departmentOut) {
              if (departmentOut.campusId == campusObject.campusId) {
                campusCB(null, campusObject);
              } else {
                campusCB(null, null);
              }
            } else {
              campusCB(null, null);
            }
          });
        } else if (objInput.campusDriveId) {
          var campusDrive = server.models.CampusDrive;
          campusDrive.find({'where': {'campusDriveId': objInput.campusDriveId}}, function(campusDriveErr, campusDriveOut) {
            if (campusDriveOut) {
              if (campusDriveOut.campusId == campusObject.campusId) {
                campusCB(null, campusObject);
              } else {
                campusCB(null, null);
              }
            } else {
              campusCB(null, null);
            }
          });
        } else {
          campusCB(null, null);
        }
      }
    } else if (campusObject.method == 'GET') {
      var serviceName = campusObject.url;
      // for campusId
      var campusId = 'campusId' + '=' + campusObject.campusId;
      var educationPersonId = 'educationPersonId' + '=' + campusObject.educationPersonId;
      var campusService = serviceName;

      var campusVariable = campusService.search('&&');
      var campusVar = campusService.search('campusId=');
      var eduVar = campusService.search('educationPersonId=');
      var campusEventVar = campusService.search('campusEventId=');
      var enrollmentVar = campusService.search('enrollmentId=');
      var departmentVar = campusService.search('departmentId=');
      var programVar = campusService.search('programId=');
      var campusDriveVar = campusService.search('campusDriveId=');
      if (campusVar > 0) {
        // var campusVariable = campusService.search('&&');
        var getCampusId;
        if (campusVariable > 0) {
          getCampusId = campusService.substr(campusVar, campusVariable);
        } else {
          getCampusId = campusService.substr(campusVar);
        }
        if (getCampusId === campusId) {
          campusCB(null, campusObject);
        } else {
          campusCB(null, null);
        }
      } else if (eduVar > 0) {
        // var eduVariable = campusService.search('&&');
        var getEducationPersonId;
        if (campusVariable > 0) {
          getEducationPersonId = campusService.substr(eduVar, campusVariable);
        } else {
          getEducationPersonId = campusService.substr(eduVar);
        }
        if (getEducationPersonId === educationPersonId) {
          campusCB(null, campusObject);
        } else {
          campusCB(null, null);
        }
      } else if (campusEventVar > 0) {
        // var campusEventVariable = campusService.search('&&');
        var getcampusEventId;
        if (campusVariable > 0) {
          var getcampusEventId = campusService.substr(campusEventVar, campusVariable);
        } else {
          var getcampusEventId = campusService.substr(campusEventVar);
        }
        var campusEvent = server.models.CampusEvent;
        getcampusEventId = getcampusEventId.split('campusEventId=');
        campusEvent.findOne({'where': {'campusEventId': getcampusEventId[1]}}, function(campusEventErr, campusEventOut) {
          if (campusEventOut) {
            if (campusEventOut.campusId == campusObject.campusId) {
              campusCB(null, campusObject);
            } else {
              campusCB(null, null);
            }
          } else {
            campusCB(null, null);
          }
        });
      } else if (enrollmentVar > 0) {
        var getEnrollmentId;
        if (campusVariable > 0) {
          var getEnrollmentId = campusService.substr(enrollmentVar, campusVariable);
        } else {
          var getEnrollmentId = campusService.substr(enrollmentVar);
        }
        var enrollment = server.models.Enrollment;
        getEnrollmentId = getEnrollmentId.split('enrollmentId=');
        enrollment.findOne({'where': {'enrollmentId': getEnrollmentId[1]}}, function(enrollmentErr, enrollmentOut) {
          if (enrollmentOut) {
            if (enrollmentOut.campusId == campusObject.campusId) {
              campusCB(null, campusObject);
            } else {
              campusCB(null, null);
            }
          } else {
            campusCB(null, null);
          }
        });
      } else if (departmentVar > 0) {
        var getDepartmentId;
        if (campusVariable > 0) {
          var getDepartmentId = campusService.substr(departmentVar, campusVariable);
        } else {
          var getDepartmentId = campusService.substr(departmentVar);
        }
        var department = server.models.Department;
        getDepartmentId = getDepartmentId.split('departmentId=');
        department.findOne({'where': {'departmentId': getDepartmentId[1]}}, function(departmentErr, departmentOut) {
          if (departmentOut) {
            if (departmentOut.campusId == campusObject.campusId) {
              campusCB(null, campusObject);
            } else {
              campusCB(null, null);
            }
          } else {
            campusCB(null, null);
          }
        });
      } else if (programVar > 0) {
        var getProgramId;
        if (campusVariable > 0) {
          var getProgramId = campusService.substr(programVar, campusVariable);
        } else {
          var getProgramId = campusService.substr(programVar);
        }
        var program = server.models.Program;
        getProgramId = getProgramId.split('programId=');
        program.findOne({'where': {'programId': getProgramId[1]}}, function(programErr, programOut) {
          if (programOut) {
            if (programOut.campusId == campusObject.campusId) {
              campusCB(null, campusObject);
            } else {
              campusCB(null, null);
            }
          } else {
            campusCB(null, null);
          }
        });
      } else if (campusDriveVar > 0) {
        var getcampusDrive;
        if (campusVariable > 0) {
          var getcampusDrive = campusService.substr(campusDriveVar, campusVariable);
        } else {
          var getcampusDrive = campusService.substr(campusDriveVar);
        }
        var campusDrive = server.models.CampusDrive;
        getcampusDrive = getcampusDrive.split('campusDriveId=');
        campusDrive.findOne({'where': {'campusDriveId': getcampusDrive[1]}}, function(campusDriveErr, campusDriveOut) {
          if (campusDriveOut) {
            if (campusDriveOut.campusId == campusObject.campusId) {
              campusCB(null, campusObject);
            } else {
              campusCB(null, null);
            }
          } else {
            campusCB(null, null);
          }
        });
      } else {
        campusCB(null, null);
      }
    } else {
      campusCB(null, null);
    }
  }
  function companyAttributeLevel(companyObject, companyCB) {
    if (companyObject.method == 'POST' || companyObject.method == 'PUT' || companyObject.method == 'DELETE') {
      var objInput = {};
      objInput = companyObject.objInput;
      if (objInput.constructor === Array) {

      } else {
        if (objInput.companyId) {
          if (objInput.companyId == companyObject.companyId) {
            companyCB(null, companyObject);
          } else {
            companyCB(null, nulll);
          }
        } else if (objInput.eventStudentListId) {
          var eventStudentList = server.models.EventStudentList;
          eventStudentList.findOne({'where': {'studentListId': objInput.eventStudentListId}}, function(eventErr, eventOut) {
            if (eventOut) {
              if (eventOut.companyId == companyObject.companyId) {
                companyCB(null, companyObject);
              } else {
                companyCB(null, null);
              }
            } else {
              companyCB(null, null);
            }
          });
        } else if (objInput.eventTestScoreId) {
          var eventTestScore = server.models.EventTestScore;
          eventTestScore.find({'where': {'eventTestScoreId': objInput.eventTestScoreId}}, function(scoreErr, scoreOut) {
            if (scoreOut) {
              if (scoreOut.companyId == companyObject.companyId) {
                companyCB(null, companyObject);
              } else {
                companyCB(null, null);
              }
            } else {
              companyCB(null, null);
            }
          });
        } else if (objInput.listCompPkgId) {
          var employerCampusListCompPkg = server.models.EmployerCampusListCompPkg;
          employerCampusListCompPkg.find({'where': {'listCompPkgId': objInput.listCompPkgId}}, function(listErr, listOut) {
            if (listOut) {
              if (listOut.companyId == companyObject.companyId) {
                companyCB(null, companyObject);
              } else {
                companyCB(null, null);
              }
            } else {
              companyCB(null, null);
            }
          });
        } else if (objInput.listId) {
          var employerCampusListHdr = server.models.EmployerCampusListHdr;
          employerCampusListHdr.find({'where': {'listId': objInput.listId}}, function(listErr, listOut) {
            if (listOut) {
              if (listOut.companyId == companyObject.companyId) {
                companyCB(null, companyObject);
              } else {
                companyCB(null, null);
              }
            } else {
              companyCB(null, null);
            }
          });
        } else if (objInput.empDriveId) {
          var employerDrive = server.models.EmployerDrive;
          employerDrive.find({'where': {'empDriveId': objInput.empDriveId}}, function(driveErr, driveOut) {
            if (driveOut) {
              if (driveOut.companyId == companyObject.companyId) {
                companyCB(null, companyObject);
              } else {
                companyCB(null, null);
              }
            } else {
              companyCB(null, null);
            }
          });
        } else if (objInput.listCampusId) {
          var employerCampusListDtl = server.models.EmployerCampusListDtl;
          employerCampusListDtl.find({'where': {'listCampusId': objInput.listCampusId}}, function(listErr, listOut) {
            if (listOut) {
              if (listOut.companyId == companyObject.companyId) {
                companyCB(null, companyObject);
              } else {
                companyCB(null, null);
              }
            } else {
              companyCB(null, null);
            }
          });
        } else if (objInput.compPackageItemId) {
          var compensationPackageItem = server.models.CompensationPackageItem;
          compensationPackageItem.find({'where': {'compPackageItemId': objInput.compPackageItemId}}, function(itemErr, itemOut) {
            if (itemOut) {
              if (itemOut.companyId == companyObject.companyId) {
                companyCB(null, companyObject);
              } else {
                companyCB(null, null);
              }
            } else {
              companyCB(null, null);
            }
          });
        } else if (objInput.compPackageId) {
          var compensationPackage = server.models.CompensationPackage;
          compensationPackage.find({'where': {'compPackageId': objInput.compPackageId}}, function(compErr, compOut) {
            if (compOut) {
              if (compOut.companyId == companyObject.companyId) {
                companyCB(null, companyObject);
              } else {
                companyCB(null, null);
              }
            } else {
              companyCB(null, null);
            }
          });
        } else if (objInput.jobRoleId) {
          var jobRole = server.models.JobRole;
          jobRole.find({'where': {'jobRoleId': objInput.jobRoleId}}, function(jobRoleErr, jobRoleOut) {
            if (jobRoleOut) {
              if (jobRoleOut.companyId == companyObject.companyId) {
                companyCB(null, companyObject);
              } else {
                companyCB(null, null);
              }
            } else {
              companyCB(null, null);
            }
          });
        } else if (objInput.organizationId) {
          var organization = server.models.Organization;
          organization.find({'where': {'organizationId': objInput.organizationId}}, function(orgErr, orgOut) {
            if (orgOut) {
              if (orgOut.companyId == companyObject.companyId) {
                companyCB(null, companyObject);
              } else {
                companyCB(null, null);
              }
            } else {
              companyCB(null, null);
            }
          });
        } else if (objInput.empEventId || objInput.employerEventId) {
          var empEventId = (objInput.empEventId) ? objInput.empEventId : objInput.employerEventId;
          var employerEvent = server.models.EmployerEvent;
          employerEvent.find({'where': {'empEventId': empEventId}}, function(eventErr, eventOut) {
            if (eventOut) {
              if (eventOut.companyId == companyObject.companyId) {
                companyCB(null, companyObject);
              } else {
                companyCB(null, null);
              }
            } else {
              companyCB(null, null);
            }
          });
        } else {
          companyCB(null, null);
        }
      }
    } else if (companyObject.method == 'GET') {
      var serviceName = companyObject.url;
      // for companyId
      var companyId = 'companyId' + '=' + companyObject.companyId;
      var employerPersonId = 'employerPersonId' + '=' + companyObject.employerPersonId;
      var companyService = serviceName;

      var companyVariable = companyService.search('&&');
      var companyVar = companyService.search('companyId=');
      var eventStudentListIdVar = companyService.search('eventStudentListId=');
      var empVar = companyService.search('employerPersonId=');
      var empEventVar = (companyService.search('empEventId=')) ? companyService.search('empEventId=') : companyService.search('employerPersonId=');
      var testVar = companyService.search('eventTestScoreId=');
      var driveVar = companyService.search('empDriveId=');
      var listCompPkgVar = companyService.search('listCompPkgId=');
      var listVar = companyService.search('listId=');
      var listCampusVar = companyService.search('listCampusId=');
      var compPackageItemVar = companyService.search('compPackageItemId=');
      var compPackageVar = companyService.search('compPackageId=');
      var jobRoleVar = companyService.search('jobRoleId=');
      var orgVar = companyService.search('jobRoleId=');
      if (companyVar > 0) {
        // var companyVariable = campusService.search('&&');
        var getcompanyId;
        if (companyVariable > 0) {
          getcompanyId = companyService.substr(companyVar, companyVariable);
        } else {
          getcompanyId = companyService.substr(companyVar);
        }
        if (getcompanyId === companyId) {
          companyCB(null, companyObject);
        } else {
          companyCB(null, null);
        }
      } else if (empVar > 0) {
        // var eduVariable = campusService.search('&&');
        var getEducationPersonId;
        if (companyVariable > 0) {
          getEmployerPersonId = companyService.substr(empVar, companyVariable);
        } else {
          getEmployerPersonId = companyService.substr(empVar);
        }
        if (getEmployerPersonId === employerPersonId) {
          companyCB(null, companyObject);
        } else {
          companyCB(null, null);
        }
      } else if (eventStudentListIdVar > 0) {
        // var campusEventVariable = campusService.search('&&');
        var geteventStudentListId;
        if (companyVariable > 0) {
          var geteventStudentListId = companyService.substr(eventStudentListIdVar, companyVariable);
        } else {
          var geteventStudentListId = companyService.substr(eventStudentListIdVar);
        }
        var eventStudentList = server.models.EventStudentList;
        geteventStudentListId = geteventStudentListId.split('studentListId=');
        eventStudentList.findOne({'where': {'studentListId': geteventStudentListId[1]}}, function(eventErr, eventOut) {
          if (eventOut) {
            if (eventOut.companyId == companyObject.companyId) {
              companyCB(null, companyObject);
            } else {
              companyCB(null, null);
            }
          } else {
            companyCB(null, null);
          }
        });
      } else if (empEventVar > 0) {
        var getEmpEventId;
        if (companyVariable > 0) {
          var getEmpEventId = companyService.substr(empEventVar, companyVariable);
        } else {
          var getEmpEventId = companyService.substr(empEventVar);
        }
        var employerEvent = server.models.EmployerEvent;
        getEmpEventId = (getEmpEventId.search('empEventId=')) ? getEmpEventId.search('empEventId=') : getEmpEventId.search('employerPersonId=');
        employerEvent.findOne({'where': {'empEventId': getEmpEventId[1]}}, function(empErr, empOut) {
          if (empOut) {
            if (empOut.companyId == companyObject.companyId) {
              companyCB(null, companyObject);
            } else {
              companyCB(null, null);
            }
          } else {
            companyCB(null, null);
          }
        });
      } else if (testVar > 0) {
        var getEventTestScoreId;
        if (companyVariable > 0) {
          var getEventTestScoreId = companyService.substr(testVar, companyVariable);
        } else {
          var getEventTestScoreId = companyService.substr(testVar);
        }
        var eventTestScore = server.models.EventTestScore;
        getEventTestScoreId = getEventTestScoreId.split('eventTestScoreId=');
        eventTestScore.findOne({'where': {'eventTestScoreId': getEventTestScoreId[1]}}, function(testErr, testOut) {
          if (testOut) {
            if (testOut.companyId == companyObject.companyId) {
              companyCB(null, companyObject);
            } else {
              companyCB(null, null);
            }
          } else {
            companyCB(null, null);
          }
        });
      } else if (driveVar > 0) {
        var getDriveVar;
        if (companyVariable > 0) {
          var getDriveVar = companyService.substr(driveVar, companyVariable);
        } else {
          var getDriveVar = companyService.substr(driveVar);
        }
        var employerDrive = server.models.EmployerDrive;
        getDriveVar = getDriveVar.split('empDriveId=');
        employerDrive.findOne({'where': {'empDriveId': getDriveVar[1]}}, function(driveErr, driveOut) {
          if (driveOut) {
            if (driveOut.companyId == companyObject.companyId) {
              companyCB(null, companyObject);
            } else {
              companyCB(null, null);
            }
          } else {
            companyCB(null, null);
          }
        });
      } else if (listCompPkgVar > 0) {
        var getlistCompPkgId;
        if (companyVariable > 0) {
          var getlistCompPkgId = companyService.substr(listCompPkgVar, companyVariable);
        } else {
          var getlistCompPkgId = companyService.substr(listCompPkgVar);
        }
        var EmployerCampusListCompPkg = server.models.EmployerCampusListCompPkg;
        getlistCompPkgId = getlistCompPkgId.split('listCompPkgId=');
        employerCampusListCompPkg.findOne({'where': {'listCompPkgId': getlistCompPkgId[1]}}, function(pkgErr, pkgOut) {
          if (pkgOut) {
            if (pkgOut.companyId == companyObject.companyId) {
              companyCB(null, companyObject);
            } else {
              companyCB(null, null);
            }
          } else {
            companyCB(null, null);
          }
        });
      } else if (listVar > 0) {
        var getlistId;
        if (companyVariable > 0) {
          var getlistId = companyService.substr(listVar, companyVariable);
        } else {
          var getlistId = companyService.substr(listVar);
        }
        var employerCampusListHdr = server.models.EmployerCampusListHdr;
        getlistId = getlistId.split('listId=');
        employerCampusListHdr.findOne({'where': {'listId': getlistId[1]}}, function(listErr, listOut) {
          if (listOut) {
            if (listOut.companyId == companyObject.companyId) {
              companyCB(null, companyObject);
            } else {
              companyCB(null, null);
            }
          } else {
            companyCB(null, null);
          }
        });
      } else if (listCampusVar > 0) {
        var getlistCampusId;
        if (companyVariable > 0) {
          var getlistCampusId = companyService.substr(listCampusVar, companyVariable);
        } else {
          var getlistCampusId = companyService.substr(listCampusVar);
        }
        var employerCampusListDtl = server.models.EmployerCampusListDtl;
        getlistCampusId = getlistCampusId.split('listCampusId=');
        employerCampusListDtl.findOne({'where': {'listCampusId': getlistCampusId[1]}}, function(listErr, listOut) {
          if (listOut) {
            if (listOut.companyId == companyObject.companyId) {
              companyCB(null, companyObject);
            } else {
              companyCB(null, null);
            }
          } else {
            companyCB(null, null);
          }
        });
      } else if (compPackageItemVar > 0) {
        var getcompPackageItemId;
        if (companyVariable > 0) {
          var getcompPackageItemId = companyService.substr(compPackageItemVar, companyVariable);
        } else {
          var getcompPackageItemId = companyService.substr(compPackageItemVar);
        }
        var compensationPackageItem = server.models.CompensationPackageItem;
        getcompPackageItemId = getcompPackageItemId.split('compPackageItemId=');
        compensationPackageItem.findOne({'where': {'compPackageItemId': getcompPackageItemId[1]}}, function(listErr, listOut) {
          if (listOut) {
            if (listOut.companyId == companyObject.companyId) {
              companyCB(null, companyObject);
            } else {
              companyCB(null, null);
            }
          } else {
            companyCB(null, null);
          }
        });
      } else if (compPackageVar > 0) {
        var getcompPackageId;
        if (companyVariable > 0) {
          var getcompPackageId = companyService.substr(compPackageVar, companyVariable);
        } else {
          var getcompPackageId = companyService.substr(compPackageVar);
        }
        var compensationPackage = server.models.CompensationPackage;
        getcompPackageId = getcompPackageId.split('compPackageId=');
        compensationPackage.findOne({'where': {'compPackageId': getcompPackageId[1]}}, function(pkgErr, pkgOut) {
          if (pkgOut) {
            if (pkgOut.companyId == companyObject.companyId) {
              companyCB(null, companyObject);
            } else {
              companyCB(null, null);
            }
          } else {
            companyCB(null, null);
          }
        });
      } else if (jobRoleVar > 0) {
        var getJobRoleId;
        if (companyVariable > 0) {
          var getJobRoleId = companyService.substr(jobRoleVar, companyVariable);
        } else {
          var getJobRoleId = companyService.substr(jobRoleVar);
        }
        var jobRole = server.models.JobRole;
        getJobRoleId = getJobRoleId.split('jobRoleId=');
        jobRole.findOne({'where': {'jobRoleId': getJobRoleId[1]}}, function(jobRoleErr, jobRoleOut) {
          if (jobRoleOut) {
            if (jobRoleOut.companyId == companyObject.companyId) {
              companyCB(null, companyObject);
            } else {
              companyCB(null, null);
            }
          } else {
            companyCB(null, null);
          }
        });
      } else if (orgVar > 0) {
        var getOrgId;
        if (companyVariable > 0) {
          var getOrgId = companyService.substr(orgVar, companyVariable);
        } else {
          var getOrgId = companyService.substr(orgVar);
        }
        var organization = server.models.Organization;
        getOrgId = getOrgId.split('organizationId=');
        organization.findOne({'where': {'organizationId': getOrgId[1]}}, function(orgErr, orgOut) {
          if (orgOut) {
            if (orgOut.companyId == companyObject.companyId) {
              companyCB(null, companyObject);
            } else {
              companyCB(null, null);
            }
          } else {
            companyCB(null, null);
          }
        });
      } else {
        companyCB(null, null);
      }
    } else {
      companyCB(null, null);
    }
  }
  function studentAttributeLevel(studentObject, studentCB) {
    if (studentObject.method == 'POST' || studentObject.method == 'PUT' || studentObject.method == 'DELETE') {
      var objInput = {};
      objInput = companyObject.objInput;
      if (objInput.constructor === Array) {
        studentCB(null, studentObject);
      } else {
        if (objInput.studentId) {
          if (objInput.studentId == studentObject.studentId) {
            studentCB(null, studentObject);
          } else {
            studentCB(null, nulll);
          }
        } else if (objInput.enrollmentId) {
          var enrollment = server.models.Enrollment;
          enrollment.findOne({'where': {'enrollmentId': objInput.enrollmentId}}, function(enrollmentErr, enrollmentOut) {
            if (enrollmentOut) {
              if (enrollmentOut.studentId == studentObject.studentId) {
                studentCB(null, studentObject);
              } else {
                studentCB(null, null);
              }
            } else {
              studentCB(null, null);
            }
          });
        } else {
          studentCB(null, null);
        }
      }
    } else if (companyObject.method == 'GET') {
      var serviceName = studentObject.url;
      var studentId = 'studentId' + '=' + studentObject.studentId;
      var studentService = serviceName;
      var studentVar = studentService.search('studentId=');
      var enrollVar = studentService.search('enrollmentId=');
      var studentVariable = studentService.search('&&');
      if (studentVar > 0) {
        var getStudentId;
        if (studentVariable > 0) {
          getStudentId = studentService.substr(studentVar, studentVariable);
        } else {
          getStudentId = studentService.substr(studentVar);
        }
        if (getStudentId === studentId) {
          studentCB(null, studentObject);
        } else {
          studentCB(null, null);
        }
      } else if (enrollVar > 0) {
        var getEnrollmentId;
        if (studentVariable > 0) {
          getEnrollmentId = studentService.substr(enrollVar, studentVariable);
        } else {
          getEnrollmentId = studentService.substr(enrollVar);
        }
        var enrollment = server.models.Enrollment;
        enrollment.findOne({'where': {'enrollmentId': getEnrollmentId[1]}}, function(enrollErr, enrollOut) {
          if (enrollOut) {
            if (enrollOut.enrollmentId == studentObject.studentId) {
              studentCB(null, studentObject);
            } else {
              studentCB(null, null);
            }
          } else {
            studentCB(null, null);
          }
        });
      } else {
        studentCB(null, null);
      }
    } else {
      studentCB(null, null);
    }
  }
};
