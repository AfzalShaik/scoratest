  'use strict';
  var server = require('../server/server');
  var logger = require('../server/boot/lib/logger');
  var validation = require('../commonValidation/addressValidation');
  var updateAddress = require('../commonValidation/updateAddress');
  var DepartmentAddress = server.models.DepartmentAddress;
  var updateAddService = require('../commonValidation/update-service-address');
  var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
  /**
   *DepartmentUpdate-function deals with updating the department data
   *@constructor
   * @param {object} departrmentData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
  function DepartmentUpdate(departmentData, cb) {
    if (departmentData) {
      if (departmentData.campusId && departmentData.departmentId && departmentData.addressId) {
        DepartmentAddress.findOne({
          'where': {
            'and': [{
              'campusId': departmentData.campusId,
            }, {
              'addressId': departmentData.addressId,
            }, {
              'departmentId': departmentData.departmentId,
            }],
          },
        }, function(err, DepartmentList) {
          if (err) {
            //throws error
            cb(err, departmentData);
          } else if (DepartmentList) {
            logger.info('for provided campusId,addressId and departmentId record found');
            updateAddService.updateAddresService(departmentData, function(err, departmentAddressList) {
              if (err) {
                logger.error('error while updating the departmentAddress');
                cb(err, departmentData);
              } else {
                logger.info('department address updated successfully');
                cb(null, departmentAddressList);
              }
            });
          } else {
            //throws error incase of invalid address_id or campusId
            throwError('Invalid address_id or campusId or departmentId', cb);
            logger.error('Invalid address_id or campusId or departmentId');
          }
        });
      } else {
        throwError('campusId and departmentId and addressId is required ', cb);
      }
    } else {
      logger.error("Input can't be Null");
      throwError('Input is Null', cb);
    }
  }

  exports.DepartmentUpdate = DepartmentUpdate;
