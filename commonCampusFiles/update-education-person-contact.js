  'use strict';
  var server = require('../server/server');
  var logger = require('../server/boot/lib/logger');
  var contactValidation = require('../commonValidation/contactValidation');
  var updateContact = require('../commonValidation/updateContact');
  var allmethods = require('../commonValidation/allmethods');
  var updateContactService = require('../commonValidation/update-service-contact');
  var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
  /**
   *updateEducationPersonContact-function deals with updating the education person contact
   *@constructor
   * @param {object} contactData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
  function updateEducationPersonContact(contactData, cb) {
    if (contactData) {
      //validating the input
      //validating the input json
      if (contactData.educationPersonId && contactData.contactId && contactData.campusId) {
        var EducationPersonContact = server.models.EducationPersonContact;
        EducationPersonContact.findOne({
          'where': {
            'and': [{
              'campusId': contactData.campusId,
            }, {
              'educationPersonId': contactData.educationPersonId,
            }, {
              'contactId': contactData.contactId,
            }],
          },
        }, function(err, EducationPersonList) {
          if (err) {
            cb(err, contactData);
          } else if (EducationPersonList) {
            updateContactService.updateContactService(contactData, function(error, info) {
              if (error) {
                logger.error('error while updating education person contact');
                cb(error, contactData);
              } else {
                logger.info('education person contact updated successfully');
                cb(null, info);
              }
            });
          } else {
            //throws error when invalid contatcId or educationPersonId or campusId
            logger.error('invalid contactid or campusId or educationPersonId');
            throwError('Invalid contactId or campusId or educationPersonId ', cb);
          }
        });
      } else {
        logger.error('educationPersonId and contactId and campusId are required');
        throwError('educationPersonId and contactId and campusId are required: ', cb);
      }
    } else {
      logger.error('Input cant be null');
      throwError("Input can't be null ", cb);
    }
  }
  exports.updateEducationPersonContact = updateEducationPersonContact;
