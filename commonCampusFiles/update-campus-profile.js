  'use strict';
  var campusValidation = require('../commonValidation/campusValidation');
  var server = require('../server/server');
  var logger = require('../server/boot/lib/logger');
  var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
  var Campus = server.models.Campus;
  /**
   *updateCampusProfile-function deals with updating the campus profile
   *@constructor
   * @param {object} campusData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
  function updateCampusProfile(campusData, cb) {
    if (campusData) {
      // if (campusData.rating || campusData.rank || campusData.tier) {
      //update operation cannot be performed if user tries to update above fieds

      //   logger.error('You cannot perform update operation ');
      //   throwError('You cannot perform update operation', cb);
      // } else {
      //validating input json

      campusValidation.validateCampusJson(campusData, function(err, value) {
        if (err) {
          //throws invalid json error
          logger.error('Invalid Json');
          throwError(err, cb);
        } else if (value == true) {
          if (campusData.campusId != undefined && campusData.campusId != null && campusData.campusId != '') {
            //find record from campus table using campusId
            if (campusData.tireValueId) {
              var lookupValueModel = server.models.LookupValue;
              var lookupType = server.models.LookupType;
              var status = [];
              lookupType.findOne({'where': {lookupCode: 'INSTITUTE_TIER'}}, function(err, lookupResponse) {
                if (err) {
                  cb(err, null);
                } else {
                  lookupValueModel.find({'where': {'lookupTypeId': lookupResponse.lookupTypeId}}, function(err, res) {
                    if (err) {
                      cb(err, null);
                    } else {
                      res.map(function(data) {
                        if (data.lookupValueId == campusData.tireValueId) {
                          status.push('true');
                        }
                      });
                      if (status[0] == 'true') {
                        updateCampusRecord(campusData, function(err, campRes) {
                          if (err) {
                            throwError('there was an error', cb);
                          } else {
                            cb(null, campRes);
                          }
                        });
                      } else {
                        throwError('id was wrong', cb);
                      }
                    }
                  });
                }
              });
            } else {
              updateCampusRecord(campusData, function(err, campusRes) {
                if (err) {
                  cb(err, null);
                } else {
                  cb(null, campusRes);
                }
              });
            }
          } else {
            //throws error if Campus_Id is not provided in the input

            logger.error('Need CampusId for Updation');
            throwError('Need Campus_Id for Updation', cb);
          }
        } else {
          logger.error('Invalid Json');
          throwError('Invalid Json', cb);
        }
      });
      // }
    } else {
      //throws error if input is null

      logger.error("Input Can't be Null");
      throwError("Input Can't be Null", cb);
    }
  }
  var updateCampusRecord = function(campusData, campusCallBack) {
    Campus.findOne({
      'where': {
        'campusId': campusData.campusId,
      },
    }, function(err, campusList) {
      if (err) {
        campusCallBack(err, campusData);
      } else if (campusList) {
        //if record found it will update the attributes
        campusData['searchName'] = (campusList.name) ? campusList.name.toUpperCase() : null;
        campusData['searchShortName'] = (campusList.shortName) ? campusList.shortName.toUpperCase() : null;
        campusList.updateAttributes(campusData, function(err, info) {
          if (err) {
            //throws Invalid input error
            throwError('updateAttributes throws an error', campusCallBack);
          } else {
            //makes requestStatus as true if successfully update the attributes
            logger.info('Updated Campus Profile successfully');
            info['requestStatus'] = true;
            info['updateDatetime'] = new Date();
            campusCallBack(null, info);
          }
        });
      } else {
        //throws Invalid Campus_Id error
        logger.error('Invalid CampusId Provided');
        throwError('Invalid Campus_Id', campusCallBack);
      }
    });
  };

  exports.updateCampusProfile = updateCampusProfile;
