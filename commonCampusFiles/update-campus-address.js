  'use strict';
  var server = require('../server/server');
  var logger = require('../server/boot/lib/logger');
  // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Folder names should be separated by - with lower case
  // This helps avoid confusion as Linux based machines are case sensitive where this will be hosted finally
  // this could be common-validation or validation
  // Same approach for file names too - except for where keeping the model name similar is mandatory.
  // https://stackoverflow.com/questions/7273316/what-is-the-javascript-filename-naming-convention
  var validation = require('../commonValidation/addressValidation');
  var updateAddress = require('../commonValidation/updateAddress');
  var updateAddService = require('../commonValidation/update-service-address');
  var CampusAddress = server.models.CampusAddress;
  var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;

  // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Break down logic into multiple functions
  // Makes it easier to read.

  /**
   *updateCampus-function deals with updating the campus data
   *@constructor
   * @param {object} campusData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
  function updateCampus(campusData, cb) {
    if (campusData) {
      // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Create a generic function which checks for these properties
      // That function can take an array or single inputs to check for the property if available, then automatically throws an error with the "property" name too. 
      // Also validations can be done using beforeEach hooks.

      if (campusData.campusId && campusData.addressId) {
        CampusAddress.findOne({
          'where': {
            'and': [{
              'campusId': campusData.campusId,
            }, {
              'addressId': campusData.addressId,
            }],
          },
        }, function(err, campusList) {
          if (err) {
            //throws error
            cb(err, campusData);
          } else if (campusList) {
            // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Fix Type for Address
            updateAddService.updateAddresService(campusData, function(err, campusAddressList) {
              if (err) {
                cb(err, campusData);
              } else {
                logger.info('successfully campus address updated');
                cb(null, campusAddressList);
              }
            });
          } else {
            //throws error incase of invalid address_id or campusId

            logger.error('Invalid address_id or campusId');
            throwError('Invalid address_id or campusId', cb);
          }
        });
      } else {
        throwError('campusId and  addressId are required', cb);
      }
    } else {
      logger.error("Input can't be Null");
      throwError("Input can't be Null", cb);
    }
  }
  exports.updateCampus = updateCampus;
