  'use strict';
  var server = require('../server/server');
  var logger = require('../server/boot/lib/logger');
  var contactValidation = require('../commonValidation/contactValidation');
  var updateContact = require('../commonValidation/updateContact');
  var updateContactService = require('../commonValidation/update-service-contact');
  var DepartmentContact = server.models.DepartmentContact;
  var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
  /**
   *updateDepartmentContact-function deals with updating department contact data
   *@constructor
   * @param {object} contactData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
  function updateDepartmentContact(contactData, cb) {
    if (contactData) {
      //validating the input
      if (contactData.campusId && contactData.departmentId && contactData.contactId) {
        DepartmentContact.findOne({
          'where': {
            'and': [{
              'campusId': contactData.campusId,
            }, {
              'departmentId': contactData.departmentId,
            }, {
              'contactId': contactData.contactId,
            }],
          },
        }, function(err, departmentList) {
          if (err) {
            cb(err, contactData);
          } else if (departmentList) {
            updateContactService.updateContactService(contactData, function(error, info) {
              if (error) {
                logger.error('error while updating department contact');
                cb(error, contactData);
              } else {
                logger.info('Successfully department contact updated');
                cb(null, info);
              }
            });
          } else {
            //throws error when invalid contatcId or departmentId or campusId
            throwErrorr('Invalid contactId or campusId or departmentId', cb);
            logger.error('Invalid contactid or campusId or departmentId');
          }
        });
      } else {
        throwError('campusId , departmentId and contactId are required', cb);
        logger.error('campusId , departmentId and contactId are required');
      }
    } else {
      logger.error("Input Can't ne Null");
      throwError("Input Can't ne Null ", cb);
    }
  }
  exports.updateDepartmentContact = updateDepartmentContact;
