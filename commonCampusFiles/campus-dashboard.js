'use strict';
var server = require('../server/server');
var async = require('async');
var selectedStudents = [];
var selectedStudentsCountArray = [];
var shortListedCountArray = [];
var departmentSelectedStudents = [];
var totalComp = 0;
var allStudentsOfDepartment = 0;
var totalStudentsInvolvedInEvents = [];
var campusDashboard = function(campusId, cb) {
  var department = server.models.Department;
  department.find({'where': {'campusId': campusId}},
  function(err, departmentsOfCampus) {
    if (err) {
      cb(err, null);
    } else {
      async.map(departmentsOfCampus, getCountsForDepartmentData,
            function(err, departmentResponse) {
              if (err) {
                cb(err, null);
              } else {
                cb(null, departmentSelectedStudents);
                departmentSelectedStudents = [];
              }
            });
    }
  });
  function getCountsForDepartmentData(departmentObject, callback) {
    var campusDrive = server.models.CampusDrive;
    var endDate = new Date();
    var startDate = new Date();
    var currentYear = new Date().getFullYear();
    var month = new Date().getMonth();
    if (month > 6) {
      startDate.setFullYear((currentYear), 5, 1);
      endDate.setFullYear((currentYear + 1), 4, 31);
    } else if (month <= 6) {
      startDate.setFullYear((currentYear - 1), 5, 1);
      endDate.setFullYear((currentYear), 4, 31);
    }
    campusDrive.find({'where': {'departmentId': departmentObject.departmentId,
    'createDateTime':
    {
      between: [startDate, endDate],
    }}},
    function(err, driveResponse) {
      if (err) {
        callback(err, null);
      } else if (driveResponse == []) {
        callback(null, 0);
      } else {
        async.map(driveResponse, getEventsOfCampus, function(err, response) {
          if (err) {
            callback(err, null);
          } else {
            async.map(selectedStudents, getAverageComp,
                function(err, averageComp) {
                  if (err) {
                    callback(err, null);
                  } else {
                    var program = server.models.Program;
                    program.find({'where': {'departmentId': departmentObject.departmentId}},
                function(err, allPrograms) {
                  if (err) {
                    callback(err, null);
                  } else {
                    async.map(allPrograms, getAllStudents, function(err, allStudents) {
                      if (err) {
                        callback(err, null);
                      } else {
                        console.log('total involved', totalStudentsInvolvedInEvents, 
                        shortListedCountArray, selectedStudentsCountArray);
                        var data = {};
                        data.departmentName = departmentObject.shortName;
                        data.selected = selectedStudentsCountArray.length;
                        data.shortListed = shortListedCountArray.length;
                        data.totalStudentsOfDepartment = allStudentsOfDepartment;
                        data.totalStudentsInvolvedInEvents = totalStudentsInvolvedInEvents.length;
                        data.unAttendedStudents = allStudentsOfDepartment - (totalStudentsInvolvedInEvents.length);
                        data.averageComp = (totalComp / (selectedStudents.length));
                        departmentSelectedStudents.push(data);
                        callback(null, response);
                        allStudentsOfDepartment = 0;
                        selectedStudents = [];
                        selectedStudentsCountArray = [];
                        shortListedCountArray = [];
                        totalComp = 0;
                        totalStudentsInvolvedInEvents = [];
                      }
                    });
                  }
                });
                  }
                });
          }
        });
      }
    });
    function getAllStudents(obj, studentCallback) {
      var endDate = new Date();
      var startDate = new Date();
      var currentYear = new Date().getFullYear();
      var month = new Date().getMonth();
        // console.log(currentYear + 1);
      if (month > 6) {
        startDate.setFullYear((currentYear), 5, 1);
        endDate.setFullYear((currentYear + 1), 4, 31);
      } else if (month <= 6) {
        startDate.setFullYear((currentYear - 1), 5, 1);
        endDate.setFullYear((currentYear), 4, 31);
      }
      var enroll = server.models.Enrollment;
      enroll.find({'where': {'programId': obj.programId,
      'planedCompletionDate':
      {
        between: [startDate, endDate],
      }}},
        function(err, programResponse) {
          if (err) {
            studentCallback(err, null);
          } else {
            allStudentsOfDepartment = programResponse.length + allStudentsOfDepartment;
            console.log('all', allStudentsOfDepartment);
            studentCallback(null, 'done');
          }
        });
    }
    function getEventsOfCampus(campusDriveObject, callBc) {
      var campusEvent = server.models.CampusEvent;
      var endDate = new Date();
      var startDate = new Date();
      var currentYear = new Date().getFullYear();
      var month = new Date().getMonth();
      if (month > 6) {
        startDate.setFullYear((currentYear), 5, 1);
        endDate.setFullYear((currentYear + 1), 4, 31);
      } else if (month <= 6) {
        startDate.setFullYear((currentYear - 1), 5, 1);
        endDate.setFullYear((currentYear), 4, 31);
      }
      campusEvent.find({'where': {'campusDriveId': campusDriveObject.campusDriveId,
      'createDateTime':
      {
        between: [startDate, endDate],
      }}},
        function(err, campusEventResponse) {
          if (err) {
            callBc(err, null);
          } else {
            async.map(campusEventResponse, allEventsOfCampus,
                function(err, eventResponse) {
                  if (err) {
                    callBc(err, null);
                  } else {
                    callBc(null, eventResponse);
                  }
                });
          }
        });
    }
  }
  function allEventsOfCampus(eventResponseData, callBack) {
    var eventStudentList = server.models.EventStudentList;
    eventStudentList.find({'where': {'campusEventId': eventResponseData.campusEventId}},
    function(err, response) {
      if (err) {
        callBack(err, null);
      } else {
        async.map(response, getStudentCounts, function(err, studentResponse) {
          if (err) {
            callBack(err, null);
          } else {
            callBack(null, studentResponse);
          }
        });
      }
    });
    function getStudentCounts(eventStudentObject, eventStudentCallBack) {
      if (eventStudentObject.candidateStatusValueId == 381) {
        selectedStudents.push(eventStudentObject);
        if (!(selectedStudentsCountArray.includes(parseInt(eventStudentObject.studentId)))) {
          selectedStudentsCountArray.push(parseInt(eventStudentObject.studentId));
          if (!(totalStudentsInvolvedInEvents.includes(parseInt(eventStudentObject.studentId)))) {
            totalStudentsInvolvedInEvents.push(parseInt(eventStudentObject.studentId));
            eventStudentCallBack(null, 'done');
          } else {
            eventStudentCallBack(null, 'done');
          }
        } else {
          eventStudentCallBack(null, 'done');
        }
      } else if (eventStudentObject.updateDatetime > eventResponseData.scheduledDate) {
        if (!(shortListedCountArray.includes(parseInt(eventStudentObject.studentId)))) {
          shortListedCountArray.push(parseInt(eventStudentObject.studentId));
          if (!(totalStudentsInvolvedInEvents.includes(parseInt(eventStudentObject.studentId)))) {
            totalStudentsInvolvedInEvents.push(parseInt(eventStudentObject.studentId));
            eventStudentCallBack(null, 'done');
          } else {
            eventStudentCallBack(null, 'done');
          }
        } else {
          eventStudentCallBack(null, 'done');
        }
      } else {
        if (!(totalStudentsInvolvedInEvents.includes(parseInt(eventStudentObject.studentId)))) {
          totalStudentsInvolvedInEvents.push(parseInt(eventStudentObject.studentId));
          eventStudentCallBack(null, 'done');
        } else {
          eventStudentCallBack(null, 'done');
        }
      }
    }
  }
  function getAverageComp(studentObject, compCallBack) {
    var comp = server.models.CompensationPackage;
    comp.findOne({'where': {'compPackageId': studentObject.compPackageId},
    'fields': {
      'totalCompPkgValue': true,
    }}, function(err, compAmount) {
      if (err) {
        compCallBack(err, null);
      } else if (compAmount == null) {
        compCallBack(null, 'done');
      } else {
        totalComp = parseInt(compAmount.totalCompPkgValue) + totalComp;
        compCallBack(null, 'done');
      }
    });
  }
};
function cleanArray(array, cb) {
  var sampleArray = [];
  for (var i = 0; i < array.length; i++) {
    if (array[i] !== [] || array[i] !== 0 || array[i] !== null) {
      sampleArray.push(array[i]);
    }
    if (i == array.length - 1) {
      cb(null, sampleArray);
    }
  }
}
exports.campusDashboard = campusDashboard;
