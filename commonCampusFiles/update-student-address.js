'use strict';
var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var validateModel = require('../ServicesImpl/CommonImpl/models-validation.js').validateInput;
var studentJson = require('../common/models/STUDENT_ADDRESS.json').properties;
var lookupValidation = require('../commonValidation/lookupMethods.js').validateLookups;
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var findAndUpdate = require('../commonValidation/find-and-update-address').findAndUpdate;
var studentAddress = server.models.StudentAddress;
var async = require('async');
  /**
   *updateStudentAddressService-function deals with updating the student address service
   *@constructor
   * @param {object} input - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function updateStudentAddressService(input, cb) {
  if (input) {
    // validateModel(input, studentJson, function(error, response) {
    //   if (error) {
    //     throwError(JSON.stringify(error), cb);
    //   } else {
    getInfo(input, function(e, r) {
      if (e) {
        cb(e, null);
      } else {
        logger.info('Student Address updated successfully');
        cb(null, r);
        //   }
        // });
      }
    });
  } else {
    throwError('Invalid Input', cb);
  }
}

function getInfo(obj, callback) {
  if (obj.studentId && obj.addressId) {
    var findData = {};
    findData['studentId'] = obj.studentId;
    findData['addressId'] = obj.addressId;
    findAndUpdate(obj, findData, studentAddress, function(err, output) {
      if (err) {
        callback(err, null);
      } else {
        callback(null, output);
      }
    });
  } else {
    callback('studentId and addressId are required', null);
  }
}
exports.updateStudentAddressService = updateStudentAddressService;
